import asyncio
import discord
import io
import json
import psutil
import queue
import re
import sys

from datetime import datetime
from dmp import diff_match_patch
from PIL import Image

import db_handler as db
import responders as r
import threading
import tkinter as tk

dmp = diff_match_patch()
dmp.Match_Distance = 10000
data = json.load(open('other stuff/info.json', 'r'))
loop = asyncio.new_event_loop()


class R:
	def __init__(self, seed):
		self.base = seed
		self.mult = int(seed * 1.5)
		self.inc = seed | self.mult
		self.mod = seed + (self.inc ^ self.mult)
	
	def rand(self):
		temp_1 = self.base * self.mult
		temp_2 = temp_1 + self.inc
		self.base = temp_2 % self.mod
		# print(temp_1, temp_2)
		return self.base
	
	def __repr__(self):
		return f"{self.base} {self.mult} {self.inc} {self.mod}"


class Channel(tk.Frame):
	def __init__(self, root, loop_c, message, name):
		super().__init__(root, name = name)
		print(self)
		try:
			self.root = root
			self.target = message.author
			self.loop = loop_c
			name = re.sub("\\\\N", "", self.target.name.encode("cp1252", "namereplace").decode('cp1252'))
			content = re.sub("\\\\N", "", message.content.encode("cp1252", "namereplace").decode('cp1252'))
			
			self.entry = tk.Entry(self)
			self.button = tk.Button(self, text = "Reply!", command = lambda: self.loop.create_task(self.reply()))
			self.text = tk.Text(self, borderwidth = 0)
			self.label = tk.Label(self, text = name)
			self.text.insert(1.0, content)
			self.text.config(state = "disabled", width = len(content), height = len(content.split("\n")))
			
			self.entry.pack()
			self.button.pack()
			self.label.pack()
			self.text.pack(anchor = "w")
			
			self.root.callback()
		except Exception as e:
			print(e, e.__traceback__.tb_lineno)
			return
		
	def add(self, val):
		text = tk.Text(self, borderwidth = 0)
		lines = val.split("\n")
		longest = max(lines, key = lambda x: len(x))
		text.insert(1.0, val)
		text.config(state = "disabled", width = len(longest), height = len(lines))
		text.pack(anchor = "w")
	
	async def reply(self):
		try:
			msg = self.entry.get()
			self.entry.delete(0, "end")
			await self.target.dm_channel.send(msg)
		finally:
			try:
				self.root.authors.remove(self.target.id)
				self.root.count -= 1
				self.root.callback()
			except Exception as e:
				print(e)
			self.destroy()
			return


class Gui(tk.Tk):
	event = threading.Event()
	queue = queue.Queue(maxsize = 1)
	
	def __init__(self, loop_g, client):
		super().__init__()
		self.count = 0
		self.authors = []
		self.frames = []
		self.obj = []
		self.loop = loop_g
		self.client = client
		self.frame = tk.Frame(self)
		button = tk.Button(self.frame, name = "button", text = "Send!", command = self.dm)
		entry = tk.Entry(self.frame, name = "entry")
		button.pack()
		entry.pack(fill = tk.X)
		self.frame.pack(fill = tk.X)
		self.label = tk.Label(self, text = self.count)
		self.label.pack()
		self.after(1000, threading.Thread(target = self.check).start)
		self.mainloop()
	
	def callback(self):
		self.label["text"] = self.count
		
	def dm(self):
		_id, message = self.frame.children["entry"].get().split(" ", 1)
		self.frame.children["entry"].delete(0, "end")
		self.queue.put({self.client.get_user(int(_id)): message})
		self.event.set()
		
	def check(self):
		while True:
			# print(self.event.is_set())
			self.event.wait()
			self.obj.insert(0, self.queue.get(block = True))
			# print(self.obj, self.authors, self.frames)
			print(self.obj)
			if self.obj[0] == "STOP":
				self.after(5, self.quit)
				break
			
			elif type(self.obj[0]) == discord.Message:
				if self.obj[0].author.id not in self.authors:
					self.count += 1
					self.authors.append(self.obj[0].author.id)
					frame = Channel(self, self.loop, self.obj[0], str(self.obj[0].author.id))
					frame.pack(side = tk.LEFT)
					self.frames.append(frame)
					self.focus_force()
				else:
					obj = self.obj.pop(0)
					self.children[str(obj.author.id)].add(obj.content)
					self.focus_force()
			
			elif type(self.obj[0]) == dict:
				obj = self.obj.pop(0)
				print(obj)
				self.loop.create_task(dm_send(obj))
			
			elif type(self.obj[0]) == tuple:
				obj = self.obj.pop(0)
				# print(obj)
				if obj[0] == "MSG":
					# print(obj[1])
					rep = '\\\\N'
					text = "\n".join([f"{el[0][0]} um {el[0][1]}: "
						f"{re.sub(rep, '', el[1].encode('cp1252', 'namereplace').decode('cp1252'))}\n"
						for el in obj[1]])
					# print(text)
					label = tk.Label(self, text = text, wraplength = 1000, anchor = tk.W, justify = tk.LEFT)
					label.pack()
					self.after(60000, label.destroy)
					
			self.event.clear()


class Data:
	def __init__(self, *args, author: discord.Member = None, command: str = None, channel: discord.TextChannel = None,
				client: discord.Client = None, message: discord.Message = None):
		self.author = author
		self.command = command
		self.channel = channel
		self.client = client
		self.message = message
		self.args = list(args)
	
	def __repr__(self):
		return str(self.__dict__)


async def approve(client: discord.Client, embed: discord.Embed):
	owner = client.get_user(data["owner"])
	dm_c = owner.dm_channel
	
	if not dm_c:
		dm_c = await owner.create_dm()
	
	dm = await dm_c.send(embed = embed)
	await dm.add_reaction("✅")
	await dm.add_reaction("❌")
	
	def check(check_reaction, check_user):
		# print(f"{dm}\n\n{check_reaction.message}")
		return check_user == dm_c.recipient and dm.id == check_reaction.message.id
	
	resp_reaction, user = await client.wait_for('reaction_add', check = check)
	return resp_reaction.emoji, dm_c


def calc_color_rgb(text):
	color_hex = hex(sum(map(ord, text))).split("x")[1].zfill(6)
	rgb = tuple(map(lambda x: int(x, base = 16), tuple(map("".join, tuple(zip(color_hex[::2], color_hex[1::2]))))))
	return color_hex, rgb


async def calc_exp(client: discord.Client, message: discord.Message):
	if message.channel == message.author.dm_channel or message.author.bot:
		return
	coll1 = db.db.get_collection(name = "User_stats")
	coll2 = db.db.get_collection(name = "Attacken")
	attacken = coll2.find(projection = {"_id": False, "nr": False})
	_id = message.author.id
	user = coll1.find_one({"id": _id})
	
	# noinspection PyDictCreation
	if user:
		try:
			if user.get("state") and (user["state"]["value"] == "dead"):
				# print("dead!")
				return
		except KeyError:
			pass
		# string = re.sub(r'(.)(\1)*',"\g<1>" ,message.content)
		exp = int(len(message.content) % (10 + user["lvl"]) + user["exp"])
		val = 100 + 10 * user["lvl"]
		level = int(exp / val)
		# print(f"{exp};{val};{level};{len(message.content) % (10 + user['lvl'])}")
		
		if level > user["lvl"]:
			bytesio = io.BytesIO()
			asset = message.author.avatar_url_as(format = "webp", static_format = "webp")
			await asset.save(bytesio)
			im = Image.open(bytesio)
			res = sorted(im.getcolors(maxcolors = 600000), key = lambda v: v[0])[-1]
			print(message.author.name, res)
			try:
				maximum = sum(res[1][:3])
			except TypeError:
				maximum = res[1]
			hp = level * 100 + 100
			
			title = f"{message.author.name} leveled up!"
			attacks1 = user["learned attacks"]
			attacks2 = user["usable attacks"]
			base_embed = r.emb_resp(title, "", "info")
			info_embed = r.emb_resp(title, "", "info")
			base_embed.set_thumbnail(url = message.author.avatar_url)
			info_embed.set_thumbnail(url = message.author.avatar_url)
			try:
				attack = attacken[len(attacks1) - 1]
			except IndexError:
				attack = None
			
			if attack and len(attacks2) < 4:
				
				base_embed.description = f"```python\nNew attack \"{attack['title']}\" learned!```"
				
				coll1.update_one(
					{
						"id": _id
					}, {
						"$set": {
							"exp": exp,
							"lvl": level,
							"init": sum(map(int, message.author.discriminator)) / 100 * level,
							"def": sum(calc_color_rgb(message.author.name)[1]) + level,
							"atk": maximum * (0 + level / 100) + level,
							"max hp": hp,
							"hp": hp
						},
						"$addToSet": {
							"learned attacks": attack,
							"usable attacks": attack
						}
					}
				)
				
				msg = await message.channel.send(embed = base_embed)
			
			elif attack:
				
				base_embed.description = f"```python\nDo you want to learn the new attack \"{attack['title']}\"?\nIf yes, " \
									f"select an attack to forget``` "
				number = 1
				for atk in attacks2:
					base_embed.add_field(name = str(number), value = f"```{atk['title']}```", inline = False)
					number += 1
				
				msg = await message.channel.send(embed = base_embed)
				
				value = f"```python\n- dmg: {attack['dmg']}\n- Hit chance: {attack['rate']}\n- AP left: {attack['AP']}```"
				info_embed.add_field(name = attack["title"], value = value)
				
				reactions = ["1️⃣", "2️⃣", "3️⃣", "4️⃣", "ℹ️", "↩️", "❌"]
				await msg.add_reaction(reactions[0])
				await msg.add_reaction(reactions[1])
				await msg.add_reaction(reactions[2])
				await msg.add_reaction(reactions[3])
				await msg.add_reaction(reactions[4])
				await msg.add_reaction(reactions[6])
				
				def check(check_reaction, check_user):
					return check_user == message.author and check_reaction.message.id == msg.id

				def mod_emb(emb, idx):
					descs = {
						-1: "```Ok, no attack overwritten!```",
						-2: "You took too long to decide!\nNo attack overwritten!"
					}
					d = {
						"exp": exp,
						"lvl": level,
						"init": sum(map(int, message.author.discriminator)) / 100 * level,
						"def": sum(calc_color_rgb(message.author.name)[1]) + level,
						"atk": maximum * (0 + level / 100) + level,
						"max hp": hp,
						"hp": hp
					}
					if idx >= 0:
						d.update({f"usable attacks.{idx}": attack})
						_atk = emb.fields[idx].value.strip('```')
						emb.description = f"```python\nOk, \"{_atk}\" successfully overwritten!```"
					else:
						emb.description = descs[idx]
					coll1.update_one(
						{
							"id": _id
						}, {
							"$set": d,
							"$addToSet": {
								"learned attacks": attack
							}
						}
					)
					# coll1.update_one({"id": _id}, {"$set": {f"usable attacks.{idx}": attack}})
				
				while True:
					try:
						resp_reaction, user = await client.wait_for("reaction_add", check = check, timeout = 60)
						if resp_reaction.emoji == reactions[0]:
							# print(1)
							
							mod_emb(base_embed, 0)
							'''coll1.update_one({"id": _id}, {"$set": {"usable attacks.0": attack}})
							atk = base_embed.fields[0].value.strip('```')
							base_embed.description = f"```python\nOk, \"{atk}\" successfully overwritten!```"'''
							break
						
						elif resp_reaction.emoji == reactions[1]:
							# print(2)
							
							mod_emb(base_embed, 1)
							'''coll1.update_one({"id": _id}, {"$set": {"usable attacks.1": attack}})
							atk = base_embed.fields[1].value.strip('```')
							base_embed.description = f"```python\nOk, \"{atk}\" successfully overwritten!```"'''
							break
						
						elif resp_reaction.emoji == reactions[2]:
							# print(3)
							
							mod_emb(base_embed, 2)
							'''coll1.update_one({"id": _id}, {"$set": {"usable attacks.2": attack}})
							atk = base_embed.fields[2].value.strip('```')
							base_embed.description = f"```python\nOk, \"{atk}\" successfully overwritten!```"'''
							break
						
						elif resp_reaction.emoji == reactions[3]:
							# print(4)
							
							mod_emb(base_embed, 3)
							'''coll1.update_one({"id": _id}, {"$set": {"usable attacks.3": attack}})
							atk = base_embed.fields[3].value.strip('```')
							base_embed.description = f"```python\nOk, \"{atk}\" successfully overwritten!```"'''
							break
						
						elif resp_reaction.emoji == reactions[4]:
							# print("info")
							
							await msg.edit(embed = info_embed)
							await msg.clear_reactions()
							await msg.add_reaction(reactions[5])
							await msg.add_reaction(reactions[6])
						
						elif resp_reaction.emoji == reactions[5]:
							# print("back")
							
							await msg.edit(embed = base_embed)
							await msg.clear_reactions()
							[await msg.add_reaction(react) for react in reactions[:5]]
							await msg.add_reaction(reactions[6])
						
						elif resp_reaction.emoji == reactions[6]:
							# print("x")
							
							mod_emb(base_embed, -1)
							break
					
					except asyncio.TimeoutError:
						mod_emb(base_embed, -2)
						# base_embed.description = "You took to long to decide!\nNo attack overwritten!"
						break
				
				base_embed.clear_fields()
				try:
					await msg.edit(embed = base_embed)
					await msg.clear_reactions()
				except discord.NotFound:
					pass
			
			else:
				coll1.update_one({"id": _id}, {"$inc": {"tokens": 1}})
				base_embed.description = "Received one learning token!"
				msg = await message.channel.send(embed = base_embed)
			
			try:
				await msg.delete(delay = 60)
			except discord.NotFound:
				pass
		
		else:
			coll1.update_one({"id": _id}, {"$set": {"exp": exp, }})

	else:
		# noinspection PyDictCreation
		user = {}
		user["name"] = client.get_user(_id).name
		user["id"] = _id
		user["learned attacks"] = [attacken[attacken.count() - 1]]
		user["usable attacks"] = [attacken[attacken.count() - 1]]
		user["exp"] = len(message.content) % 100
		user["lvl"] = 0
		user["max hp"] = 100
		user["init"] = 0
		user["def"] = sum(calc_color_rgb(message.author.name)[1])
		user["atk"] = 0
		user["hp"] = user["max hp"]
		user["last attack"] = f"{datetime.now().strftime('%Y/%m/%d - %H:%M:%S')}"
		print(user)
		coll1.insert(user)


async def check_json(key: str) -> str:
	with open('commands.json') as cmd_list:
		json_data = json.load(cmd_list)["Commands"]
		# print(json_data)
		for val in json_data:
			# print(val)
			if val['name'] == key:
				# print(val)
				return val['name']
			else:
				for alt in val['alt']:
					if alt == key:
						# print(alt)
						return val['name']
	return "error"


def concat(_type: str = "v", _id: str = "") -> str:
	types = {
		"v": "watch",
		"list": "playlist"
	}
	return f"https://www.youtube.com/{types[_type]}?{_type}=" + _id


async def dm_send(obj):
	try:
		dm = await list(obj.keys())[0].create_dm()
		await dm.send(list(obj.values())[0])
	except Exception as e:
		print(e)
	finally:
		return


async def get_alt(content: str, cmd: str):
	index_old = 0
	index_new = 0
	nope = 0
	i = 0
	regex = re.compile("?".join(list(cmd)) + "?", re.I)
	
	if content.find(cmd) != -1:
		print("new message: ", re.split(r" ?\w*" + cmd + r"\w*", content, re.IGNORECASE))
		content_new = "".join(re.split(r" ?\w*" + cmd + r"\w*", content, re.IGNORECASE))
		_alts_, _descs_ = content_new.split(";", 1)
		# _descs_ = content_new.split(";")[1]
	
	else:
		_alts_, _descs_ = content.split(";", 1)
		# _descs_ = content.split(";")[1]
	
	if not _alts_.find(" "):
		_alts_ = _alts_[1:]
	if not _descs_.find(" "):
		_descs_ = _descs_[1:]
	
	print("_alts_: ", _alts_)
	print("_descs_: ", _descs_)
	
	alts = _alts_.split(" ")
	descs = _descs_.split(" ")
	
	print("alts: ", alts)
	print("descs: ", descs)
	
	for alt, desc in zip(alts, descs):
		# print("0")
		
		if (not alt or not desc) or (await check_json(alt) == "error" and await check_json(desc) == "error"):
			print("alt und desc: ", alt, desc)
			if regex.match(alt):
				if nope:
					nope -= 1
			elif regex.match(desc):
				nope += 1
				
			# while (i < len(alt)) or (i < len(desc)):
			# 	# print("1")
			#
			# 	if not nope:
			# 		# print("2")
			#
			# 		try:
			# 			index_new = cmd.find(alt[i], index_old)
			# 		except IndexError:
			# 			pass
			# 		# print("3")
			#
			# 	else:
			# 		# print("4")
			#
			# 		try:
			# 			index_new = cmd.find(desc[i], index_old)
			# 		except IndexError:
			# 			pass
			#
			# 		# print("5")
			#
			# 	# print("6")
			# 	i = i + 1
			#
			# 	if (index_new == -1) and (not nope):
			# 		# print("7")
			# 		nope = 1
			# 		i = 0
			# 		index_old = 0
			#
			# 	# print("8")
		else:
			print("exists")
			return -1, 1
	
	if not nope:
		print("yep")
		desc = content.split(";")[1]
		
		if not desc.find(" "):
			desc = desc[1:]
		print(alts, desc)
	
	else:
		print("nope")
		alts = descs
		desc = content.split(";")[0]
		
		if not desc.find(" "):
			desc = desc[1:]
		print(alts, desc)
		
	return alts, desc


def get_bot_uptime():
	processes = psutil.process_iter(["cmdline", "create_time", "memory_percent"])
	# print(processes)
	exe = sys.executable
	try:
		bot = next(filter(lambda x: x.info["cmdline"] == [exe, "startup.py"], processes))
	except Exception as e:
		print(e)
		return
	utc_offset = datetime.now() - datetime.utcnow()
	return f'{datetime.fromtimestamp(bot.info["create_time"])} (UTC+{utc_offset})'


async def get_content(message: discord.Message):
	content = message.split(data["prefix"], 1)[1]  # remove prefix
	return re.split("\s+", content, 1)  # split identifier


async def make_temp_file(im, file_type = None, name = None):
	if file_type:
		if name:
			bytesio = io.BytesIO()
			im.save(bytesio, file_type)
			bytesio.seek(0)
			file = discord.File(bytesio, filename = name)
			return file
		else:
			raise RuntimeError("No name provided!")
	else:
		raise TypeError("No filetype provided!")


async def notify_owner(client: discord.Client, message: str):
	owner = client.get_user(data["owner"])
	dm_c = owner.dm_channel
	
	if not dm_c:
		dm_c = await owner.create_dm()
	
	await dm_c.send(embed = r.emb_resp("PROBLEM", message, "error"))
	

async def reaction(msg: discord.Message, author: discord.Member, client: discord.Client) -> discord.Emoji:
	await msg.add_reaction("✅")
	await msg.add_reaction("❌")
	
	def check(check_reaction, check_user):
		print(f"{msg}\n\n{check_reaction.message}")
		return check_user == author and msg.id == check_reaction.message.id
	
	resp_reaction, user = await client.wait_for('reaction_add', check = check)
	await msg.clear_reactions()
	return resp_reaction.emoji
	

async def separate(text: list, embed: discord.Embed, separator = ". ") -> discord.Embed:
	for seq in text:
		seq: str = seq.replace("\n", " ")
		length = len(seq)
		# print(length)
		# print(seq)
		if length < 1024:
			try:
				embed.add_field(name = "💬", value=seq, inline=False)
			except Exception as e:
				print(e)
		else:
			seq1 = seq[:seq.rfind(separator, 900, 1000)]
			seq2 = seq[seq.rfind(separator, 900, 1000):]
			# print(seq1, seq2)
			embed.add_field(name = "💬", value=seq1, inline=False)
			embed.add_field(name = "💬", value=seq2, inline=False)
	return embed


def separate_2(embed: discord.Embed, text: str, separator = "\n", field_name = "_ _"):
	while text:
		split_pos = text.rfind(separator, 800, 1000)
		if split_pos != -1:
			new_desc = text[:split_pos]
			text = text[split_pos:]
		else:
			new_desc = text[:]
			text = ""
		embed.add_field(name = field_name, value = new_desc, inline = False)
	return embed
