import math
import random
from datetime import datetime

import discord

import db_handler as db
import responders as r

coll = db.db.get_collection(name = "User_stats")


async def get_profile(author: discord.Member):
	embed = r.emb_resp("", "", "std_info")
	
	embed.set_thumbnail(url = author.avatar_url)
	
	user = coll.find_one({"id": author.id})
	
	if not user:
		return "Error", "You don't have a profile yet!", "error"
	
	embed.title = f"__EXP collected: {user['exp']} || Until next level: " \
		f"{(100 + 10 * user['lvl']) * (user['lvl'] + 1) - user['exp']}__"
	
	embed.description = f"```python\nHP {user['hp']}/{user['max hp']}```"
	
	embed.set_author(name = f"{author.name}'s profile")
	
	attacks = user["usable attacks"]
	
	embed.add_field(name = "Level", value = user["lvl"])
	embed.add_field(name = "You are", value = user["state"]["value"] if user.get("state") else "alive", inline = False)
	embed.add_field(name = "Tokens for learning attacks for free",
		value = f'```python\n{user.get("tokens") or 0}```', inline = False)
	embed.add_field(name = "Attack", value = user.get("atk"))
	embed.add_field(name = "Defense", value = user.get("def"))
	embed.add_field(name = "Initiative", value = user.get("init"))
	
	for atk in attacks:
		try:
			value = f"```python\n- dmg: {atk['dmg']}\n- Hit chance: {atk['rate']}\n- AP left: {atk['AP']}```"
			embed.add_field(name = atk["title"], value = value, inline = False)
		except Exception as e:
			print(e)
	
	return embed


async def get_attacks(author: discord.Member, sort_by, inverse, *args: int):
	embed = r.emb_resp("", "", "std_info")
	
	user = coll.find_one({"id": author.id})
	try:
		embed.set_author(name = f"Showing {args[1] - args[0] + 1} out of")
	except IndexError:
		embed.set_author(name = f"Showing {len(user['learned attacks'])} out of")
	
	embed.title = f"{len(user['learned attacks'])} learned attacks"
	# print(sort_by, inverse)
	key = (lambda x: x.get(sort_by) or x.get(sort_by.upper()) or 0)
	attacks = sorted(user["learned attacks"], key = key, reverse = bool(inverse))
	
	text = ""
	index = 1
	for attack in attacks:
		if args:
			if args[0] <= index <= args[1]:
				text += f"- [{attack['title']}](https://www.pokewiki.de/{attack['title']})\n"
		else:
			text += f"- [{attack['title']}](https://www.pokewiki.de/{attack['title']})\n"
		index += 1
		if len(text) > 2000:
			embed.set_author(name = f"Showing {index-1} out of")
			break
	embed.description = f"{text}"
	return embed


async def mute(target: discord.Member, channel: discord.TextChannel, reason, _type):
	if _type == "day":
		state = {
			"value": "dead",
			"since": datetime.now().strftime("%Y/%m/%d"),
			"in": channel.id
		}
	elif _type == "temp":
		state = {
			"value": "muted",
			"since": datetime.now().strftime("%H/%M/%S"),
			"in": channel.id
		}
	else:
		return await channel.send("Something weird went wrong")
	
	coll.update_one(
		{
			"id": target.id
		}, {
			"$set": {
				"state": state
			}
		}
	)
	try:
		await channel.set_permissions(target, send_messages = False, reason = reason)
	except discord.Forbidden:
		pass
	except discord.NotFound:
		return "Error", f"{target.display_name} seems to have left the guild within the past milliseconds!", "error"
	except Exception as e:
		return "Error", f"{str(e)}\n{e.__traceback__.tb_lineno}", "error"
	# print("a")


async def update(target: dict, attack_info: tuple, *args: int):
	attack: dict = attack_info[0]
	attacker: dict = attack_info[1]
	diff = attacker["lvl"] - target["lvl"]
	init = attacker["init"]
	print(diff)
	div = math.ceil(init / 100) * 100
	chances = [init / div, 1 - init / div]
	print(div, chances)
	crit = random.choices([1, 0], chances)[0]
	print(crit)
	if diff:
		mod = 1 / diff if diff > 0 else 1 - 1 / abs(diff)
	else:
		mod = 1
	print(mod)
	mult = (attacker["atk"] / target["def"]) + mod
	print(mult)
	damage = attack["dmg"] * mult
	print(damage)
	damage *= 2 if crit else 1
	damage = int(damage)
	print(damage)
	"""
	print(diff)
	div = math.ceil(attacker["init"]/100)*100
	print(div)
	crit = random.choices([1, 0], [attacker["init"]/div, 1 - attacker["init"]/div])[0]
	# print(crit)
	mult = attacker["atk"]*(100/abs(diff)) - target["def"]/diff
	print(mult)
	damage = attack["dmg"] * mult
	print(damage)
	damage *= 2 if crit else 1
	print(damage)"""
	_id = target["id"]
	
	if args:
		damage *= args[0]
		coll.update_one({"id": _id}, {"$inc": {"hp": - damage}})
		if coll.find_one({"id": _id})['hp'] <= 0:
			status = "dead"
		else:
			status = "alive"
	
	else:
		coll.update_one({"id": _id}, {"$inc": {"hp": - damage}})
		if coll.find_one({"id": _id})['hp'] <= 0:
			status = "dead"
		else:
			status = "alive"
	return crit, damage, status
