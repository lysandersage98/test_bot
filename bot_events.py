import aiohttp
import bs4
import datetime
import discord
import random
import re

from asyncio import sleep
from humanfriendly.tables import format_pretty_table, format_robust_table
from humanfriendly.terminal import ansi_strip

import db_handler as db
import responders as r
import utilities as u

prefix = u.data["prefix"]


def fill_queue(msg):
	while u.Gui.event.is_set():
		pass
	u.Gui.queue.put(msg)
	u.Gui.event.set()
# 	print(msg, u.Gui.event.isSet())


async def change_status(client: discord.Client):
	app_info: discord.AppInfo = await client.application_info()
	owner = app_info.owner
	statuses = []
	
	while 1:
		
		if len(statuses) != len(db.db.get_collection(name = "Lists").find_one({"name": "stati"})["stati"]):
			statuses = db.db.get_collection(name = "Lists").find_one({"name": "stati"})["stati"]
		shuffled_stati = statuses.copy()
		random.shuffle(shuffled_stati)
		print(len(shuffled_stati), len(statuses))
		
		dm_c = owner.dm_channel
		if not dm_c:
			dm_c = await owner.create_dm()
			
		for status in shuffled_stati:
			res = ""
			try:
				res = await check_wiki(db.db.get_collection("Lists"))
				if res:
					await dm_c.send(f"```{res}```")
			except aiohttp.ClientConnectorError:
				print("connection error while collecting new codes!")
			except discord.HTTPException:
				if res:
					print(res)
					await dm_c.send("NEW CODES IN CONSOLE")
			try:
				url, res = await check_gift(db.db.get_collection("ふも"))
				if res:
					await dm_c.send(embed = r.emb_resp("NEW", "\n".join(res), url = url))
			except AttributeError:
				pass
			except aiohttp.ClientConnectorError:
				print("connection error while checking gift!")
			except discord.HTTPException:
				if res:
					print("\n".join(res))
					await dm_c.send("NEW FUMOS IN CONSOLE")
			try:
				await client.change_presence(activity = discord.Game(name = status))
			except ConnectionResetError:
				pass
			# print(status)
			await check_state(client)
			await sleep(180)


async def check_wiki(coll):
	url = "https://genshin-impact.fandom.com/wiki/Promotional_Codes"
	async with aiohttp.request("GET", url) as resp:
		res = await resp.text()
		html = bs4.BeautifulSoup(res, "html.parser")
		wikitable: bs4.Tag = html.find(attrs = {"class": "wikitable"})
		# COLLECTING
		table_rows = wikitable.find_all("tr")
		titles = list(table_rows.pop(0).stripped_strings)
		table_columns = map(lambda row: row.find_all("td"), table_rows)
		all_data = list(map(lambda column: list(map(lambda x: list(filter(lambda y: not re.match("\[\d+]", y), x.stripped_strings)), column)), table_columns))
		code, server, reward, duration = list(zip(*all_data))
		# CLEANING
		codes = list(map(lambda x: x[0], code))
		servers = list(map(lambda x: x[0], server))
		rewards = list(map(lambda x: re.sub(" \|\| ×", "×", " || ".join(x)), reward))
		durations = list(map(lambda x: " || ".join(x), duration))
		data = list(zip(codes, servers, rewards, durations))
		data = list(filter(lambda x: "Expired" not in x[3], data))
		
		res = coll.find_one_and_update(
			{
				"name": "codes"
			}, {
				"$addToSet": {
					"codes": {
						"$each": codes
					}
				}
			},
			upsert = True,
			projection = {"id": False},
		)
		if not res or not set(codes).issubset(res["codes"]):
			return ansi_strip(format_robust_table(data, titles))


async def check_gift(coll):
	base = "https://www.gift-gift.jp"
	url = base + datetime.datetime.today().strftime("/news/%Y%m.html")
	# print(url)
	async with aiohttp.request("GET", url) as resp:
		res = await resp.text()
		html = bs4.BeautifulSoup(res, "html.parser")
		news = html.find(attrs = {"class": "news-item"})
	data = [f"[{el.text}]({base + el.a.get('href')})" for el in news.findAll("li")]
	news_id = news.get("id")
	if not coll.find_one({"id": news_id}):
		coll.insert_one({"id": news_id, "data": data})
		return url, data
	else:
		return None, None


async def check_state(client: discord.Client):
	coll = db.db.get_collection(name = "User_stats")
	user_list = client.users
	for user in user_list:
		player = coll.find_one({"id": user.id})
		if player and player.get("state") and player["state"].get("value"):
			try:
				if player["state"]["value"] == "dead":
					# print(player["name"])
					await try_revive(user, client.get_channel(player["state"]["in"]), "passive", 0)
				elif player["state"]["value"] == "passive":
					# print(player["name"])
					await try_revive(user, None, "alive", 0)
				elif player["state"]["value"] == "muted":
					# print(player["name"])
					await try_revive(user, client.get_channel(player["state"]["in"]), "alive", 1)
			except Exception as e:
				if "state" not in str(e):
					print(e)


async def dm_log(message):
	coll = db.db.get_collection(name = "data")
	try:
		if not message.channel.id == message.author.dm_channel.id:
			return
		else:
			# fill_queue(message)
			coll.update_one({"id": message.author.id, "log": True}, {"$push": {"messages": message.content}})
	except AttributeError:  # as e:
		pass   # print(message.author, e)


async def on_msg(message, client):
	coll3 = db.db.get_collection("data")
	_id = message.author.id
	user_check = coll3.find_one({"id": _id})
	if not user_check:
		user_check = {"id": _id, "name": message.author.name}
		coll3.insert_one(user_check)
	# print(message.content)
	channel = message.channel
	if not message.content:
		return
	try:
		if message.content.startswith(prefix):  # check if command
			content = await u.get_content(message.content)

			if list(filter(None, content)) and content[0]:
				return "everyone", content, message.author, channel
			else:
				return None
	
		# print(message.channel.permissions_for(message.author).administrator,discord.Permissions(8).administrator)
		
		elif message.content.split()[0].endswith(prefix):
			# print(message.channel.permissions_for(message.author).administrator,f"\n{message.author}\n{
			# client.get_user(db.json.load(open('other stuff/info.json',
			# 'r'))['owner'])}")##################message.channel.permissions_for(message.author).administrator or
			if message.author == client.get_user(u.data["owner"]):
				return "Admin", channel
		
			else:
				title = "Warning!"
				text = '''"Missing permission owner!"'''"not allowed to use that command!"
				embed = r.emb_resp(title, text, "error_2")
				embed.add_field(name = "Details", value = f"{message.channel.permissions_for(message.author)}")
				await channel.send(embed = embed)
				return
		
	except Exception as e:
		msg = await channel.send(embed = r.emb_resp("Error", f"{str(e)} in {e.__traceback__.tb_lineno}", "error_2"))
		await msg.delete(delay = 120)
		return
	
	# try:
	# 	if channel.id == client.get_user(u.data["owner"]).dm_channel.id:
	# 		last = client.cached_messages[-2]
	# 		await last.channel.send(message.content)
			
	# except AttributeError:
	# 	pass
	
	try:
		if user_check.get("leveling"):
			# print("leveling!")
			return
		else:
			coll3.find_one_and_update({"id": _id}, {"$set": {"leveling": True}}, upsert = True)
			await u.calc_exp(client, message)
			coll3.find_one_and_update({"id": _id}, {"$set": {"leveling": False}})
	except Exception as e:
		coll3.find_one_and_update({"id": _id}, {"$set": {"leveling": False}})
		await channel.send(embed = r.emb_resp("Error", e.__repr__()+f"\n{e.__traceback__.tb_lineno}", "error_2"))

	return


async def try_revive(target, channel, state, _type):
	coll = db.db.get_collection(name = "User_stats")
	subject = coll.find_one({"id": target.id})
	# print(subject["state"])
	
	if _type:
		old_state = datetime.datetime.strptime(subject["state"]["since"], "%H/%M/%S")
		now = datetime.datetime.now()
		comp = (now - old_state) >= datetime.timedelta(minutes = 10)
		# print((now - old_state).total_seconds() > 5*60)
	else:
		old_state = datetime.datetime.strptime(subject["state"]["since"], "%Y/%m/%d").date()
		today = datetime.datetime.now().date()
		comp = old_state < today
		# print(old_state, today, comp)

	if comp:
		# print(today)
		await channel.set_permissions(target, overwrite = None) if channel else None
		coll.update_one(
			{
				"id": target.id
			}, {
				"$set": {
					"hp": subject["lvl"] * 100 + 100,
					"max hp": subject["lvl"] * 100 + 100,
					"state": {
						"value": state,
						"in": None,
						"since": datetime.datetime.now().strftime("%Y/%m/%d")
					}
				}
			}
		)
		print("after")
