import json
import pymongo

m_client = pymongo.MongoClient()
db = m_client.get_database(name = "Test_bot")


async def add_cmd(new_cmd, alt, cmd_desc, cmd_usage):
	# usage_example = re.search("^°(\w| )+$", cmd_usage).group()
	coll = db.get_collection(name = "Commands")
	if not cmd_usage:
		info = None
	else:
		info = cmd_usage.split(";")
	try:
		cmd = {
			"enabled": True,
			"name": new_cmd,
			"alt": alt,
			"description": cmd_desc,
			"usage": "{}" + info[0] if info else "",
			"usage_example": "{}" + info[1].strip() if info else ""
		}
	except IndexError:
		return 0
	except Exception as e:
		print(e.__traceback__.tb_lineno)
		return -1
	coll.insert_one(cmd)
	await get_json()
	await update_lists()
	return 1


async def del_cmd(cmd):
	coll = db.get_collection(name = "Commands")
	state = 0
	print(f"{coll}, {cmd}")
	state = state + coll.delete_one({"name": cmd}).deleted_count
	state = state + coll.delete_one({"alt": cmd}).deleted_count
	coll = db.get_collection(name = "Commands_extended")
	state = state + coll.delete_one({"name": cmd}).deleted_count
	state = state + coll.delete_one({"alt": cmd}).deleted_count
	if not state:
		return f"Command {cmd} not found!"
	await get_json()
	return f"{cmd} successfully deleted"


async def fatal_log(_hash, author, command, time, stack, e, *args):
	coll = db.get_collection(name = "Errors")
	result = coll.insert_one({
		"hash": _hash,
		"time": time.strftime("%Y/%m/%d - %H:%M:%S"),
		"author": (author.name, author.id),
		"command": command,
		"args": args,
		"error": e,
		"traceback": [s.split("\n")[:-1] for s in stack]
	})
	return result.inserted_id


async def find_data(collection, value):
	coll = getattr(db, collection)
	data = coll.find_one({"name": value})
	if data:
		return 1, data
	else:
		data = coll.find_one({"Variations": value})
		if data:
			return 1, data
		else:
			return 0, []


async def get_cmd(mode: int, command: str, helper = None):
	command = command.lower()
	dmp = helper
	print(command + " -> ", end = "")
	coll = db.get_collection(name = "Commands")
	
	while mode:
		# print(coll, mode)
		cmd = coll.find_one({"name": command})
		
		if cmd is not None:
			# print(coll.find_one({"name": command}))
			return 1, command, cmd["enabled"]
		
		else:
			try:
				# print(coll.find_one({"alt": command}))
				cmd = coll.find_one({"alt": command})
				return 1, cmd["name"], cmd["enabled"]
			except (AttributeError, TypeError):
				cmds = db.Lists.find_one({"name": "strings"})["commands"]
				if dmp:
					pos = dmp.match_bitap(cmds, command, 0)
					if pos > -1 and mode == 1:
						if (pos > 0 and cmds[pos - 1] == " ") or pos == 0:
							command = cmds[pos: cmds.find(" ", pos)]
							cmd = coll.find_one({"name": command})
							return 1, command, cmd["enabled"]
						elif mode == 1:
							return 0, command, None
					elif mode == 1:
						return 0, command, None
				elif mode == 1:
					return 0, command, None
			except Exception as e:
				print(e, type(e))
		coll = db.get_collection(name = "Commands_extended")
		mode = mode - 1


async def get_json():
	coll = db.get_collection(name = "Commands")
	commands = {"Commands": []}
	data = coll.find(projection = {'_id': False})
	# print(commands)
	# print(data)
	for d in data.sort('name', 1):
		commands["Commands"].append(d)
		# print(commands)
	with open('commands.json', 'w') as cmd_list:
		json.dump(commands, cmd_list, indent = 4)
	coll = db.get_collection(name = "Commands_extended")
	commands = {"Commands_ext": []}
	data = coll.find(projection = {'_id': False})
	# print(data)
	for d in data.sort('name', 1):
		commands["Commands_ext"].append(d)
	with open('commands_ext.json', 'w') as cmd_list:
		json.dump(commands, cmd_list, indent = 4)


async def update_lists():
	lists = db.Lists
	attacken = " ".join(map(lambda x: x["title"], db.Attacken.find({}, projection = {"_id": False, "title": True})))
	commands = " ".join(map(lambda x: x["name"], db.Commands.find({}, projection = {"_id": False, "name": True})))
	d = {"attacken": attacken, "commands": commands}
	lists.update_one({"name": "strings"}, {"$set": d}, upsert = True)
