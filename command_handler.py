import commands
import commands_ext
import db_handler as db
import discord
import json
import os
import re
import responders as r
import traceback
import utilities as u

from asyncio import TimeoutError
from typing import Union, Optional, Any

prefix = u.data["prefix"]


async def execute(author: discord.Member, command: str, channel: discord.TextChannel,
					client: discord.Client, message: discord.Message, *args: str) -> Union[Optional[int], Any]:
	
	stuff = u.Data(*args, author = author, command = command, channel = channel, client = client, message = message)
	print(f"Executing {command.upper()} with args: {args}")
	
	function = getattr(commands, command, None)
	
	if not function:
		function = getattr(commands_ext, command, None)
	
		if not function:
			await channel.send(embed = r.emb_resp("Whoops", "```This command isn't implemented yet!```", "error_2"))
			return
	
	try:
		result = await function(stuff)
		if result and type(result) != discord.Message:
			try:
				await channel.send(embed = r.emb_resp(result[0], result[1], result[2]))
			except IndexError:
				await channel.send(result[0])
			except Exception as e:
				print("Error in execution: ", e, type(e))
			print("Execution result: ", result)
		elif not result:
			if command == "stop":
				# print("stop bot")
				return 0
			elif command == "restart":
				# print("restart bot")
				return 1
	except discord.Forbidden:
		pass
	except Exception as ex:
		_hash = hash(ex)
		title = "Whoops"
		text = f"Command not executable due to an unexpected error!\n" \
			f"Send this code to the bot in his direct message: ```{str(_hash)}```"
		modules = json.load(open("module-list.json"))
		await channel.send(embed = r.emb_resp(title, text, "error_2"))
		stack = traceback.extract_tb(ex.__traceback__)
		[print(fs) for fs in stack]
		try:
			fs = next(filter(
				lambda x: os.getcwd().split("\\")[-1] in x.filename and x.filename.rsplit("\\")[-1] in modules,
				reversed(stack)
			))
			print(fs)
			code = open(fs.filename, "rb").read().splitlines()
			linenumber = fs.lineno - 1
			print(linenumber)
			prev = code[linenumber - 1]
			print(prev)
			info = b"return await stuff.channel.send('Currently disabled due to error')  # fixme"

			if b"elif" not in code[linenumber]:
				if prev and prev[-1] == b":" and not re.match(b"\t*#", prev):
					count = prev.count(b"\t") + 1
				elif prev:
					count = prev.count(b"\t")
				else:
					count = code[linenumber].count(b"\t")
			else:
				count = code[linenumber + 1].count(b"\t")
				info += code.pop(linenumber)
			
			print(count)
			to_insert = b"\t" * count + info
			print(to_insert)
			code.insert(linenumber, to_insert)

			with open(fs.filename, "wb") as f:
				written = f.write(b"\n".join(code) + b"\n")
				print(written)
			print("xxxxxxx")
			stuff = u.Data(channel = None)
			await commands_ext.reload(stuff)
		except Exception as e:
			print(e)

		if command == "stop":
			print("stop bot")
			_id = 0
		elif command == "restart":
			print("restart bot")
			_id = 1
		else:
			print(ex)
			_id = await db.fatal_log(_hash, author, command, u.datetime.now(), stack.format(), ex.__repr__(), *args)
		return _id


async def command_handler(content, author: discord.Member, channel: discord.TextChannel,
							client: discord.Client, message: discord.Message) -> None:
	# await channel.send(f"{content}, {type(content)}")
	# get identifier
	if content[0] == "^^":
		return
	found, cmd, status = await db.get_cmd(1, content[0], u.dmp)
	print(cmd)
	try:
		arg_string = content[1].split("%%")[0]
		message.content = arg_string
		print(arg_string)
		args = arg_string.split()
	except IndexError:
		message.content = ""
		args = []
		# print(e)
	# print(type(args),args)

	if found:
		if not status:
			await channel.send(embed = r.emb_resp("STOP", "Currently disabled", "error_2"))
			return
		return await execute(author, cmd, channel, client, message, *args)
	else:
		title = "INFO"

		text = f"command **{cmd}** not found, do you want to add it?\nWrite an affirmative word to confirm and" \
			f" add at least one alternative name (being an abbreviation of the command name), as well as a short " \
			f"description separated by semicolons"

		info_msg = await channel.send(embed = r.emb_resp(title, text, "std_info"))

		try:
			coll = db.db.get_collection(name="Lists")
	
			def check(n_c: discord.Message):
				try:
					# print(n_c, content)
					check_1 = n_c.content.lower() in coll.find_one({"name": "declinations"})["declinations"]
					check_2 = n_c.content.split(";")[0].lower() in coll.find_one({"name": "affirmations"})['affirmations']
					return (check_1 or check_2) and n_c.author == author and n_c.channel == channel
				except Exception as check_e:
					print(check_e)
					return False
	
			new_cmd: discord.Message = await client.wait_for('message', timeout=180.0, check=check)
	
			if new_cmd.content.lower() in coll.find_one({"name": "declinations"})["declinations"]:
		
				text = "Ok, not added!"
				await channel.send(embed = r.emb_resp(title, text, "info"), delete_after = 20)
				await info_msg.delete(delay = 10)
				await new_cmd.delete(delay = 10)
				return
	
			try:
				cmd_desc_alt = new_cmd.content.split(";", 1)[1]
			except IndexError:
				description = f"Command {cmd} not added because no alternative name and/or description was provided!"
				await channel.send(embed = r.emb_resp(title, description, "error_2").set_author(name = author.display_name))
				return

			alt, cmd_desc = await u.get_alt(cmd_desc_alt, cmd)
	
			try:
				if not (alt + cmd_desc):
			
					title = "WARNING!"
					text = "At least one alt seems to exist already!"
			
					await channel.send(embed = r.emb_resp(title, text, "error_2"))
					return
			except Exception as e:
				print(e)
	
			text = "Shall the command take any arguments? If yes, add the usage pattern and a usage example, separated " \
				"by semicolon now (without prefix) and react to it with any emoji to confirm. If not, react to this message"

			usage_message = await channel.send(embed = r.emb_resp(title, text, "std_info"))
			usage = ""

			try:
				reaction, user = await client.wait_for('reaction_add', timeout=180.0)
				add = 1
				# if reaction.message.author == client.user:
				if usage_message.id == reaction.message.id:
					text = f"Ok, no arguments added!"
					await channel.send(embed = r.emb_resp(title, text, "info"))
				else:
					if new_cmd.author.id == user.id:
						usage = reaction.message.content
			
					else:
				
						text = f"Command not added because {user.mention} reacted to {new_cmd.author.mention}\'s " \
								f"message! "
				
						await channel.send(embed = r.emb_resp(title, text, "error_2"))
						return
			except TimeoutError:
				await channel.send(embed = r.emb_resp("Timeout!", "You needed too long! No usage Example added!", "error_2"))
				return
	
			print(cmd_desc, alt, usage)
	
			# await add_json(new_cmd,alt,cmd_desc)
			if usage:
				insert = "usage example"
			else:
				insert = ""
				
			response = f"✅ {cmd}; {', '.join(alt)}; {insert} and description added"
	
			if add:
				text = f"User {user} would like to have `{cmd}` with alts: {alt} and description```{cmd_desc}```added!"
				emb = r.emb_resp("New Cmd request!", text, "info")\
					.add_field(name = "usage", value = usage if usage else "None")
				emoji, dm_c = await u.approve(client, embed = emb)
	
				if str(emoji) == "✅":
					await channel.send(embed = r.emb_resp(title, response, "success"))
					res = await db.add_cmd(cmd, alt, cmd_desc, usage)
					if not res:
						await channel.send(embed = r.emb_resp("Error", "You didn't give enough information about the usage!", "error"))
						return
					elif res == -1:
						await channel.send(embed = r.emb_resp("Error", "Something went wrong while adding the command!", "error_2"))
						return
					to_insert = b"async def " + cmd.encode("utf-8") + b"(stuff: u.Data):"
					text = open("commands.py", "rb").read()
					lines = text.splitlines()
					to_replace = list(filter(lambda x: b"def" in x and x > to_insert, lines))
					code = b"\n\treturn await stuff.channel.send('Command not implemented yet', delete_after = 60.0)" \
						b"  # TODO IMPLEMENT\n"
					if not to_replace:
						with open("commands.py", "ba") as f:
							written = f.write(b"\n\n" + to_insert + code)
					else:
						write = text.replace(to_replace[0], to_insert + code + b"\n\n" + to_replace[0])
						written = open("commands.py", "wb").write(write)
					print(written)
					
					await commands_ext.reload(u.Data(channel=channel))
	
				else:
					def check(check_message):
						return check_message.channel == dm_c
			
					message = await client.wait_for('message', check=check)
					text = f"❌ Command wasn't accepted\n```Reason: {message.content}```"
					await channel.send(embed=r.emb_resp(title, text, "error_2"))
		
			print("done")

		except TimeoutError:
			emb = r.emb_resp("Timeout!", "You needed too long!", "error_2").set_author(name = author.display_name)
			try:
				await info_msg.edit(embed = emb)
				await info_msg.delete(delay = 20)
			except discord.Forbidden:
				pass
		except Exception as e:
			print(e, type(e), e.__traceback__.tb_lineno)


async def command_handler_extended(message: discord.Message, channel: discord.TextChannel,
									client: discord.Client) -> None:
	try:
		content = message.content.split(" ", 1)[1]
		print(content)
	except IndexError:
		content = ""
	# print(content)
	found, cmd, status = await db.get_cmd(2, message.content.split(prefix)[0], u.dmp)
	print(cmd)
	if cmd == "help":
		args = ["ext"]
	else:
		args = []
	
	args.extend(content.split())

	if found:
		res = await execute(message.author, cmd, channel, client, message, *args)
	else:
		await channel.send(embed = r.emb_resp("Error", "```Command not found!```", "error_2"))
		res = 0

	try:
		await channel.send(message.author.mention, delete_after = 5)
	except RuntimeError:
		pass
	return res
