import aiohttp
import asyncio
import datetime
import discord
import json
import logging
import os
# import sys

import bot_events as evt
import db_handler as db
import utilities as u

from command_handler import command_handler, command_handler_extended
from music import MusicManager
from threading import Thread
from time import sleep
from pymongo import errors

if "bot_logs" not in os.listdir("."):
	os.mkdir("bot_logs")
today = datetime.date.today()

logger = logging.getLogger('discord')
logger.setLevel(logging.INFO)
handler = logging.FileHandler(filename = f'bot_logs/{today}discord.log', encoding = 'utf-8', mode = 'a')
handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)
started = False
res = 1
loop = u.loop
prefix = u.data["prefix"]
client = discord.Client(loop = loop, intents = discord.Intents.all())
# bot = Thread(target = client.run, args = [u.data["token"]])
# gui = Thread(target = u.Gui, args = [loop, client])
music = Thread(target = MusicManager)


@client.event
async def on_connect():
	mods = []
	with open('module-list.json', 'w') as modules:
		for r, d, f in os.walk(os.getcwd()):
			if r == os.getcwd():
				for file in f:
					if ".py" in file and ".pyc" not in file:
						mods.append(file)
		json.dump(mods, modules, indent=4)


async def func(gld):
	if not gld.me.nick or prefix not in gld.me.nick:
		try:
			await gld.me.edit(nick = client.user.name + f" ({prefix})")
		except Exception as ex:
			print(ex)
	return {
		"id": gld.id,
		"name": gld.name,
		"owner": {
			"id": gld.owner_id,
			"name": gld.owner.name
		}
	}


@client.event
async def on_ready():
	global started
	print("BOT Ready!")
	await db.get_json()
	await db.update_lists()
	coll = db.db.get_collection("Lists")
	try:
		coll.update_one(
			{
				"name": "guilds"
			}, {
				"$set": {
					"name": "guilds",
					"joined": [await func(guild) for guild in client.guilds]
				}
			}, upsert = True
		)
	except AttributeError:
		print("couldn't update guild list!")
	
	if not started:
		started = True
		await evt.change_status(client)


@client.event
async def on_private_channel_create(channel: discord.DMChannel):
	print(channel)
	try:
		user: discord.User = channel.recipient
		try:
			coll = db.db.get_collection(name = "data")
			member = coll.find_one({"id": user.id})
			if not member:
				doc = {"name": user.name, "id": user.id, "log": True, "messages": []}
				coll.insert_one(doc)
			elif not member.get("messages") or not member.get("log") or not member.get("name"):
				member.update({"name": user.name, "log": True, "messages": []})
				coll.update_one({"id": user.id}, {"$set": member})
		except db.pymongo.errors.DuplicateKeyError:
			return
	except Exception as ex:
		print(ex)


@client.event
async def on_message(message: discord.Message):
	# print(u.datetime.now().strftime("%S"))
	if message.is_system():
		return
	# if not (message.attachments or message.embeds):
	if not message.author.bot:
		await evt.dm_log(message)

	if client.user.mentioned_in(message) and not message.mention_everyone:
		return await message.channel.send(f"Hi {message.author.mention}!\nMy prefix is `{prefix}`")

	if message.author != client.user:
		# print(message.author, message.clean_content, message.guild, message.channel)
		args = await evt.on_msg(message, client)

		if args:
			# print("-"*10, "\n", args, "\n", "-"*10)
			
			if args[0] != "Admin":
				tmp = await command_handler(args[1], args[2], args[3], client, message)
			
			else:
				tmp = await command_handler_extended(message, args[1], client)
			global res
			res = tmp if tmp is not None else 1
	# else:
	# 	print(message.embeds)


@client.event
async def on_user_update(before: discord.User, after: discord.User):
	owner = client.get_user(u.data["owner"])
	dm_c = owner.dm_channel

	if not dm_c:
		dm_c = await owner.create_dm()
	x = before.name
	y = after.name
	if x != y:
		msg = f"{x}\n\n{(y, after.id)}"
		try:
			common_guilds = [guild.name for guild in client.guilds if before in guild.members]
			msg += f"\n{common_guilds}"
		except Exception as ex:
			print(ex)
		await dm_c.send(msg)
	else:
		print("Other changes at", x, "'s profile.")


@client.event
async def on_typing(channel, user: discord.Member, when):
	try:
		if type(channel) is not discord.DMChannel and user.status == discord.Status.offline:
			pass
			# print(channel)
			# print(f"{user.name.upper()} IS OFFLINE!", when.strftime("%S"))
	except Exception as ex:
		print(ex)


@client.event
async def on_voice_state_update(member: discord.Member, before: discord.VoiceState, after: discord.VoiceState):
	if member == client.user:
		return
	# print("before:", member, before, end = "")
	coll = db.db.get_collection("data")
	
	if before.channel is None:
		# if member == client.user:
		# 	temp = list(u.music_queues.keys())
		# 	for q in filter(lambda x: u.re.search(after.channel.id, x), temp):
		# 		key = after.channel.guild.voice_client.token + after.channel.id + q.split(after.channel.id)[1]
		# 		u.music_queues[key] = u.music_queues.pop(q)
		
		# print(" no channel change")
		coll.update_one({"id": member.id}, {"$set": {"channel": after.channel.id}}, upsert = True)
		
	elif after.channel != before.channel and after.channel is not None:
		# print([log async for log in member.guild.audit_logs()])
		channel = client.get_channel(coll.find_one({"id": member.id})["channel"])
		
		if after.channel == channel:
			# print(" pass")
			return
		
		elif not coll.find_one({"id": member.id, "re-move": True}):
			# print(" disabled")
			return
		
		else:
			# print(" move!")
			# print(after)
			try:
				await member.move_to(before.channel)
			except (discord.HTTPException, TypeError):
				pass
	else:
		# print(" pass")
		members = before.channel.members
		members = list(filter(lambda x: not x.bot, members))
		v_c = before.channel.guild.voice_client
		if (
			not members or len(list(filter(lambda x: x.voice.self_deaf or x.voice.deaf, members))) == len(members)
		) and v_c and v_c.is_playing():
			v_c.pause()
			me = before.channel.guild.me
			nick = me.nick
			await me.edit(nick = "[PAUSED] " + nick)
			

@client.event
async def on_guild_join(guild: discord.Guild):
	print(guild)
	try:
		await guild.me.edit(nick = client.user.name + f" ({prefix})")
	except Exception as ex:
		print(ex)
	coll = db.db.get_collection("Lists")
	coll.update_one({
		"name": "guilds"
	}, {
		"$addToSet": {
			"joined": {
				"id": guild.id,
				"guild": guild.name,
				"owner": {
					"id": guild.owner_id,
					"name": guild.owner.name
				}
			}
		}
	}, upsert = True)


# gui.start()
print("running!")
music.start()
# bot.start()
try:
	print("trying connect")
	client.run(u.data["token1"])
except (discord.DiscordServerError, discord.HTTPException, aiohttp.ClientConnectorError) as e:
	print(e)
	sleep(30)
	# evt.fill_queue("STOP")
	asyncio.run(MusicManager.stop(full = True))
	print("restart")
exit(res)
# sys.exit(res)
