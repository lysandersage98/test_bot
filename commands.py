import aiohttp
import bs4
import discord
import googleapiclient.discovery
import html2text
# import io
import inspect
import json
import math
import random
import re

from asyncio import Lock, TimeoutError  # , sleep
from commands_ext import reload
from datetime import datetime, timedelta
from humanfriendly.tables import format_pretty_table
from music import Player, MusicManager, VoiceClientCopy
from PIL import Image, ImageFont, ImageDraw
from scipy import stats

import db_handler as db
import profiles as p
import responders as r
import utilities as u

font = ImageFont.truetype(font = "other stuff/hymmnos.ttf", size = 40)
lock = Lock()
prefix = u.data["prefix"]
time_is_settings = "?c=d3l1_3F_3j1_3Y1_3WXtH2i2sXfmtsXc0Xo120Xz1Xa1Xb51ea29.4e4185.28571f.2d99db.80265.1bb85e" \
	".1c3b23Xw0Xv20200430Xh0Xi1XZ1XmXuXB0&l=en "
time_formats = {
	"\d{1,2}:\d{1,2}:\d{1,2}_\d{1,2}.\d{1,2}.\d{4}": "%H:%M:%S_%d.%m.%Y",
	"\d{1,2}:\d{1,2}_\d{1,2}.\d{1,2}.\d{4}": "%H:%M_%d.%m.%Y",
	"\d{1,2}.\d{1,2}.\d{4}_\d{1,2}:\d{1,2}:\d{1,2}": "%d.%m.%Y_%H:%M:%S",
	"\d{1,2}.\d{1,2}.\d{4}_\d{1,2}:\d{1,2}": "%d.%m.%Y_%H:%M"
}

a = 3
b = 25 / 12 + 11 / 6 * math.cos(1 / 3 * math.atan((36 * math.sqrt(1077)) / 613))
c = 2 * b - a
d = 3 * b - 2 * a


async def abookofcreatures(stuff: u.Data):
	channel = stuff.channel
	args = stuff.args
	coll = db.db.get_collection(name = "Monsters")
	url = "https://abookofcreatures.com/"
	
	def check(check_html):
		return not (re.search("references", check_html, re.IGNORECASE) and
			re.search("entry-title", check_html, re.IGNORECASE)) or re.search("oops", check_html, re.IGNORECASE)
	
	def get_story(story_text: str):
		left = re.search(r"\[*!\[+(.*\n?){0,4}\n\n", story_text)
		le = left.span()[1]
		ri = re.search(r".*References", story_text).span()[0] - 1
		print("-----\n", ri, "\n-----")
		g_story = story_text[le:ri]
		# story = text[l:text.find("**References**")-1]
		ret_story = g_story.split("\n\n")
		return ret_story
	
	def get_vars(var_text):
		desc_vars = re.search("\*\*Variations:\*\*.*\n?\n", var_text, re.IGNORECASE)
		if desc_vars:
			desc_vars = desc_vars.group()[:-1]
		else:
			desc_vars = "No Variations"
		print("-----\n", desc_vars, "\n-----")
		return desc_vars
	
	def rand_date():
		start = datetime.strptime("2015/03/15", "%Y/%m/%d")
		end = datetime.now()
		return start + (end - start) * random.random()
	
	if not args:
		async with channel.typing():
			while True:
				date = rand_date().strftime("%Y/%m/%d")
				
				async with aiohttp.request("GET", url + date) as resp:
					html = await resp.text()
					
					if check(html):
						# print(html.split("<"))
						# print(date)
						pass
					else:
						print("-" * 5, date, "-" * 5)
						break
			
			title = html[html.find("entry-title"):html.find("entry-content\"")]
			title = title[title.rfind("k\">") + 3:title.find("</a")]
			print(title)
			
			text = html2text.html2text(html, url + date)
			# print(text)
			desc = get_vars(text)
			
			embed = r.emb_resp(title, desc, "success", url + date)
			story = get_story(text)
			
			img = re.search("data-orig-file.*data-orig-size", html).group()
			img = re.search("\".*\"", img)
			image = img.group()[1:-1]
			# await channel.send(image)
			# print(story)
			# print("\n",story,"\n")
			# print(len(story))
			# print(len(story))
			# print(story)
			
			embed = await u.separate(story, embed)
			embed.set_image(url = image)
			
			try:
				coll.insert_one(
					{"name": title, "Variations": desc[:-1], "url": url + date, "image-src": image, "story": story})
			
			except Exception as e:
				print(e)
	
	else:
		print("args")
		if args[0] == "info":
			title = "Page info"
			text = ""
			url = url + "/2015/03/14/"
			embed = r.emb_resp(title, text, "info", url)
		
		elif args[0] == "list":
			data = coll.find().sort('name', 1)
			text = []
			
			for entry in data:
				text.append(f"- {entry['name']}\n")
			embed = r.emb_resp("Discovered Creatures", f"```{''.join(text)}```", "info")
		
		else:
			url = url + args[0]
			found, data = await db.find_data("Monsters", args[0].title())
			
			if found:
				title = data["name"]
				desc = data["Variations"]
				url = data["url"]
				embed = r.emb_resp(title, desc, "success", url)
				embed = await u.separate(data["story"], embed)
				image = data["image-src"]
				embed.set_image(url = image)
			
			else:
				async with aiohttp.request("GET", url) as resp:
					html = await resp.text()
					
					if not check(html):
						title = html[html.find("entry-title"):html.find("entry-content\"")]
						title = title[title.rfind("k\">") + 3:title.find("</a")]
						
						text = html2text.html2text(html, url)
						desc = get_vars(text)
						embed = r.emb_resp(title, desc, "success", url)
						story = get_story(text)
						img = re.search("data-orig-file.*data-orig-size", html).group()
						img = re.search("\".*\"", img)
						image = img.group()[1:-1]
						embed = await u.separate(story, embed)
						embed.set_image(url = image)
						if len(story) < 6000:
							try:
								creature = {
									"name": title,
									"Variations": desc[:-1],
									"url": url,
									"image-src": image,
									"story": story
								}
								coll.insert_one(creature)
							except Exception as e:
								print(e)
					
					else:
						embed = r.emb_resp("Error", "The Creature you searched wasn't found.", "error_2")
	try:
		await channel.send(embed = embed)
	except discord.HTTPException as e:
		if e.code == 50035:
			embed.clear_fields()
			embed.add_field(name = "Error", value = "Text too long to be displayed")
			await channel.send(embed = embed)
		else:
			print(e)


async def add_to_my_playlist(stuff: u.Data):
	coll = db.db.get_collection("User_Songs")
	channel = stuff.channel
	guild = channel.guild
	playlists = stuff.message.content.split(",")
	author = stuff.author
	valid = False
	
	if not guild:
		return "Nope", "Not possible here", "error"
	v_c = guild.voice_client
	if not v_c:
		return "Try again when i am actually playing music", "", "error"
	
	current = Player.get_current(guild.id)
	if not current:
		return "Weird", "There should be music but isn't", "error_2"

	for playlist in playlists:
		if playlist:
			valid = True
			coll.find_one_and_update(
				{
					"user": author.id,
					"name": playlist.strip() if playlist else "default"
				}, {
					"$addToSet": {
						"songs": [current["link"], current["title"]]
					}
				}, upsert = True
			)
			desc = f"`{current['title']}` to `{playlist}`!"
			await channel.send(embed = r.emb_resp("Added", desc, "ok"))
	
	if not valid:
		return "Error", "No playlist given!", "error"


async def addalternative(stuff: u.Data):
	channel = stuff.channel
	client = stuff.client
	args = stuff.args
	
	coll = db.db.get_collection(name = "Commands")
	title = "Info"
	response = ""
	color = "error_2"
	
	try:
		alt = args[1]
		found, cmd, enabled = await db.get_cmd(1, args[0])
		
		if not found:
			alt = args[0]
			found, cmd, enabled = await db.get_cmd(1, args[1])
			
			if not found:
				response = "Command not found!"
				color = "error"
		
		if alt != cmd:
			if found:
				response = f"✅ New alternative name {alt} for {cmd} added!"
				color = "success"
				text = f" ```{alt}``` for cmd ```{cmd}```"
				emoji, dm_c = await u.approve(client, r.emb_resp("New alt requested!", text, "info"))
				
				if str(emoji) == "✅":
					coll.update_one({"name": cmd}, {"$addToSet": {"alt": alt}})
				
				else:
					def check(check_message):
						return check_message.channel == dm_c
					
					message = await client.wait_for('message', check = check)
					response = f"❌ New alt \"{alt}\" wasn't approved\n```Reason: {message.content}```"
					color = "error_2"
		
		else:
			response = f"{alt} is already the full command name!!"
			color = "error_2"
		await channel.send(embed = r.emb_resp(title, response, color))
	
	except Exception as e:
		print(e)
		msg = await channel.send(embed = r.emb_resp("Error", str(e), "error_2"))
		return await msg.delete(delay = 120)


async def addplaying(stuff: u.Data):
	channel = stuff.channel
	client = stuff.client
	args = stuff.args
	
	coll = db.db.get_collection(name = "Lists")
	title = "Info"
	await channel.send(embed = r.emb_resp(title, "The suggestion will be sent to the owner!", "info"))
	emoji, dm_c = await u.approve(client, r.emb_resp('New status request!', f'\"{" ".join(args)}\"', 'info'))
	
	if str(emoji) == "✅":
		
		try:
			coll.update_one({"name": "stati"}, {"$addToSet": {"stati": " ".join(args)}})
			text = f"✅ New Status \"{' '.join(args)}\" successfully added!"
			color = "success"
		
		except Exception as e:
			print(e)
			msg = await channel.send(embed = r.emb_resp("Error", str(e), "error_2"))
			return await msg.delete(delay = 120)
	
	else:
		def check(check_message):
			return check_message.channel == dm_c
		
		message = await client.wait_for('message', check = check)
		text = f"❌Status \"{' '.join(args)}\" wasn't approved\n```Reason: {message.content}```"
		color = "error_2"
	
	await channel.send(embed = r.emb_resp(title, text, color))


async def binaryconverter(stuff: u.Data):
	bases = {
		"bin": ("0b", 2),
		"oct": ("0o", 8),
		"hex":  ("0x", 16)
	}
	channel = stuff.channel
	print(stuff.message.mentions, stuff.message.raw_mentions)
	s = stuff.message.clean_content.strip()
	# s = " ".join(stuff.args)
	match = re.search("(--hex)|(--oct)", s, re.I)
	formatting = "bin"
	
	if match:
		s = "".join(s.split(match.group())).strip()
		formatting = match.group()[2:]
		
	try:  # to convert from binary to text
		res = "".join(
			list(
				map(
					lambda x: chr(
						int(
							bases[formatting][0] + x,
							bases[formatting][1]
						)
					) if len(x) else None,
					s.split()
				)
			)
		)
		encrypt = True
	except ValueError:  # convert from text to binary
		res = " ".join([eval(formatting)(ord(x))[2:] for x in s])
		encrypt = False
		
	title = f"Converted from {formatting} to normal text" if encrypt else f"Convert from normal text to {formatting}"
	await channel.send(embed = r.emb_resp(title, res, "success"))


async def changedescription(stuff: u.Data):
	args = stuff.args
	client = stuff.client
	author = stuff.author
	channel = stuff.channel
	
	coll = db.db.get_collection(name = "Commands")
	title = "Info"
	
	try:
		cmd = args.pop(0)
		desc = " ".join(args)
		found, cmd, enabled = await db.get_cmd(1, cmd)
		
		if not found:
			response = f"Command {cmd} not found!"
			color = "error"
		
		else:
			response = f"✅ New description for {cmd} added!"
			color = "success"
			text = f" ```{desc}``` for cmd ```{cmd}```"
			embed = r.emb_resp("New description requested!", text, "info").set_author(name = author.name)
			emoji, dm_c = await u.approve(client, embed)
			
			if str(emoji) == "✅":
				coll.update_one({"name": cmd}, {"$set": {"description": desc}})
			else:
				def check(check_message):
					return check_message.channel == dm_c
				
				message = await client.wait_for('message', check = check)
				response = f"❌ Description `\"{desc}\"` wasn't approved\n```Reason: {message.content}```"
				color = "error_2"
		await channel.send(embed = r.emb_resp(title, response, color))
		stuff.args = []
		return await reload(stuff)
	
	except Exception as e:
		print(e)
		msg = await channel.send(embed = r.emb_resp("Error", str(e), "error_2"))
		return await msg.delete(delay = 120)


async def changeusage(stuff: u.Data):
	channel = stuff.channel
	client = stuff.client
	author = stuff.author
	args = stuff.args
	
	coll = db.db.get_collection(name = "Commands")
	title = "Info"
	
	try:
		# cmd = args.pop(0)
		cmd = args[0]
		usage = " ".join(args)
		found, cmd, enabled = await db.get_cmd(1, cmd)
		
		if not found:
			response = f"Command {cmd} not found!"
			color = "error"
		
		else:
			response = f"✅ New usage-hint for {cmd} added!"
			color = "success"
			text = f" ```{usage}``` for cmd ```{cmd}```"
			embed = r.emb_resp("New usage-hint requested!", text, "info").set_author(name = author.name)
			emoji, dm_c = await u.approve(client, embed)
			
			if str(emoji) == "✅":
				coll.update_one({"name": cmd}, {"$set": {"usage": "{}" + usage}})
			else:
				def check(check_message):
					return check_message.channel == dm_c
				
				message = await client.wait_for('message', check = check)
				response = f"❌ Usage-hint \"{usage}\" wasn't approved\n```Reason: {message.content}```"
				color = "error_2"
		await channel.send(embed = r.emb_resp(title, response, color))
		stuff.args = []
		return await reload(stuff)
	
	except Exception as e:
		print(e)
		msg = await channel.send(embed = r.emb_resp("Error", str(e), "error_2"))
		return await msg.delete(delay = 120)


async def changeusageexample(stuff: u.Data):
	channel = stuff.channel
	client = stuff.client
	author = stuff.author
	args = stuff.args
	
	coll = db.db.get_collection(name = "Commands")
	title = "Info"
	
	try:
		# cmd = args.pop(0)
		cmd = args[0]
		usage = " ".join(args)
		found, cmd, enabled = await db.get_cmd(1, cmd)
		
		if not found:
			response = f"Command {cmd} not found!"
			color = "error"
		
		else:
			response = f"✅ New usage example for {cmd} added!"
			color = "success"
			text = f" ```{usage}``` for cmd ```{cmd}```"
			embed = r.emb_resp("New usage example requested!", text, "info").set_author(name = author.name)
			emoji, dm_c = await u.approve(client, embed)
			
			if str(emoji) == "✅":
				coll.update_one({"name": cmd}, {"$set": {"usage_example": "{}" + usage}})
			else:
				def check(check_message):
					return check_message.channel == dm_c
				
				message = await client.wait_for('message', check = check)
				response = f"❌ Usage example \"{usage}\" wasn't approved\n```Reason: {message.content}```"
				color = "error_2"
		await channel.send(embed = r.emb_resp(title, response, color))
		stuff.args = []
		return await reload(stuff)
	
	except Exception as e:
		print(e)
		msg = await channel.send(embed = r.emb_resp("Error", str(e), "error_2"))
		return await msg.delete(delay = 120)


async def chisquare(stuff: u.Data):
	channel = stuff.channel
	content = stuff.message.content.split(";")
	observed = json.loads(content[0])
	try:
		_p = float(content[1])
		_p = 1 - _p / 200
	except IndexError:
		return "Error", "Provide a p value, separated by semicolon", "error"
	res = 0
	total = sum(map(sum, observed))
	# print(total)
	cols, rows = 0, 0
	for vals in observed:
		col_sum = sum(vals)
		cols += 1
		if not rows:
			rows = len(vals)
		# print(row_sum)
		for val, i in zip(vals, range(len(vals))):
			row_sum = sum(list(zip(*observed))[i])
			# print(col_sum)
			expected = col_sum * row_sum / total
			# print(expected)
			res += (val - expected)**2 / expected
	
	# def calc_p(x, inner_df):  # "pochisq(x, df)"from https://www.mathsisfun.com/data/images/chi-square.js
	# 	bigx = 20
	# 	y = None
	# 	log_sqrt_pi = 0.5723649429247000870717135
	# 	i_sqrt_pi = 0.5641895835477562869480795
	#
	# 	def poz(temp_z):
	# 		z_max = 6
	# 		if not temp_z:
	# 			temp_x = 0
	# 		else:
	# 			temp_y = 0.5 * abs(temp_z)
	# 			print(temp_y)
	# 			if temp_y >= z_max * 0.5:
	# 				temp_x = 1
	# 			elif temp_y < 1:
	# 				temp_w = temp_y**2
	# 				temp_x = ((((((((0.000124818987 * temp_w - 0.001075204047) * temp_w + 0.005198775019) * temp_w - 0.019198292004) * temp_w + 0.059054035642) * temp_w - 0.151968751364) * temp_w + 0.319152932694) * temp_w - 0.531923007300) * temp_w + 0.797884560593) * temp_y * 2.0
	# 			else:
	# 				temp_y -= 2
	# 				temp_x = (((((((((((((-0.000045255659 * temp_y + 0.000152529290) * temp_y - 0.000019538132) * temp_y - 0.000676904986) * temp_y + 0.001390604284) * temp_y - 0.000794620820) * temp_y - 0.002034254874) * temp_y + 0.006549791214) * temp_y - 0.010557625006) * temp_y + 0.011630447319) * temp_y - 0.009279453341) * temp_y + 0.005353579108) * temp_y - 0.002141268741) * temp_y + 0.000535310849) * temp_y + 0.999936657524
	# 		return (temp_x + 1) * 0.5 if temp_z > 0 else (1 - temp_x) * 0.5
	#
	# 	def ex(temp_x):
	# 		return f"0; because χ² too big" if temp_x < -bigx else math.e**temp_x
	# 		# return math.e**temp_x
	#
	# 	if x <= 0.0 or inner_df < 1:
	# 		return 1
	# 	p_a = 0.5 * x
	# 	even = not inner_df & 1
	# 	if inner_df > 1:
	# 		y = ex(-p_a)
	# 	s = y if even else 2 * poz(-math.sqrt(x))
	# 	if inner_df > 2:
	# 		x = 0.5 * (inner_df - 1.0)
	# 		z = 1 if even else 0.5
	# 		if p_a > bigx:
	# 			e = 0 if even else log_sqrt_pi
	# 			p_c = math.log(p_a)
	# 			while z <= x:
	# 				e = math.log(z) + e
	# 				s += ex(p_c * z - p_a - e)
	# 				z += 1
	# 			return s
	# 		else:
	# 			e = 1 if even else i_sqrt_pi / math.sqrt(p_a)
	# 			p_c = 0
	# 			while z <= x:
	# 				e = e * p_a / z
	# 				p_c = p_c + e
	# 				z += 1
	# 			return p_c * y + s
	# 	else:
	# 		return s
	df = (rows-1) * (cols-1)
	# chi_p = calc_p(res, df)
	# thx https://stackoverflow.com/questions/11725115/p-value-from-chi-sq-test-statistic-in-python
	chi_p = stats.chi2.sf(res, df)
	t_crit = stats.chi2.ppf(_p, df = df)
	return await channel.send(f"χ² = {res}\np = {chi_p}\nt_crit_{_p} = {t_crit}")


async def comparetime(stuff: u.Data):
	channel = stuff.channel
	content = stuff.message.content.split(" ", 2)
	
	time, place = content[0], content[1]
	if len(content) < 3:
		places = []
		# return "Error", "No time or place(s) given!", "error"
	else:
		places = list(map(lambda x: x.strip(), content[2].split(",")))
	# print(time, place_s)
	base = "https://www.time.is/"
	time += datetime.today().strftime("_%d.%m.%Y") if "_" not in time else ""
	for item in time_formats:
		if re.match(item, time):
			fmt = time_formats[item]
			break
	else:
		return "Error", "Invalid time format!", "error"
		
	time_new = datetime.strptime(time, fmt).strftime("%H%M_%d_%B_%Y")
	# print(time)
	time_in = f"{time_new}_in_{place}/{'/'.join(places)}"
	print(time_in)
	url = base + time_in
	
	async with aiohttp.request("GET", url + time_is_settings) as resp:
		if resp.status == 400:
			return "Error", f"Invalid syntax, check {prefix}help comparetime!", "error"
		text = await resp.text()
		html = bs4.BeautifulSoup(text, "html.parser")
		infos = html.find(attrs = {"class": "w90"})
		# print(infos.prettify())
		# print(infos.find_all("h2"))
		
		# title = "Result of your query " + f"`{time} in {place.title()}`"
		title = ''.join(infos.find_all('h1')[0].strings)
		embed = r.emb_resp(title, "", "success", url = url.replace(" ", "_"))
		
		diff = infos.find(attrs = {'class': 'nicediff'}).string
		desc = "```Difference between now and the requested time:``````" + diff + "```"
		
		if places:
			append = "\n```Comparison with the other places:```"
			n_places = infos.find(attrs = {"class": "tbx leftfloat"}).find_all("h2")
			for pl in n_places:
				loc_info = list(pl.strings)
				name = loc_info.pop(0)
				if name not in title:
					value = f'```{"".join(loc_info)}```'
					embed.add_field(name = name, value = value)
		
		else:
			append = ""
		
		embed.description = desc + append
	
	await channel.send(embed = embed)


async def confidence_interval(stuff: u.Data):
	args = stuff.message.content.split(";")
	try:
		alpha, mean, sigma, n = args
		variance = None
	except ValueError as e:
		try:
			alpha, data, _type = args
		except ValueError:
			return "Invalid amount of arguments!", e.__repr__(), "error"
		
		alpha = float(alpha)
		data = json.loads(data)
		n = len(data)
		mean = sum(data)/n
		
		if _type.strip() == "population":
			div = n
		elif _type.strip() == "sample":
			div = n - 1
		else:
			return "Invalid type!", "Must be either 'population' or 'sample'!", "error"
		variance = sum(map(lambda x: (x - mean)**2, data)) / div
		sigma = math.sqrt(variance)

	except Exception as e:
		return "Error!", e.__repr__(), "error"
	
	alpha = float(alpha)
	mean = float(mean)
	sigma = float(sigma)
	n = int(n)
	if not variance:
		variance = sigma**2
	s_e = sigma / math.sqrt(n)
	
	if alpha <= 0 or mean <= 0 or sigma <= 0 or n <= 0:
		return "Error!", "Must be positive values!", "error"
	
	# original functions taken from js on https://goodcalculators.com/student-t-value-calculator/
	
	def fln(val):
		return math.log(val) / math.log(10)
	
	def dplcs(val):
		return math.floor(val) if val > 0 else math.ceil(val)
	
	def todcml(val):
		return abs(dplcs(fln(abs(val)) - 7))
	
	def fresdc(fresdc_t, fresdc_r):
		fresdc_t *= 10**fresdc_r
		fresdc_t = round(fresdc_t)
		return fresdc_t / 10**fresdc_r
	
	def fdecms(val):
		return fresdc(val, todcml(val)) if val else 0
	
	def tprdf(tprdf_t, tprdf_r):
		o = tprdf_t - 2
		v = math.atan2(tprdf_r / math.sqrt(tprdf_t), 1)
		n_calc = math.cos(v)**2
		s = 1
		while o >= 2:
			o -= 2
			s = 1 + (o - 1) / o * n_calc * s
		if tprdf_t % 2 == 0:
			calc_e = 0.5
			calc_a = math.sin(v) / 2
		else:
			calc_e = 0.5 + v / math.pi
			calc_a = 0 if 1 == tprdf_t else math.cos(v) * math.sin(v) / math.pi
		return max(0, 1 - calc_e - calc_a * s)
	
	def tdfu(tdfu_t, tdfu_r):
		def tdf(val):
			r_calc = -math.log(4 * val * (1 - val))
			e_calc = math.sqrt(r_calc * (1.570796288 + r_calc * (.03706987906 + r_calc * (-.0008364353589 + r_calc * (-.0002250947176 + r_calc * (6841218299e-15 + r_calc * (5824238515e-15 + r_calc * (-104527497e-14 + r_calc * (8.360937017e-8 + r_calc * (-3.231081277e-9 + r_calc * (3.657763036e-11 + 6.936233982e-13 * r_calc)))))))))))
			return -e_calc if tdfu_t > 0.5 else e_calc
		
		if 0.5 > tdfu_r:
			return -tdfu(tdfu_t, 1 - tdfu_r)
		calc_e = tdf(tdfu_r)
		calc_a = calc_e**2
		calc_l = (((((27 * calc_a + 339) * calc_a + 930) * calc_a - 1782) * calc_a - 765) * calc_a + 17955) / 368640
		calc_n = ((((79 * calc_a + 776) * calc_a + 1482) * calc_a - 1920) * calc_a - 945) / 92160
		s = (((3 * calc_a + 19) * calc_a + 17) * calc_a - 15) / 384
		o = ((5 * calc_a + 16) * calc_a + 3) / 96
		u_calc = (calc_a + 1) / 4
		calc_d = calc_e * (1 + (u_calc + (o + (s + (calc_n + calc_l / tdfu_t) / tdfu_t) / tdfu_t) / tdfu_t) / tdfu_t)
		if tdfu_t <= fln(tdfu_r)**2 + 3:
			while True:
				f = tprdf(tdfu_t, calc_d)
				calc_c = tdfu_t + 1
				i = (f - tdfu_r) / math.exp(
					(calc_c * math.log(calc_c / (tdfu_t + calc_d * calc_d)) + math.log(tdfu_t / calc_c / 2 / math.pi) - 1 + (
						1 / calc_c - 1 / tdfu_t) / 6) / 2)
				calc_d += i
				h = fresdc(i, abs(dplcs(fln(abs(calc_d)) - 6)))
				if calc_d and 0 != h:
					break
		return calc_d
	
	def calc_t(pre_df, pre_sig):
		df = pre_df - 1
		sig = pre_sig / 100
		return fdecms(tdfu(df, sig / 2))
	
	t = calc_t(n, alpha)
	temp = sigma / math.sqrt(n)
	confidence = [mean - t * temp, mean + t * temp]
	return "Results", \
		f"```python\nα: {alpha}%\nmean: {mean}\nσ: {sigma}\nn: {n}\nVariance: {variance}\nStandard Error: {s_e}```" \
		f"{100-alpha}% confidence interval: {confidence}", \
		"success"


async def connect(stuff: u.Data):
	# options = {
	# 	"ALL": None,
	# 	"SHOW": None,
	# 	"PLAYLIST": None,
	# 	"SHUFFLE": None
	# }
	author = stuff.author
	channel = stuff.channel
	client = stuff.client
	args = stuff.args
	play_all = 0
	sh = False
	show = False
	
	options = {
	
	}
	before_options = {
		"start": (lambda x: " -ss 0" + str(timedelta(seconds = int(x)))),
		"end": (lambda x: " -to 0" + str(timedelta(seconds = int(x))))
	}
	ffmpeg_options = ""
	ffmpeg_before_options = ""
	
	guild: discord.Guild = channel.guild
	if not guild:
		return "Info", "Only possible in a guild", "error"
	if not author.voice:
		return "Nope", "Only allowed to do that when you are in a voice channel", "error"
	msg = await channel.send(embed = r.emb_resp("Searching!", "", "info"))
	# matches = re.search("|".join(options.keys()), " ".join(args))
	if len(args) == 0:  # Wrong usage
		return await msg.edit(embed = r.emb_resp("Error", "No search value provided!", "error"))
	print(args)
	
	matches = re.finditer(" (--(\S*)) (\S*)", stuff.message.content)
	invalid_option = []
	for match in matches:
		# print(match.group(1))
		# print(match.group(2))
		# print(match.group(3))
		try:
			args.remove(match.group(1))
			args.remove(match.group(3))
			try:
				ffmpeg_options += options[match.group(2)](match.group(3))
			except KeyError:
				try:
					ffmpeg_before_options += before_options[match.group(2)](match.group(3))
				except KeyError:
					invalid_option.append(match.group(2))
		except Exception as e:
			await msg.edit(content = "Something went wrong!" + str(e))
	if invalid_option:
		desc = "\n".join(invalid_option) + f"\nHas to be in {list(options.keys())} or {list(before_options.keys())}"
		embed = r.emb_resp("Invalid options!", desc, "error")
		await channel.send(embed = embed)

	# print(ffmpeg_before_options)

	if "SHUFFLE" in args:
		print("pre-shuffle added playlist")
		sh = True
		args.remove("SHUFFLE")
		print(args)
		
	if "ALL" in args:  # Only when connecting (really?)
		print("play_all")
		args.remove("ALL")
		append = playlist = ""
		if args:
			play_all = " ".join(args)
		else:
			play_all = "default"
		args.clear()
	elif "PLAYLIST" in args:
		print("playlist")
		playlist = True
		args.remove("PLAYLIST")
		append = "&sp=EgIQAw%253D%253D"
	else:
		playlist = False
		append = "&sp=EgIQAw%253D%253D"
		
	if args:
		if "youtu" not in args[0]:  # Check if link given
			# return "Currently not working!", "", "error_2"
			search = "+".join(args)
			print(search + append)
			api_info = u.data["api_info"]
			youtube = googleapiclient.discovery.build(api_info[0], api_info[1], developerKey = api_info[2])
			request = youtube.search().list(
				part = "snippet",
				q = " ".join(args),
				type = "video" if not playlist else "playlist"
			)
			response = request.execute()
			if playlist:
				_type = ("list", "playlist")
			else:
				_type = ("v", "video")
				
			url = u.concat(_type[0], response["items"][0]["id"][f"{_type[1]}Id"])
			res = f"Found {_type[1]}!\n"
			await channel.send(res + url)
			# async with aiohttp.request("GET", "https://www.youtube.com/results?search_query=" + search + append) as resp:
			# 	try:
			# 		html = await resp.text()
			# 		tag_text = bs4.BeautifulSoup(html, "html.parser")
			# 		print(tag_text.prettify())
			# 		print(tag_text.find_all("script"))
			# 		definition = next(filter(lambda x: 'ytInitialData' in str(x), tag_text.find_all("script"))).string
			# 		# print(definition)
			# 		definition = definition.split("= ", 1)[1]
			# 		# print(definition[-200:])
			# 		# right = re.search(";\s*window", definition)
			# 		# print(right.group())
			# 		definition = json.loads(definition[:-1])
			# 		# print(definition)
			# 		contents = definition["contents"]["twoColumnSearchResultsRenderer"]["primaryContents"]["section"
			# 			"ListRenderer"]["contents"]
			# 		# print(contents)
			# 		# item_list = contents[0]["itemSectionRenderer"]["contents"]
			# 		video_list = list(filter(lambda x: f"{'playlist' if playlist else 'video'}Renderer" in json.dumps(x), contents))
			# 		item_list = video_list[0]["itemSectionRenderer"]["contents"]
			# 		item = next(filter(lambda x: "correctedQuery" not in json.dumps(x), item_list))
			#
			# 		if not playlist:
			# 			# print(video)
			# 			# res = tag_text.find(attrs = {"class": "yt-lockup yt-lockup-tile yt-lockup-video vve-check clearfix"})
			# 			url = u.concat("v", item["videoRenderer"]["videoId"])  # res["data-context-item-id"])
			# 			await channel.send("Found song!\n" + url)
			# 		else:
			# 			# res = tag_text.find(attrs = {"class": "yt-lockup yt-lockup-tile yt-lockup-playlist vve-check clearfix"})
			# 			# print(res)
			# 			# url = "https://www.youtube.com" + res.findAll("a")[-1]["href"]
			# 			# print(item)
			# 			url = u.concat("list", item["playlistRenderer"]["playlistId"])
			# 			await channel.send("Found playlist!\n" + url)
			# 		resp.close()
			# 	except Exception as e:
			# 		if str(e) == "videoRenderer":
			# 			return "Unexpected error", "But an error that just randomly appears sometimes"\
			# 				"\nTry again and it should work."\
			# 				"\nIf it appears a 2nd time in a row, notify the owner.", "error_2"
			# 		print("in line", e.__traceback__.tb_lineno, type(e))
			# 		resp.close()
			# 		await u.notify_owner(client, f"error `{e}` in file {__name__} in line {e.__traceback__.tb_lineno}")
			# 		return "Error", str(e)+f"\nerror in {e.__traceback__.tb_lineno}", "error_2"
		else:
			timestamp = re.search("((\?)|(&))t=(\d*)", args[0])
			if timestamp:
				ffmpeg_before_options += before_options["start"](timestamp.group(4))
				re.sub("((\?)|(&))t=(\d*)", "", args[0])
			video_id = re.search("(v=|be/)(.{11})", args[0])
			if video_id:
				video_id = video_id.group(2)
				url = u.concat("v", video_id)
			else:
				playlist_id = re.search("(list=|be/)(.{18,34})", args[0])
				if playlist_id:
					playlist_id = playlist_id.group(2)
					url = u.concat("list", playlist_id)
				else:
					url = "None"
	else:
		url = "None"

	print(url)
	v_c = guild.voice_client
	if not v_c:
		v_c: VoiceClientCopy = await author.voice.channel.connect(cls = VoiceClientCopy)
	if not v_c.is_paused():
		await guild.me.edit(nick = guild.me.nick.replace("[PAUSED] ", ""))

	data = {
		"url": url,
		"client": client,
		"channel": channel,
		"guild": channel.guild,
		"author": author,
		"v_c": v_c,
		"loop": client.loop,
		"play_all": play_all,
		"msg": msg,
		"show": show,
		"sh": sh,
		"type": "default",
		"ffmpeg_options": [
			["options", ffmpeg_options],
			["before_options", ffmpeg_before_options]
		]
	}
	await msg.edit(embed = r.emb_resp("Adding to player!", "", "info"))
	MusicManager.put(data)


async def convertcurrencies(stuff: u.Data):
	channel = stuff.channel
	coll = db.db.get_collection("Currencies")
	content = stuff.message.content.split(" ", 2)
	base = "https://currency.world/"
	
	if len(content) < 3:
		return "Error", "Not enough arguments given!", "error"
	
	amount, src_curr = content[0], content[1]
	dst_currs = list(map(lambda x: x.strip(), content[2].split(",")))
	
	if not re.match(r"\d+\.?\d*$", amount):
		return "Error", "Invalid amount given!", "error"
	
	options = ["code", "symbol", "name", "countries"]
	async with aiohttp.request("GET", base + "currencies") as resp:
		text = await resp.text()
		html = bs4.BeautifulSoup(text, "html.parser")
	# print(html)
	table = html.find(attrs = {"class", "list_table currencies nolinkborder"})
	rows = table.find_all("tr")
	
	try:
		if len(rows) == coll.estimated_document_count():
			_pass = True
		else:
			_pass = False
	except Exception as e:
		print(type(e), e)
		_pass = False
	
	if not _pass:
		msg = await channel.send("Updating currency list, one moment", delete_after = 120)
		for row in rows:
			currency = {}
			columns = row.find_all("td")
			currency["code"] = columns[1].string
			currency["symbol"] = columns[2].string
			currency["name"] = columns[3].string
			currency["countries"] = list(map(lambda x: x.strip(), columns[4].string.split(",")))
			coll.insert_one(currency)
			print(columns[1:])
		await msg.edit(content = "Done!")
	
	async def search_curr(val):
		reg_val = re.escape(val)
		print(val, reg_val)
		for option in options:
			# print(option)
			s_res = coll.find_one({option: {"$regex": r"^" + reg_val + "$", "$options": "is"}})
			if s_res:
				break
		else:
			await msg.edit(content = "Not found locally, searching online")
			async with aiohttp.request("GET", "http://www.geonames.org/search.html?q=" + val + "&") as resp2:
				text2 = await resp2.text()
				# print(text2)
				if "no records" in text2:
					print(val, "not found")
					return await msg.edit(content = "",
						embed = r.emb_resp("Error", "Not all requested currencies could be found! "
							"Maybe the arguments are incorrectly separated?", "error"))
				html2 = bs4.BeautifulSoup(text2, "html.parser")
				table2 = html2.find(attrs = {"class": "restable"})
				# print(table2.prettify())
				rows2 = table2.find_all("tr")
				for row2 in rows2[2:]:
					columns2 = row2.find_all("td")
					# [print("column:", column) for column in columns2]
					country = columns2[2].find_all("a")
					# print("country:", country)
					if country:
						country = country[0].string
						print("country:", country)
						break
				else:
					txt = f"The found country ({country}) seems to have no (listed) currency... ~~Or something went wrong~~"
					embed2 = r.emb_resp("Hmm...", txt, "error_2")
					return await channel.send(embed = embed2)
					# columns2 = rows2[3].find_all("td")
					# country = columns2[2].find_all("a")[0].string
				s_res = coll.find_one({"countries": country})
				# if not s_res:
				# 	txt = f"The found country ({country}) seems to have no (listed) currency... ~~Or something went wrong~~"
				# 	embed2 = r.emb_resp("Hmm...", txt, "error_2")
				# 	return await channel.send(embed = embed2)
		# print(s_res)
		return s_res
	
	msg = await channel.send(f"Searching currency value for {src_curr}!")

	n_src_curr = await search_curr(src_curr)
	
	if not n_src_curr or type(n_src_curr) == discord.Message:
		return
	
	base_url = f"{base}convert/{n_src_curr['code']}_{amount}/"
	curr_codes = []
	
	for curr in dst_currs:
		await msg.edit(content = f"Searching currency value for {curr}!")
		res = await search_curr(curr)
		if not res or type(res) == discord.Message:
			pass
		else:
			curr_codes.append(res["code"]) if res else None
	if not curr_codes:
		return
	
	url = base_url + "/".join(curr_codes)
	
	async with aiohttp.request("GET", url) as resp:
		text = await resp.text()
		html = bs4.BeautifulSoup(text, "html.parser")
	
	results = html.find(attrs = {"id": "converter_body"}).find_all("div", recursive = False)
	embed = r.emb_resp("Result", "", "success", url = url)
	
	for result in results:
		# print("RES:", result)
		curr_name = result.find(attrs = {"class": "curname"}).string
		rate = result.find(attrs = {"class": "xrate"}).string
		_code = result.find(attrs = {"class": "code"})
		flag = f":flag_{_code.string[:2].lower()}: "
		_input = result.find("input")
		_symbol = result.find(attrs = {"class": "symbol"}).string
		name = f"{flag} {_code.string}"
		curr_name = f"{curr_name} ({_symbol})"
		
		embed.add_field(name = name, value = curr_name, inline = False)
		embed.add_field(name = "Amount", value = f'```{_input.get("value")}```')
		embed.add_field(name = "Conversion rate", value = f"```{rate}```")

	await msg.delete()
	await channel.send(embed = embed)
	

async def decrypt(stuff: u.Data):
	channel = stuff.channel
	text = stuff.message.content
	rest_pos = text.rfind("c")
	try:
		rest_int = int(text[rest_pos + 1:], 8)
		if rest_int:
			rest = hex(rest_int).replace("0x", "")
		else:
			rest = ""
		# print(rest)
	except ValueError:
		return "Error", "Can't decode!", "error"
	dec_info_pos = text.rfind("e")
	text = text[:rest_pos]
	try:
		dec_info = int(text[dec_info_pos + 1:])
		# print(dec_info)
	except ValueError:
		return "Error", "Can't decode!", "error"
	if not dec_info:
		return "Error", "Can't decode!", "error"
	text = text[:dec_info_pos]
	seed_len_pos = text.rfind("f")
	seed_len = int(text[seed_len_pos + 1:], 8)
	text = text[:seed_len_pos]
	seed = int(text[1:seed_len * 2:2] + rest, 16)
	# print(seed, hex(seed))
	clean_hash = text[:seed_len * 2:2] + text[seed_len * 2:]
	# print(clean_hash)
	parts = [clean_hash[dec_info * x:dec_info * (x + 1)] for x in range(len(clean_hash) // dec_info)]
	# print(parts)
	own_random = u.R(seed)
	# print(own_random)
	res = ""
	for part in parts:
		r_val = own_random.rand()
		val = int(part, 16) - 42
		# print("r_val: ", r_val, "val: ", val)
		temp = val ^ r_val
		char = chr(temp)
		# print(char, temp, r_val, val)
		res += char
	await channel.send(f'```{res}```')


async def deleteplaylist(stuff: u.Data):
	author = stuff.author
	args = stuff.args
	coll = db.db.get_collection("User_Songs")
	
	if args:
		playlist_titles = stuff.message.content.split(",")
		for playlist_title in playlist_titles:
			res = coll.delete_one({"user": author.id, "name": playlist_title})
			if res.deleted_count:
				return "Deleted", f"`{playlist_title}` successfully!", "ok"
			else:
				return "Info", f"Playlist `{playlist_title}`wasn't found!", "error_2"
	else:
		return "Invalid usage", "Need the playlist name!", "error"


async def deletesong(stuff: u.Data):
	channel = stuff.channel
	guild = channel.guild
	if not guild:
		await channel.send("Try again in a guild")
		return
	content: str = stuff.message.content
	playlist_arg = re.search("--(\S*)", content)
	if playlist_arg:
		to_sub = playlist_arg.group()
		playlist_name = playlist_arg.group(1)
		content = re.sub(to_sub, "", content)
	else:
		playlist_name = ""
	args = content.split()
	v_c = guild.voice_client
	if not v_c:
		await channel.send("*There probably isn't even a queue...*")
		return
	player = Player.get_player(guild.id)
	arglist = ",".join(args)
	numbers = re.findall("\d+", arglist)
	# print(numbers)
	
	if len(numbers) == 2:
		number = slice(*[int(x) for x in numbers])
	
	elif len(numbers) == 3:
		new_numbers = [int(x) for x in numbers]
		number = slice(*new_numbers)
	
	elif numbers:
		number = int(numbers[0])
	
	else:
		res = player.find_by_name(" ".join(args), multi = True)
		print(res)
		if res:
			if len(res) > 1:
				number = res
			else:
				number = res[0]
		else:
			number = None
			
	if number:
		response = Player.edit_queue(guild.id, number, keep = False, playlist = playlist_name)
	
	# print(response)
	else:
		res = Player.get_player(guild.id).del_current(playlist = playlist_name)
		response = ["Removed", f"```{res['data']['title']}```", "success"]
	embed = r.emb_resp(*response)
	
	desc = embed.description
	if len(desc) > 2048:
		embed = r.emb_resp("Info", desc[:re.search("-", desc, 2000).start()] + "\n...", "success")
	
	await stuff.message.delete(delay = 60.0)
	await channel.send(embed = embed, delete_after = 60.0)


async def disconnect(stuff: u.Data):
	channel = stuff.channel
	author = stuff.author
	guild = channel.guild
	nick = channel.guild.me.nick
	
	if not guild:
		return "*Yeah?*", "You sure about that?", "error"

	if not author.voice:
		return "Error", "You can't just stop the music someone else is listening to!", "error"

	else:
		if "PAUSED" in nick:
			await guild.me.edit(nick = nick[8:])
		await channel.send(embed = r.emb_resp("Disconnecting!", "", "success"))
		await MusicManager.stop(str(guild.id))


async def encrypt(stuff: u.Data):
	channel = stuff.channel
	text = stuff.message.content
	
	buffer_seed = u.io.StringIO()
	timestamp = int(datetime.now().timestamp())
	# print(timestamp)
	modified = timestamp // len(text)
	# print(modified)
	seed = (len(str(stuff.client.user.id)) * len(text)) | (len(text) + len(str(stuff.client.user.id))) + modified
	# print(seed)
	hex_seed = hex(seed).replace("0x", "")
	# print(hex_seed)
	buffer_seed.write(hex_seed)
	buffer_seed.seek(0)
	own_rand = u.R(seed)
	# print(own_rand)
	hex_list = []
	max_h = 0
	for char in text:
		r_val = own_rand.rand()
		new_char = ord(char) ^ r_val
		val = new_char + 42
		# print(char, ord(char), r_val, new_char)
		h = hex(val)[2:]
		if len(h) > max_h:
			max_h = len(h)
		hex_list.append(h)
	# print(hex_list)
	padded = list(map(lambda x: x.zfill(max_h), hex_list))
	hex_string = "".join(padded)
	dec_info = f"e{max_h}"
	hex_1, hex_2 = hex_string[:len(hex_string) // 2], hex_string[len(hex_string) // 2:]
	# print(hex_1, hex_2)
	mix = list(zip(hex_1, list(buffer_seed.read(len(hex_1)))))
	# print(mix)
	secret = "".join(map(lambda x: "".join(x), mix))
	# print(secret)
	to_replace = "".join(list(zip(*mix))[0])
	# print(to_replace)
	hex_1 = hex_1.replace(to_replace, secret, 1)
	# print(hex_1)
	hex_string = hex_1 + hex_2
	rest = buffer_seed.read()
	# print(rest)
	diff = len(hex_seed) - len(rest)
	dec_info += f"c{oct(int(rest or '0', 16))}"
	append = f"f{oct(diff)}{dec_info}" if rest else f"f{oct(len(hex_seed))}{dec_info}"
	hex_string = hex_string + append
	await channel.send(f'```{hex_string.replace("0o", "")}```')


async def fanadata(stuff: u.Data):
	author = stuff.author
	channel = stuff.channel
	client = stuff.client
	args = stuff.args
	# return await channel.send("I failed on something and don't want to update almost 700 characters all again now")
	
	base_url = "https://saomd.fanadata.com/"
	coll1 = db.db.get_collection(name = "SAOMD_Chars")
	coll2 = db.db.get_collection(name = "SAOMD_Weapons")
	coll3 = db.db.get_collection(name = "Lists")
	
	if args:
		
		if "update" in args:
			if author.id == u.data["owner"] or author.id == client.user.id:
				pass
			else:
				return "Error", "Only the bot owner can do that!", "error"
			if not lock.locked():
				await channel.send("Auto-update in progress!")
				await lock.acquire()
			ch = w = i_i = False
			args.remove("update")
			progress1 = await channel.send(f"Progress: ")
			amount_1_1 = 0
			amount_1_2 = 0
			amount_2_1 = 0
			amount_2_2 = 0
			
			if "char" in args:
				args.remove("char")
				ch = True
			if "weapon" in args:
				args.remove("weapon")
				w = True
			if "ignoreimages" in args:
				args.remove("ignoreimages")
				i_i = True
			print(ch, w, i_i)
			if ch:
				i = 0
				j = int(args[0])
				await progress1.edit(content = f"Progress:\n-chars: ")
				while i < (int(args[1]) - int(args[0])):
					await progress1.edit(
						content = f"Progress:\n- chars: {(i + 1) / (int(args[1]) - int(args[0])) * 100}%")
					j += 1
					i += 1
					
					try:
						
						async with aiohttp.request("GET", base_url + "character-" + str(j)) as resp:
							html = await resp.text()
							if "NO DATA" in html:
								continue
							
							tag_text = bs4.BeautifulSoup(html, "html.parser")
							contents = list(tag_text.find_all(attrs = {"class": "border_buff__glob"}))
							abilities = []
							if contents:
								for content in contents:
									content = list(content.find_all(re.compile(r"div.*")))
									abilities.append(sorted([
										"".join(list(s.stripped_strings))
										.replace("\n", "")
										.replace(" ", "")
										.replace("?", "")
										.replace(">", "") for s in content]))
							else:
								abilities = [["-"]]
							abilities_set = set(abilities[0])
							if len(abilities) == 2:
								abilities_set.update(abilities[1])
							# print("test1")
							base_info = tag_text.find(attrs = {"class": "row"})
							title_tag = list(base_info.find("h5").stripped_strings)
							if len(title_tag) == 2:
								title, tag = title_tag
							else:
								title = title_tag[0]
								tag = ""
							# print(title, tag)
							# desc, world = base_info.find(attrs = {"class": "description__charac"}).stripped_strings
							desc_world = base_info.find(attrs = {"class": "description__charac"})
							desc = desc_world.p.string.strip()
							world = desc_world.div.string.strip()
							# print(desc.string.strip(), world.string.strip())
							world = world.split()[1]
							# print(desc, world)
							rarity, thumbnail = base_info.find(attrs = {"class": "charac__type"}).find_all("img")
							weapon_type, element = thumbnail.get("alt").split("_")
							thumbnail = thumbnail.get("src").replace(" ", "%20")
							rarity = "R" + rarity.get("title").split()[0]
							# print(weapon_type, element, thumbnail, rarity)
							image = base_info.find_all(attrs = {"class": "artwork"})
							images = list(map(lambda x: x.get("src"), image))
							# print(images)
							if not i_i:
								for img in images:
									async with aiohttp.request("GET", base_url + img) as check_image:
										if check_image.status == 404:
											images[images.index(img)] = "/public/asset/no_data/no_img_artwork.png"
							# print(images)
							# return
							# else:
							# print("nothing there")
							
							'''text = html[re.search("col l4 s12 contenu-first", html).span()[1]:
										re.search("glob__desc_type", html).span()[0]]
							text = html2text.html2text(text)
							# print(text)
							# print("test2")
							
							tag = re.search(r"[[【].*[]】]", text).group()
							# print("test3")
							
							title = re.search(" .*", re.search("#.*\n", text).group()).group()
							# print("test4")
							
							img_1 = re.findall("\..*png", text)
							img_2 = re.findall("\..*jpg", text)
							async with aiohttp.request("GET", base_url + img_2[0][2:]) as check_image:
								if check_image.status == 404:
									img_2 = ["./public/asset/no_data/no_img_artwork.png"]
							# print("test5")
							
							weapon_element = re.search(r"/[\w ]+\.", img_1[1]).group()
							# print("test6")
							weapon_type = re.search(r"/[\w ]+_", weapon_element).group()[1:-1]
							# print("test7")
							element = re.search(r"_\w+\.", weapon_element).group()[1:-1]
							# print("test8")
							
							text = text.split("\n\n")
							
							text = text[len(text) - 3]
							# print("test9")'''
							disc = re.search("\"footer-copyright\".*Scripts", html, re.DOTALL).group()
							disc = html2text.html2text(disc)
							disc = disc[re.search("\">", disc).span()[1] + 2:re.search("<", disc).span()[0]]
							# print("test10")
							abilities_set = sorted(abilities_set)
							
							char = {
								"nr": j,
								"tag": tag,
								"name": title,
								"description": desc if desc else "?!?",
								"world": world,
								"abilities": abilities,		# passt..?
								"thumbnail": thumbnail,
								"images": images,
								"disc": disc,
								"element": element,
								"type": weapon_type,
								"rarity": rarity,
								"all_abilities": abilities_set
							}
							# print("test11")
							
							# print("test12")
							if i_i:
								char.pop("images")
							coll3.update_one({"name": "rarity"}, {"$addToSet": {"value": rarity}}, upsert = True)
							coll3.update_one({"name": "world"}, {"$addToSet": {"value": world}}, upsert = True)
	
							if not coll3.find_one({"name": "name"}) or not title.strip().lower() in \
																		coll3.find_one({"name": "name"})["value"]:
								
								data = {"value": title.strip().lower()}
								print(data)
								
								coll3.update_one({"name": "name"}, {"$addToSet": data}, upsert = True)
								await channel.send(f"{title.strip()} added to automatic name search!", delete_after = 10)
							
							# print("test13")
							
							if not coll3.find_one({"name": "all_abilities"}):
								coll3.insert_one({"name": "all_abilities", "value": []})
							
							check_abilities = coll3.find_one({"name": "all_abilities"})["value"]
							# print(check_abilities, end = "----")
							test = list(filter(lambda x: x not in check_abilities, abilities_set))
							print(test)
							if test:
								# print("test14")
								test2 = test.copy()
								test = {"value": {"$each": test}}
								print(test)
								
								coll3.update_one({"name": "all_abilities"}, {"$addToSet": test})
								msg = "- " + '\n- '.join(test2)
								await channel.send(f"```{msg}```added to automatic ability search!", delete_after = 20)
							
							# print("test15")
							
							result = coll1.find_one_and_update({"nr": j}, {"$set": char}, projection = {"_id": False},
																upsert = True)
							# print("test16")
							resp.close()
							del tag_text
						
						if not result or result != char:
							
							# print("test17")
							send_url = base_url + "character-" + str(j)
							embed = r.emb_resp(f"{tag}{title} ({rarity})", f"```{desc}```", "success", send_url)
							
							if not result:
								# print("test18")
								amount_1_1 += 1
								embed.set_thumbnail(url = base_url + char["thumbnail"])
								embed.set_image(url = base_url + char["images"][0])
								embed.set_footer(text = disc)
								embed.add_field(name = "World", value = world, inline = False)
								if len(abilities) == 2:
									embed.add_field(name = "Abilities A",
										value = "```- " + "\n- ".join(abilities[0]) + "```")
									embed.add_field(name = "Abilities B",
										value = "```- " + "\n- ".join(abilities[1]) + "```")
								else:
									embed.add_field(name = "Abilities",
										value = "```- " + "\n- ".join(abilities[0]) + "```")
							
							elif result != char:
								print("result: ", result)
								print("char: ", char)
								# print("test19")
								amount_1_2 += 1
								change = {}
								for val in char.items():
									try:
										if val[1] != result.get(val[0]):
											change[val[0]] = val[1]
									except KeyError:
										change[val[0]] = val[1]
								print("change: ", change)
								# change = {val[0]: val[1] for val in char.items() if val[1] != result[val[0]]}
								try:
									embed.set_thumbnail(url = base_url + change["thumbnail"])
								except KeyError:
									pass
								try:
									embed.set_image(url = base_url + change["images"][0])
								except KeyError:
									pass
								try:
									embed.set_footer(text = change["disc"])
								except KeyError:
									pass
								try:
									embed.add_field(name = "World", value = change["world"], inline = False)
								except KeyError:
									pass
								try:
									embed.add_field(name = "Abilities",
													value = "```- " + "\n- ".join(change['all_abilities']) + "```")
								except KeyError:
									pass
							
							# print("test20")
							obj = await channel.send(embed = embed)
							await obj.delete(delay = 240)
					
					except Exception as e:
						print(j, e)
						if "span" not in str(e):
							await channel.send(embed = r.emb_resp2(f"```{j} in line {e.__traceback__.tb_lineno}\n{str(e)}```"))
				
			progress2 = progress1.content
			
			if w:
				i = 0
				j = int(args[0])
				while i < (int(args[1]) - int(args[0])):
					await progress1.edit(
						content = f"{progress2}\n- weapons: {(i + 1) / (int(args[1]) - int(args[0])) * 100}%")
					j += 1
					i += 1
					
					try:
						
						async with aiohttp.request("GET", base_url + "weapon-" + str(j)) as resp:
							html = await resp.text()
							if "NO DATA" in html:
								continue
								
							tag_text = bs4.BeautifulSoup(html, "html.parser")
							title = next(tag_text.find(attrs = {"class": "object_equipment"}).stripped_strings)
							print(title)
							img = tag_text.find(attrs = {"class": "col l3 m12 s12 glob_artwork_equipment"})
							img = list(img.children)[1].get("src")
							print(img)
							async with aiohttp.request("GET", base_url + img) as check_image:
								if check_image.status == 404:
									img = "public/asset/no_data/no_img_arme.png"
							print(img)
							thumbnail = tag_text.find(attrs = {"class": "weapon icon_equipment"})
							icon = thumbnail.get("src").replace(" ", "%20")
							weapon_type, element = thumbnail.get("title").split("_")
							rarity = "R" + tag_text.find_all(attrs = {"class": "icon_equipment"})[-2].get("title")[0]
							print(icon, weapon_type, element, rarity)
							'''return
							text = html[re.search("title__first", html).span()[1]:
										re.search("col l8 s12", html).span()[0]]
							text = html2text.html2text(text)
							
							title = re.search(r"\b[\w .'&+-・×]+\b", re.search(r"#.*!", text).group()).group()
							
							img = re.findall(r"\.[\w \n/]+\.png", text)
							img.extend(re.findall(r"\..*jpg", html))
							
							# await channel.send(img)
							
							imgs = []
							for image in img:
								if "weapon" in image or "500/stuff" in image:
									imgs.append(image[2:].replace("\n", " "))
							
							if len(imgs) == 2:
								img_1, img_2 = imgs
								async with aiohttp.request("GET", base_url + img_2) as check_image:
									if check_image.status == 404:
										img_2 = "public/asset/no_data/no_img_arme.png"
							elif len(imgs) == 1:
								img_1 = imgs[0]
								img_2 = "public/asset/no_data/no_img_arme.png"
							else:
								await channel.send(embed = r.emb_resp2(f"Error at weapon {title} with index {j}\n{img}\n{imgs}"))
								continue
								
							weapon_element = re.search(r"/[\w ]+\.", img_1).group()
							weapon_type = re.search(r"/[\w ]+_", weapon_element).group()[1:-1]
							element = re.search(r"_\w+\.", weapon_element).group()[1:-1]'''
							
							disc = re.search("\"footer-copyright\".*Scripts", html, re.DOTALL).group()
							disc = html2text.html2text(disc)
							disc = disc[re.search("\">", disc).span()[1] + 2:re.search("<", disc).span()[0]]
							# return await channel.send(imgs)
							weapon = {
								"nr": j,									# passt
								"name": title,								# passt
								"thumbnail": icon,							# passt
								"image": img,								# passt
								"disc": disc,								# passt
								"element": element,							# passt
								"type": weapon_type,						# passt
								"rarity": rarity
							}
							# return print(weapon)
							coll3.update_one({"name": "rarity"}, {"$addToSet": {"value": rarity}}, upsert = True)
							result = coll2.find_one_and_update({"nr": j}, {"$set": weapon}, projection = {"_id": False},
								upsert = True)
							del tag_text
						
						if not result or result != weapon:
							
							send_url = base_url + "weapon-" + str(j)
							embed = r.emb_resp(f"{title} ({rarity})", "", "success", send_url)
							
							if not result:
								amount_2_1 += 1
								embed.set_thumbnail(url = base_url + weapon["thumbnail"])
								embed.set_image(url = base_url + weapon["image"])
								embed.set_footer(text = disc)
							
							elif result != weapon:
								amount_2_2 += 1
								change = {val[0]: val[1] for val in weapon.items() if val[1] != result.get(val[0])}
								try:
									embed.set_thumbnail(url = base_url + change["thumbnail"])
								except KeyError:
									pass
								try:
									embed.set_image(url = base_url + change["image"])
								except KeyError:
									pass
								try:
									embed.set_footer(text = change["disc"])
								except KeyError:
									pass
								
							obj = await channel.send(embed = embed)
							await obj.delete(delay = 240)
					
					except Exception as e:
						print(j, e)
						if "span" not in str(e):
							await channel.send(embed = r.emb_resp2(f"{j} {str(e)}"))
				
			embed = r.emb_resp("✅ Done!", "", "success")
			if amount_1_1:
				embed.add_field(name = "Chars added:", value = f"```Python\n{amount_1_1}```")
			if amount_1_2:
				embed.add_field(name = "Chars updated:", value = f"```Python\n{amount_1_2}```")
			if amount_2_1:
				embed.add_field(name = "Weapons added:", value = f"```Python\n{amount_2_1}```")
			if amount_2_2:
				embed.add_field(name = "Weapons updated:", value = f"```Python\n{amount_2_2}```")

			lock.release()
			return await channel.send(embed = embed)
		
		if "search" in args:
			print(args)
			
			args = " ".join(" ".join(args).lower().split("search")).split()
			print(args)
			
			s_c = s_w = _spam = _list = _n = _u = False
			
			if "name" in args:
				_n = True
				args.remove("name")
			
			if "char" in args:
				s_c = True
				args.remove("char")
			
			elif "weapon" in args:
				s_w = True
				args.remove("weapon")
			
			else:
				msg = await channel.send(embed = r.emb_resp("Error", "_```Weapon or char search?```_", "error"))
				return await msg.delete(delay = 120)
			
			print(args)
			
			query = {}
			
			if "spam" in args:
				if "--u--" in args:
					if author.id == u.data["owner"]:
						args.remove("--u--")
						_u = True
					else:
						await channel.send(embed = r.emb_resp("Error", "Only the bot owner may do that", "error"))
				if channel.id != 0:
					_spam = True
					args.remove("spam")
				else:
					return await channel.send(embed = r.emb_resp("Error", "```Not allowed to spam in here!```", "error"))
				print(args)
			
			elif "list" in args:
				_list = True
				args.remove("list")
				print(args)
			
			if "desc" in args:
				args.remove("desc")
				temp_args = args.copy()
				query["description"] = {"$regex": f'(?=.*{")(?=.*".join(temp_args)})', "$options": "is"}
				print(args)
			
			elif "tag" in args:
				args.remove("tag")
				temp_args = args.copy()
				query["tag"] = {"$regex": f'(?=.*{")(?=.*".join(temp_args)})', "$options": "is"}
				print(args)
			
			else:
				for arg in args:
					print(args)
					data = coll3.find_one({"value": {"$regex": arg, "$options": "i"}})
					
					if data:
						query[data["name"]] = {"$regex": arg, "$options": "i"}
						args = " ".join(" ".join(args).split(arg)).split()
			
				else:
					if _n:
						query["name"] = {"$regex": ".*".join(args), "$options": "i"}
			
			print(query)
			if len(query) == 0:
				# query["name"] = {"$regex":" ".join(args),"$options":"i"}
				msg = await channel.send(
					embed = r.emb_resp("Error", "_```So you want to search...\nfor what exactly?```_", "error"))
				return await msg.delete(delay = 120)
			
			content = f"||Your input got interpreted to following query:\n`{query}`||"
			
			if _spam:
				unlimited = False
				_curr = 0
				ranges = re.findall(r"\d", "".join(args))
				if not _u:
					
					try:
						_min = int(ranges[0])
						_max = int(ranges[1])
					
					except IndexError:
						_min = 0
						_max = 10
				
				else:
					_min = 0
					_max = 2000
					unlimited = True
				
				if s_c:
					chars = coll1.find(query)
					
					for char in chars:
						# print(char)
						_curr += 1
						url = f"{base_url}character-{char['nr']}"
						embed = r.emb_resp(f"{char['tag']}{char['name']} ({char['rarity']})", char['description'], "success", url)
						embed.set_image(url = f"{base_url}{char['images'][0]}")\
							.set_thumbnail(url = f"{base_url}{char['thumbnail']}")\
							.set_footer(text = char["disc"])\
							.add_field(name = "World", value = char["world"], inline = False)
						if len(char["abilities"]) == 2:
							embed.add_field(name = "Abilities A",
											value = "```- " + "\n- ".join(char["abilities"][0]) + "```")
							embed.add_field(name = "Abilities B",
											value = "```- " + "\n- ".join(char["abilities"][1]) + "```")
						else:
							embed.add_field(name = "Abilities",
											value = "```- " + "\n- ".join(char["abilities"][0]) + "```")
						
						if (_max - _min <= 10) or unlimited:
							
							if _min <= _curr <= _max:
								await channel.send(embed = embed)
						
						else:
							return "Error", "Nope!", "error"
				
				elif s_w:
					weapons = coll2.find(query)
					
					for weapon in weapons:
						_curr += 1
						embed = r.emb_resp(f"{weapon['name']} ({weapon['rarity']})", "", "success", f"{base_url}weapon-{weapon['nr']}")
						embed.set_image(url = f"{base_url}{weapon['image']}")\
							.set_thumbnail(url = f"{base_url}{weapon['thumbnail']}")\
							.set_footer(text = weapon["disc"])
						
						if (_max - _min < 10) or unlimited:
							
							if _min <= _curr <= _max:
								await channel.send(embed = embed)
						
						else:
							return "Error", "Nope!", "error"
			
			elif _list:
				
				if s_c:
					
					chars = coll1.find(query)
					embed = r.emb_resp("", "", "success")
					embed.set_author(name = f"Chars found: {chars.count()}")
					text = ""
					field_array = []
					
					for char in chars:
						if len(text) > 950:
							field_array.append(text)
							text = ""
						else:
							text += f"- [{char['tag']}{char['name']}]({base_url}character-{char['nr']})\n"
					if text:
						field_array.append(text)
					i = 0
					
					for field in field_array:
						i += 1
						embed.add_field(name = f"Part {i}", value = field, inline = False)
					try:
						msg = await channel.send(content = content, embed = embed)
					except discord.HTTPException:
						print(len(field_array))
						error = "Too many chars found to display them in a list! (Making them a link costs space too)"
						msg = await channel.send(embed = r.emb_resp("Error", f"```{error}```", "error"))
				
				elif s_w:
					weapons = coll2.find(query)
					
					embed = r.emb_resp("", "", "success")
					embed.set_author(name = f"Weapons found: {weapons.count()}")
					text = ""
					field_array = []
					
					for weapon in weapons:
						if len(text) > 950:
							field_array.append(text)
							text = ""
						else:
							text += f"- [{weapon['name']}]({base_url}weapon-{weapon['nr']})\n"
					if text:
						field_array.append(text)
					i = 1
					
					for field in field_array:
						i += 1
						embed.add_field(name = f"Part {i}", value = field)
					try:
						msg = await channel.send(content = content, embed = embed)
					except discord.HTTPException:
						error = "Too many weapons found to display them in a list! (Making them a link costs space too)"
						msg = await channel.send(embed = r.emb_resp("Error", f"```{error}```", "error"))
			
			else:
				
				if s_c:
					
					chars = list(coll1.find(query))
					if chars:
						nr = i_nr = 0
						max_nr = len(chars) - 1
						
						embed = r.emb_resp("", "", "success")
						embed.set_author(name = f"Chars found: {len(chars)}\nCharacter {nr + 1}")
						embed.title = f"{chars[nr]['tag']}{chars[nr]['name']} ({chars[nr]['rarity']})"
						embed.description = f"{chars[nr]['description']}"
						embed.url = f"{base_url}character-{chars[nr]['nr']}"
						embed.set_image(url = f"{base_url}{chars[nr]['images'][i_nr]}")\
							.set_thumbnail(url = f"{base_url}{chars[nr]['thumbnail']}")\
							.set_footer(text = chars[nr]["disc"])\
							.add_field(name = "World", value = chars[nr]["world"], inline = False)
						if len(chars[nr]["abilities"]) == 2:
							embed.add_field(name = "Abilities A",
											value = "```- " + "\n- ".join(chars[nr]["abilities"][0]) + "```")
							embed.add_field(name = "Abilities B",
											value = "```- " + "\n- ".join(chars[nr]["abilities"][1]) + "```")
						else:
							embed.add_field(name = "Abilities",
											value = "```- " + "\n- ".join(chars[nr]["abilities"][0]) + "```")
						
						msg = await channel.send(content = content, embed = embed)
						
						await msg.add_reaction("❌")
						if len(chars) > 1:
							await msg.add_reaction("⬅")
							await msg.add_reaction("➡")
						if len(chars[nr]["images"]) > 1:
							await msg.add_reaction("⬇")
							await msg.add_reaction("⬆")
						
						def check(check_reaction, check_user):
							return check_user == author and check_reaction.message.id == msg.id
							
						while True:
							
							try:
								reaction, user = await client.wait_for("reaction_add", check = check, timeout = 3600)
								try:
									await msg.remove_reaction(reaction.emoji, user)
								except discord.Forbidden:
									pass
							except TimeoutError:
								response = "```Closed due to no one using it in the last hour```"
								try:
									await msg.clear_reactions()
								except discord.Forbidden:
									pass
								break
							
							if reaction.emoji == "⬅":
								# print("C"*100)
								if nr <= 0:
									nr = max_nr
								else:
									nr -= 1
							
							elif reaction.emoji == "➡":
								if nr == max_nr:
									nr = 0
								else:
									nr += 1
									
							elif reaction.emoji == "⬇":
								if i_nr <= 0:
									i_nr = len(chars[nr]["images"]) - 1
								else:
									i_nr -= 1

							elif reaction.emoji == "⬆":
								if i_nr == len(chars[nr]["images"]) - 1:
									i_nr = 0
								else:
									i_nr += 1
							
							elif reaction.emoji == "❌":
								response = "```Char search left```"
								try:
									await msg.clear_reactions()
								except discord.Forbidden:
									pass
								break
							# print(reaction.message.id == msg.id)
							if len(chars[nr]["images"]) > 1:
								await msg.add_reaction("⬇")
								await msg.add_reaction("⬆")
							else:
								i_nr = 0
								await msg.clear_reaction("⬇")
								await msg.clear_reaction("⬆")
							embed.set_author(name = f"Chars found: {len(chars)}\nCharacter {nr + 1}")
							embed.title = f"{chars[nr]['tag']}{chars[nr]['name']} ({chars[nr]['rarity']})"
							embed.description = f"{chars[nr]['description']}"
							embed.url = f"{base_url}character-{chars[nr]['nr']}"
							embed.set_image(url = f"{base_url}{chars[nr]['images'][i_nr]}")\
								.set_thumbnail(url = f"{base_url}{chars[nr]['thumbnail']}")\
								.set_footer(text = chars[nr]["disc"])
							embed.clear_fields()
							embed.add_field(name = "World", value = chars[nr]["world"], inline = False)
							if len(chars[nr]["abilities"]) == 2:
								embed.add_field(name = "Abilities A",
												value = "```- " + "\n- ".join(chars[nr]["abilities"][0]) + "```")
								embed.add_field(name = "Abilities B",
												value = "```- " + "\n- ".join(chars[nr]["abilities"][1]) + "```")
							else:
								embed.add_field(name = "Abilities",
												value = "```- " + "\n- ".join(chars[nr]["abilities"][0]) + "```")
							
							await msg.edit(embed = embed)
						
						await msg.edit(content = response, embed = None)
					else:
						return "Error", "No chars with the given criteria found!", "error"
				
				elif s_w:
					
					weapons = list(coll2.find(query))
					
					if weapons:
						nr = 0
						max_nr = len(weapons) - 1
						
						embed = r.emb_resp("", "", "success")
						embed.set_author(name = f"Weapons found: {len(weapons)}\nWeapon {nr + 1}")
						embed.title = f"{weapons[nr]['name']} ({weapons[nr]['rarity']})"
						embed.url = f"{base_url}weapon-{weapons[nr]['nr']}"
						embed.set_image(url = f"{base_url}{weapons[nr]['image']}")\
							.set_thumbnail(url = f"{base_url}{weapons[nr]['thumbnail']}")\
							.set_footer(text = weapons[nr]["disc"])
						
						msg = await channel.send(content = content, embed = embed)
						
						await msg.add_reaction("⬅")
						await msg.add_reaction("➡")
						await msg.add_reaction("❌")
						
						def check(check_reaction, check_user):
							return check_user == author and check_reaction.message.id == msg.id
						
						while True:
							try:
								reaction, user = await client.wait_for("reaction_add", check = check, timeout = 3600)
								try:
									await msg.remove_reaction(reaction.emoji, user)
								except discord.Forbidden:
									pass
							except TimeoutError:
								response = "```Closed due to no one using it in the last hour```"
								try:
									await msg.clear_reactions()
								except discord.Forbidden:
									pass
								break

							if reaction.emoji == "⬅":
								# print("W"*100)
								if nr == 0:
									nr = max_nr
								else:
									nr -= 1
							
							elif reaction.emoji == "➡":
								if nr == max_nr:
									nr = 0
								else:
									nr += 1
							
							elif reaction.emoji == "❌":
								response = "```Weapon search left```"
								try:
									await msg.clear_reactions()
								except discord.Forbidden:
									pass
								break
							
							embed.set_author(name = f"Weapons found: {len(weapons)}\nWeapon {nr + 1}")
							embed.title = f"{weapons[nr]['name']} ({weapons[nr]['rarity']})"
							embed.url = f"{base_url}weapon-{weapons[nr]['nr']}"
							embed.set_image(url = f"{base_url}{weapons[nr]['image']}")\
								.set_thumbnail(url = f"{base_url}{weapons[nr]['thumbnail']}")\
								.set_footer(text = weapons[nr]["disc"])
							
							await msg.edit(embed = embed)
							
						return await msg.edit(content = response, embed = None)
					else:
						return "Error", "No weapons with the given criteria found!", "error"
		
		else:
			obj = stuff.message
			msg = await channel.send(
				embed = r.emb_resp("Error", f"```python\nTry with \"{obj.content} search\" again```", "error")
			)
			try:
				await msg.delete(delay = 120)
				await obj.delete()
			except discord.Forbidden:
				pass
			return
	
	else:
		# return await channel.send("No-argument usage currently unavailable")
		async with aiohttp.request("GET", "https://saomd.fanadata.com/") as resp:
			text = await resp.text()
		bs = bs4.BeautifulSoup(text, "html.parser")
		lists = bs.find(attrs = {"class": "col l6 m12 s12 count_bar"})
		lis = lists.find_all(attrs = {"class": "bar_style_int"})[:2]
		cnt_1 = int(lis[0].span.string)
		cnt_2 = int(lis[1].span.string)
		
		title = "Total amount of chars and weapons in database"
		cnt1 = f"Chars: {coll1.count()}"
		cnt2 = f"Weapons: {coll2.count()}"
		cnt = f"```{cnt1}\n{cnt2}```"
		ch_update = coll1.count() != cnt_1
		w_update = coll2.count() != cnt_2
		update = False
		
		if ch_update or w_update:
			update = True
			title += "\n**UPDATE NEEDED!**"
			args = ["update"]
			if ch_update:
				args.append("char")
			if w_update:
				args.append("weapon")
			limit_1 = list(coll1.find())[-1]["nr"]
			limit_2 = list(coll2.find())[-1]["nr"]
			args.append(min(limit_1, limit_2) - 10)
			args.append(max(limit_1, limit_2) + 10)
			stuff = u.Data(*args, author = client.user, channel = channel, client = client)
		
		embed = r.emb_resp(title, cnt, "info", url = base_url)
		msg = await channel.send(embed = embed.set_thumbnail(url = base_url + "/public/asset/network"
																				"/yuis_database_logo_mini.png"))
		
		if update:
			title += "\nUpdate in progress!"
			embed.title = title
			await msg.edit(embed = embed)
			
			if lock.locked():
				await lock.acquire()
				await msg.channel.send(embed = r.emb_resp("Fanadata-Info!", "```Update complete!```", "std_info"))
				lock.release()
			
			else:
				await lock.acquire()
				await fanadata(stuff)


async def getpinnedmessage(stuff: u.Data):
	channel = stuff.channel
	args = stuff.args
	
	pins = await channel.pins()
	# print(pins)
	
	if "content" in args:
		args.remove("content")
		for message in pins:
			if (" ".join(args)).lower() in message.content.lower():
				return await channel.send(
					embed = r.emb_resp("✅ Message found!", f"[click here]({message.jump_url})", "success"))
		return await channel.send(embed = r.emb_resp("❌ Message not found!", "", "error"))
	
	elif "number" in args:
		args.remove("number")
		number = int(args[0]) - 1
		try:
			return await channel.send(
				embed = r.emb_resp("✅ Message found!", f"[click here]({pins[number].jump_url})", "success"))
		except IndexError:
			return await channel.send(embed = r.emb_resp("❌ Message not found!", "", "error"))
	
	else:
		await channel.send(embed = r.emb_resp("Warning!", "```No search type specified```", "error"))


async def getprofilepicture(stuff: u.Data):
	client = stuff.client
	channel = stuff.channel
	args = stuff.args
	mentions = stuff.message.mentions
	
	if not mentions and (not args or not args[0].isdecimal()):
		return "", "No ID or mention given!", "error"
	else:
		if not mentions:
			try:
				user = await client.fetch_user(int(args[0]))
			except Exception as e:
				print(e, type(e))
				return "Couldn't find user " + args[0] + "!",
		else:
			user = mentions[0]
		await channel.send(user.avatar_url)


async def gettime(stuff: u.Data):
	channel = stuff.channel
	arg_list = stuff.message.content.split(",")
	
	for args in map(lambda x: x.split(" "), arg_list):
		
		place = " ".join(args).replace(" ", "_")
		if place:
			async with aiohttp.request("GET", "https://time.is/" + place + time_is_settings) as resp:
				res = resp.url.parts[1]
				html = await resp.text()
				text = bs4.BeautifulSoup(html, "html.parser")
				tag = text.find(attrs = {"id": "msgdiv"})
				timezone = text.find(attrs = {"id": "time_zone"})
				tag = tag.contents[0]
				if not tag.contents[0].next_sibling:
					strings = list(tag.parent.strings)
					location = f"{strings[0]} ({strings[1]})" if res in strings else f"{res} ({strings[0]})"
				else:
					location = "".join(list(tag.strings)[1:])[:-4]
					# temp: {'timezone' if res.isupper() else 'location'} {res}
				title = f"Current time {'for' if res.isupper() else 'in'} {location}"
				embed = r.emb_resp(title, text.find("time").string, "info", url = resp.url.human_repr())
				try:
					embed.add_field(name = "Time zone info", value = timezone.find("li").string)
				except Exception as e:
					print(type(e))
					return "Internal error", "No idea what happened", "error_2"
				
		else:
			return "Error", "No query given!", "error"
		
		await channel.send(embed = embed)
	

async def help(stuff: u.Data):
	channel = stuff.channel
	args = stuff.args
	if "-keep" in args:
		keep = 1
		args.remove("-keep")
	else:
		keep = 0
	ext = ""
	try:
		if args[0] == "ext":
			# print(args)
			ext = args.pop(0)
	except IndexError:  # Exception as e:
		pass  # print(e)
	
	if not len(args) > 0:
		
		if ext == "ext":
			src = 'commands_ext.json'
			key = "Commands_ext"
			title = "Full help list for owner"
		else:
			src = 'commands.json'
			key = "Commands"
			title = "Full help list"
		# print("full")
		
		with open(src, 'r') as file:
			data = json.load(file)[key]
			
			text = "**```Use help <command name> to get further information about it.\n" \
				"Add \"-keep\" at the end so the message won't get deleted.\n```**" \
				"```You can even add a comment to the command," \
				" which will be ignored on command-usage by writing \"%%\"!\n```" \
				f"Example:```{prefix}p profile %% Look at my marvelous profile everyone!```"
			value = ""
			
			embed = r.emb_resp(title, text, "success")
			en = {
				True: "⭕",
				False: "⛔"
			}
			for val in data:
				if val["enabled"]:
					enabled = True
				else:
					enabled = False
				value = value + f"{en[enabled]} {val['name']}\n"
				
			# print(val,type(val))
			# print(value)
			
			embed.add_field(name = "Commands:", value = f"```{value}```", inline = False)
		# print(embed.fields[0])
	else:
		print("partial ", args)
		
		if ext == "ext":
			coll = db.db.get_collection(name = "Commands_extended")
		else:
			coll = db.db.get_collection(name = "Commands")
		
		cmd = args[0]
		data = coll.find_one({'name': cmd})
		# print(coll.find(), data)
		# print(data)
		
		if data:
			title = f"Help for {cmd}"
		else:
			data = coll.find_one({'alt': cmd})
			if data:
				title = f"Help for {data['name']}"
			else:
				cmds = db.db.Lists.find_one({"name": "strings"})["commands"]
				pos = u.dmp.match_bitap(cmds, cmd, 0)
				if pos != -1:
					cmd = cmds[pos: cmds.find(" ", pos)]
					print(cmd)
					data = coll.find_one({'name': cmd})
					if not data:
						return "Something went wrong", "", "error_2"
					title = f"Help for {cmd}"
				else:
					await channel.send(embed = r.emb_resp("Warning!", f"Command ```{cmd}``` doesn't exist!", "error"))
					return
		
		text = ""
		
		if not data["enabled"]:
			title += "\n**`Currently disabled!`**"
			
		embed = r.emb_resp(title, text, "success")
		
		embed.add_field(name = "Alternative Names:", value = f"```{', '.join(sorted(data['alt']))}```", inline = False)
		
		embed.add_field(name = "Description:", value = f"```Python\n{data['description']}```", inline = False)

		val1 = f"```python\n{re.sub(r'{}', prefix, data['usage'])}```" if data["usage"] else "None"
		embed.add_field(name = "Usage:", value = val1, inline = False)
		val2 = f"```{re.sub(r'{}', prefix, data['usage_example'])}```" if data["usage_example"] else "None"
		embed.add_field(name = "Usage example", value = val2, inline = False)
	embed.set_footer(text = f"You might want to do {prefix}help help to get a better understanding of the command usage")
	msg = await channel.send(embed = embed)
	message = stuff.message
	# print(type(message), type(msg))
	if not keep:
		await msg.delete(delay = 120)
		await message.delete(delay = 120)


async def hymmnos(stuff: u.Data):
	channel = stuff.channel
	content = stuff.message.clean_content.strip()
	
	lines = content.split("\n")
	# print(lines)
	height = len(lines)
	# print(height)
	width = len(max(lines, key = lambda x: len(x)))
	# print(width)
	im = Image.new("L", (40 * width, 55 * height), 255)
	draw = ImageDraw.Draw(im)
	width, height = draw.textsize(content, font = font)
	draw.text((10, 0), content, font = font, fill = 0)
	im = im.crop((0, 0, width + 40, height + 10))
	# im.save("other stuff/images/hymmnos" + str(author.id) + ".png")
	# file = discord.File("other stuff/images/hymmnos" + str(author.id) + ".png")
	file = await u.make_temp_file(im, "PNG", "hymmnos.png")
	await channel.send(file = file)


async def invite(stuff: u.Data):
	channel = stuff.channel
	client = stuff.client
	
	url = discord.utils.oauth_url(client_id = client.user.id, permissions = discord.Permissions.all())
	title = f"Invite {client.user.name}!"
	embed = r.emb_resp(title, "", "std_info", url)
	await channel.send(embed = embed)


async def info(stuff: u.Data):
	guild = stuff.message.guild
	channel = stuff.channel
	client = stuff.client
	uptime = u.get_bot_uptime()
	embed = r.emb_resp(
		f"Bot-info\n\nRunning since\n`{uptime}`",
		"```\"No purpose\"-bot\nSuggest new commands just by using them!```",
		"std_info"
	)
	bot = client.user
	embed.set_image(url = bot.avatar_url)
	embed.add_field(name = "Gitlab repository", value = "https://gitlab.com/lysandersage98/test_bot", inline = False)
	embed.add_field(name = "Is ~~in~~active in", value = f"```{len(client.guilds)} guilds```", inline = True)
	embed.add_field(name = "with", value = f"```{len(client.users)} Users in total```")
	embed.add_field(name = "ID", value = f"```{bot.id}```", inline = False)
	embed.add_field(name = "Created at", value = f"```{bot.created_at.strftime('%D - %T')}```", inline = True)
	date = guild.get_member(bot.id).joined_at.strftime("%D - %T")
	embed.add_field(name = "Joined this guild at", value = f"```{date}```")
	owner = client.get_user(u.data['owner'])
	url = owner.avatar_url
	embed.set_author(name = f"Created by {owner}", icon_url = url)
	embed.set_footer(text = f"Image source: {u.data['profile']}")
	await channel.send(embed = embed)


async def loop(stuff: u.Data):
	author = stuff.author
	channel = stuff.channel
	guild = channel.guild
	args = stuff.args

	if not author.voice:
		return "Error", "Nope!", "error"
	
	if guild:
		v_c = guild.voice_client
		
		if v_c:

			res = Player.set_loop(guild.id, (lambda x: "all" if len(x) == 0 else "".join(x))(args))
			try:
				if args[0] == "stop":
					await channel.send(embed = r.emb_resp("Loop disabled!", "", "success"))
				else:
					if not res:
						await channel.send(embed = r.emb_resp("Loop enabled!", "", "success"))
					else:
						await channel.send(embed = r.emb_resp("Info", res, "error_2"))
			except IndexError:
				await channel.send(embed = r.emb_resp("Loop enabled!", "", "success"))
		else:
			return "Error", "Nothing to loop!", "error"

	else:
		return "Error", "Not possible here!", "error"


async def morsecodetranslation(stuff: u.Data):
	channel = stuff.channel
	base_args = stuff.args
	coll = db.db.get_collection("Lists")
	text = " ".join(base_args).upper()
	args = re.sub("/", " /", text).split()
	mentions = stuff.message.raw_mentions
	# print(text)
	
	morse = coll.find_one({"name": "morse"})
	
	encrypt = morse["encrypt"]
	decrypt = morse["decrypt"]
	
	if "--replacements" in base_args:
		embed = r.emb_resp("Replacable characters", "", "std_info")
		embed.add_field(name = "Encryption", value = "```python\n\"" + "\"; \"".join(encrypt.keys()) + "\"```")
		embed.add_field(name = "Decryption", value = "```python\n\"" + "\"; \"".join(decrypt.keys()) + "\"```")
		await channel.send(embed = embed)
	else:
		names = []
		if mentions:
			use_mentions = True
			for _id in mentions:
				member = channel.guild.get_member(_id)
				names.append(member.nick or member.name)
			# print(names)
		else:
			use_mentions = False
		'''regex_replace = {
			".": "\.",
			"^": "\^",
			"$": "\$",
			"*": "\*",
			"+": "\+",
			"?": "\?",
			"{": "\}",
			"}": "\}",
			"[": "\[",
			"]": "\]",
			"\\": "\\",
			"|": "\|",
			"(": "\(",
			")": "\)"
		}'''
		
		if args and args[0] in decrypt:
			sub = "(" + " )|(".join(decrypt.keys()) + " )"
			sub = re.sub("\.", "\.", sub)
			# sub = re.sub("|".join(list(regex_replace.values())), lambda x: regex_replace[x.group()], sub)
			enc = False
		elif args and args[0][0].upper() in encrypt or names and names[0][0].upper() in encrypt:
			keys = list(encrypt.keys())
			special = keys.pop(keys.index("-"))
			sub = "[" + "".join(keys) + special + "]"
			enc = True
		else:
			return "Error", "No (valid) message given!", "error"
		
		# print(sub, enc)
		
		if enc:
			replace = encrypt
		else:
			replace = decrypt
		
		embed = r.emb_resp(f"Result of {'Encryption' if enc else 'Decryption'}", "", "success")
		
		text = " ".join(args).upper() if not use_mentions else " ".join(names).upper()
		desc = re.sub(sub, lambda x: replace[x.group()] if enc else replace[x.group().strip()], text + " ")
		desc = re.sub("/ *", " ", desc) if not enc else desc
		embed.description = f'```{desc}```'
		
		if enc:
			txt = f"Not the expected result? Take a look at the replacement-table with: {prefix}morse --replacements!"
			embed.set_footer(text = txt)
		
		await channel.send(embed = embed)
	

async def movesong(stuff: u.Data):
	channel = stuff.channel
	args = stuff.args
	guild = channel.guild
	if not guild:
		await channel.send(embed = r.emb_resp("Error", "Can only be used in a guild!", "error"), delete_after = 20.0)
		return
	
	v_c = guild.voice_client
	if not v_c:
		await channel.send(embed = r.emb_resp("Error", "*I don't think you will find anything to move...*", "error"))
		return
	
	try:
		x = int(args[0])
		y = int(args[1])
	except IndexError:
		return "Error", "Pressed \"Enter\" too early?", "error"
	except ValueError:
		return "Error", "Need numbers!", "error"
	res = Player.edit_queue(guild.id, x, y)
	return res


async def movetoposition(stuff: u.Data):
	channel = stuff.channel
	guild = channel.guild
	args = stuff.args
	
	if not guild:
		return "Nope", "Try again in a guild", "error"
	
	player = Player.get_player(guild.id)
	if not player:
		return "Nope", "Apparently there is nothing playing now", "error"
	
	try:
		number = float(args[0])
	except TypeError:
		return "Invalid type!", "Needs to be a positive or negative number!", "error"
	except IndexError:
		number = None
	
	player.seek(number or 5)
	await stuff.message.delete(delay = 10)


async def music_player(stuff: u.Data):
	args = stuff.args
	client = stuff.client
	message = stuff.message
	guild: discord.Guild = message.guild
	
	if not guild:
		return "Nope", "Only possible in a server!", "error"
	
	channel: discord.TextChannel = await message.guild.create_text_channel("music-player", overwrites = {
		message.guild.default_role: discord.PermissionOverwrite(send_messages = False)},
		category = message.channel.category, position = message.channel.position - 1)
	
	embed = r.emb_resp("Music-menu", "Now playing: ")
	embed.colour = 0x666666
	
	base = ["⬅", "➡", "⚙"]
	settings = {
		"🆕": 1,
		"⏯": 1,
		"⏹": 0,
		"⏪": 1,
		"⏩": 1,
		"⏭": 1,
		"🔀": 0,
		"🔁": 1,
		"↔": 2,
		"⬅": 0,
		"➡": 0,
		"➕": 1,
		"➖": 1
	}
	number_reactions = {
		"0️⃣": "0",
		"1️⃣": "1",
		"2️⃣": "2",
		"3️⃣": "3",
		"4️⃣": "4",
		"5️⃣": "5",
		"6️⃣": "6",
		"7️⃣": "7",
		"8️⃣": "8",
		"9️⃣": "9",
		"🆗": ""
	}
	
	toggle = 0
	data = {
		"type": "gui",
	}
	
	msg = await channel.send(embed = embed)
	[await msg.add_reaction(x) for x in settings.keys()]
	
	def check(check_reaction: discord.Reaction, check_user):
		check_emoji = check_reaction.emoji
		return check_reaction.message == msg and (check_emoji in base or check_emoji in settings or check_emoji in numbers)
	
	v_c = guild.voice_client
	if v_c:
		return "Only configured to work if there isn't any music playing yet"

	# vc = await stuff.author.voice.channel.connect(cls = VoiceClientCopy)
	reaction_cache = []
	setting = 0
	numbers = []
	
	async def toggle_reactions(tgl):
		tgl ^= 1
		await msg.clear_reactions()
		if tgl:
			[await msg.add_reaction(x) for x in number_reactions.keys()]
		else:
			[await msg.add_reaction(x) for x in settings.keys()]

	while True:
		try:
			reaction, user = await client.wait_for("reaction_add", check = check, timeout = 30)
			emoji = reaction.emoji
			print("reaction = ", reaction)
			users = await reaction.users().flatten()
			for user in users:
				if user != client.user:
					await reaction.remove(user)
			
			if emoji == "🆕":
				pass
			
			elif emoji == "⏯":
				setting = settings[emoji]
				await toggle_reactions(toggle)
			
			elif emoji == "⏹":
				pass
			
			elif emoji == "⏪":
				pass
			
			elif emoji == "⏩":
				pass
			
			elif emoji == "⏭":
				pass
			
			elif emoji == "🔀":
				pass
			
			elif emoji == "🔁":
				pass

			elif emoji == "↔":
				setting = settings[emoji]
				await toggle_reactions(toggle)
				
			elif emoji == "⬅":
				pass
			
			elif emoji == "➡":
				pass
			
			elif emoji in list(number_reactions.keys())[:-1]:
				await msg.clear_reactions()
				reaction_cache.append(number_reactions[emoji])
				
			elif emoji == "🆗":
				if setting == 2:
					numbers.append(int("".join(reaction_cache)))
					reaction_cache.clear()
					if len(numbers) == 2:
						setting = 0
						await toggle_reactions(toggle)
						res = Player.edit_queue(guild.id, numbers[0], numbers[1])
						numbers.clear()
				else:
					value = re.match("\d+", "".join(args))
					if value:
						value = int(value.group())
					else:
						value = " ".join(args)
					res = Player.skip(channel.guild.id, value)
		
		except TimeoutError:
			await channel.delete(reason = "Out of use")
			break


async def mycolor(stuff: u.Data):
	client = stuff.client
	channel = stuff.channel
	author = stuff.author
	mentions = stuff.message.raw_mentions
	args = stuff.args
	
	if mentions:
		mention = mentions[0]
		user = client.get_user(mention)
		text = f"`{user.name}`'s color is"
		color, rgb = u.calc_color_rgb(user.name)
		# color = hex(sum(map(ord, user.name))).split("x")[1].zfill(6)
		# rgb = tuple(map(lambda x: int(x, base = 16), tuple(map("".join, tuple(zip(color[::2], color[1::2]))))))
		
	elif args:
		to_convert = "".join(args)
		text = "The color of the given text is"
		color, rgb = u.calc_color_rgb(to_convert)
		# color = hex(sum(map(ord, to_convert))).split("x")[1].zfill(6)
		# rgb = tuple(map(lambda x: int(x, base = 16), tuple(map("".join, tuple(zip(color[::2], color[1::2]))))))
	
	else:
		text = "Your color is"
		color, rgb = u.calc_color_rgb(author.name)
		# color = hex(sum(map(ord, author.name))).split("x")[1].zfill(6)
		# rgb = tuple(map(lambda x: int(x, base = 16), tuple(map("".join, tuple(zip(color[::2], color[1::2]))))))
	
	im = Image.new("RGB", size = (128, 128), color = rgb)
	# im.save(f"other stuff\\images\\{str(author.id)}.png")

	# f = discord.File(f"other stuff\\images\\{author.id}.png")
	file = await u.make_temp_file(im, "PNG", f"{author.id}_color.png")
	await channel.send(f"{text} `HEX: 0x{color} / RGB: {rgb}`:", file = file)


'''async def myping(stuff: u.Data):
	channel = stuff.channel
	message = stuff.message
	
	now = datetime.now()
	async with aiohttp.request("GET", "https://www.time.is/Unix_time_now") as resp:
		res = resp.url.parts[1]
		html = await resp.text()
		text = bs4.BeautifulSoup(html, "html.parser")
		tm = text.find("time").string
		tm = str(datetime.utcfromtimestamp(int(tm)) - message.created_at)
		embed = r.emb_resp("Ping", tm, "info")
	await channel.send(embed = embed)'''


async def nowplaying(stuff: u.Data):
	client = stuff.client
	channel = stuff.channel
	guild = channel.guild
	author = stuff.author
	coll = db.db.get_collection("User_Songs")
	if not guild:
		await channel.send("*Get out*")
		return
	
	v_c = guild.voice_client
	if not v_c:
		return "Error", "Not connected!", "error"
	
	data = Player.get_current(channel.guild.id)
	
	if not data:
		return "Error", "No info found!", "error_2"
	time_passed_info = data.get("passed")
	# time_passed_info.seek(0)
	# lines = list(filter(lambda x: "info" in x, time_passed_info.readlines()))
	# print(len(lines))
	# time_passed = "?"
	# if lines:
	# 	time_passed = re.search("time=(\S*)", lines[-1])
	# 	if time_passed:
	# 		time_passed = time_passed.group(1)
	# loops = data["loops"]
	# if loops:
	# 	time = loops * 20/60000
	# 	time_passed = timedelta(minutes = time)
	# else:
	# 	time_passed = "❔"
	# print(time_passed_info)
	time_passed = timedelta(seconds = time_passed_info)
	final = f"[{data['title']}]({data['link']})"
	thumbnail = data["thumbnail"]
	
	passed = {
		"name": "Time passed since start",
		"value": f"```{time_passed}```",
		"inline": False
	}
	dur = {
		"name": "Duration",
		"value": f"```{data['duration']}```",
		"inline": False
	}
	ffmpeg_info = data["ffmpeg_options"]
	embed = r.emb_resp("Currently playing:", final, "success").add_field(**passed).add_field(**dur)
	# print(ffmpeg_info)
	if ffmpeg_info and list(filter(lambda x: x[1], ffmpeg_info)):
		times = ffmpeg_info[1][1]
		starting_point = re.search("-ss (\S*)", times).group(1)
		ending_point = re.search("-to (\S*)", times).group(1)
		start = {
			"name": "Starting point in the track",
			"value": f"```{starting_point}```"
		}
		end = {
			"name": "Ending point in the track",
			"value": f"```{ending_point}```"
		}
		embed.add_field(**start).add_field(**end)
	if thumbnail:
		embed = embed.set_thumbnail(url = thumbnail)
	if "description" in stuff.args:
		embed.add_field(name = "Video Description", value = "_ _" if data["description"] else "❔", inline = False)
		u.separate_2(embed, discord.utils.escape_markdown(data["description"]), "\n")
	try:
		embed.add_field(name = "Buffer progress", value = data["buffer_status"])
		await stuff.message.delete(delay = 60.0)
		msg = await channel.send(embed = embed, delete_after = 60.0)
	except discord.HTTPException:
		while True:
			try:
				embed.remove_field(-1)
				msg = await channel.send(embed = embed, delete_after = 60.0)
				break
			except discord.HTTPException:
				pass
			except Exception as e:
				print(e)
				return
	except Exception as e:
		print(e)
		return
	
	def check1(check_reaction, check_user):
		return check_user == author and check_reaction.message.id == msg.id
	
	def check2(check_message: discord.Message):
		c_1 = check_message.author == author
		c_2 = check_message.channel == channel
		c_3 = not check_message.content.startswith(prefix)
		return c_1 and c_2 and c_3
	
	await msg.add_reaction("➕")
	await msg.add_reaction("➖")
	reaction: discord.Reaction
	user: discord.User
	
	while True:
		try:
			reaction, user = await client.wait_for("reaction_add", check = check1, timeout = 60.0)
		except TimeoutError:
			break
		info_msg = await channel.send("Type the name of the playlist you want to modify!")
		try:
			playlist_name_msg = await client.wait_for("message", check = check2, timeout = 60.0)
			playlist_name = playlist_name_msg.content
			await playlist_name_msg.delete(delay = 20.0)
			await info_msg.delete(delay = 20.0)
		except TimeoutError:
			await info_msg.edit(content = "Timed out!")
			await info_msg.delete(delay = 20.0)
			continue
		
		if reaction.emoji == "➕":
			response = f"Added {data['title']} to playlist `{playlist_name}`!"
			try:
				coll.find_one_and_update(
					{
						"user": user.id,
						"name": playlist_name
					}, {
						"$addToSet": {
							"songs": (data["link"], data["title"])
						}
					}, upsert = True
				)
			except Exception as e:
				print(e)
		elif reaction.emoji == "➖":
			response = f"Removed {data['title']} from playlist `{playlist_name}`!"
			coll.find_one_and_update(
				{
					"user": user.id,
					"name": playlist_name
				}, {
					"$pull": {
						"songs": (data["link"], data["title"])
					}
				}, upsert = True
			)
		else:
			continue
		await channel.send(embed = r.emb_resp("Info", response, "success"), delete_after = 10.0)
	

async def pause(stuff: u.Data):
	channel = stuff.channel
	guild = channel.guild
	nick = guild.me.nick
	if not guild:
		await channel.send("***HMMMMM***")
		return
	v_c = guild.voice_client
	if not v_c:
		await channel.send(":x: Nothing there to pause!")
		return
	if stuff.message.author not in v_c.channel.members:
		await channel.send("*Nope*")
		return
	if not v_c.is_paused():
		await stuff.message.delete(delay = 10)
		v_c.pause()
		await guild.me.edit(nick = "[PAUSED] " + nick)
	

async def pokemon(stuff: u.Data):
	author = stuff.author
	channel = stuff.channel
	guild = stuff.message.guild
	client = stuff.client
	args = stuff.args
	
	if not args:
		return await channel.send(embed = r.emb_resp("Wrong usage!", "Look at the help site for this command", "error"))
	
	coll = db.db.get_collection(name = "User_stats")
	
	try:
		if "profile" not in args and coll.find_one({"id": author.id})["state"]["value"] == "dead":
			try:
				await channel.last_message.delete()
			except Exception as e:
				print(e)
			return await channel.send(
				embed = r.emb_resp("Nope", f"_```This is no place for the undead!``` {author.mention}_", "info"))
	except Exception as e:
		print(e)
		
	subs = "profile learn learned registered"
	pos = u.dmp.match_bitap(subs, args[0], 0)
	if pos != -1:
		args[0] = subs[pos: ((lambda x: x if x != -1 else None)(subs.find(" ", pos)))]
	
	if args[0] == "update":
		base_url = "https://www.pokewiki.de/"
		async with aiohttp.request("GET", base_url + "Attacken-Liste") as resp:
			html = await resp.text()
			table = re.search(r"darkBg1.*/table>\n", html, re.DOTALL).group()[10:]
		table = re.split(r"<td><span style=\"background-color:.*", table)
		table2 = []
		for obj in table:
			table2.append(html2text.html2text(obj))
		print(len(table), table2[0])
		
		for attack in table2:
			atk = {}
			
			text = attack.split("|")
			try:
				atk['nr'] = int(text[0].strip())
				atk['title'] = re.search(r"\[\w*]", text[1]).group()[1:-1]
				atk['dmg'] = int(text[4].strip().strip("*"))
				atk['rate'] = int(text[5].strip().strip("*"))
				atk['AP'] = int(text[6].strip().strip("*"))
				coll.insert_one(atk)
			except Exception as e:
				print(e, e.__traceback__.tb_lineno)
		return
	
	elif args[0] == "profile":
		embed = await p.get_profile(author)
		return await channel.send(embed = embed)
	
	elif args[0] == "learn":
		if len(args) >= 2:
			print(args)
			player = coll.find_one({"id": author.id, "learned attacks.title": args[1].title()})
			
			if not player:
				return await channel.send(embed = r.emb_resp("", "```You haven't learned that attack!```", "error"))
			if args[1].title() not in list(map(lambda val: val["title"], player["usable attacks"])):
				for atk in player["learned attacks"]:
					if atk["title"] == args[1].title():
						new_atk = atk
						break
				else:
					return
			else:
				return "❌ Nope", "You already know that attack!", "error"
				
			attacks = player["usable attacks"]
			
			if len(attacks) < 4:
				return await channel.send(
					embed = r.emb_resp("", "```You can only do that when you have learned more than 4 attacks!```", "error"))
			elif atk in attacks:
				return "Nope", "```You can't learn an attack you already have!```", "error"
			
			tokens = player.get("tokens")

			if tokens:
				msg = await channel.send("This action costs one learning token. Proceed?")
			else:
				msg = await channel.send("This action costs 5 AP of one of the Attacks you have. Proceed?")
			emoji = await u.reaction(msg, author, client)
			
			if emoji == "❌":
				return await msg.edit(content = "Ok, no changes were made!")
			
			elif emoji == "✅":
				title = "Select the attack you want to forget"
				description = f"```python\nTo learn the new attack \"{new_atk['title']}\"```"
				embed = r.emb_resp(title, description, "std_info")
				number = 1
				
				for atk in attacks:
					# print(atk)
					embed.add_field(name = str(number), value = f"```{atk['title']}```", inline = False)
					number += 1
					
				await msg.clear_reactions()
				msg = await channel.send(embed = embed)
				await msg.add_reaction("1⃣")
				await msg.add_reaction("2⃣")
				await msg.add_reaction("3⃣")
				await msg.add_reaction("4⃣")
				
				def check(check_reaction, check_user):
					return check_user == author and check_reaction.message.id == msg.id
				
				try:
					reaction, user = await client.wait_for("reaction_add", check = check, timeout = 600)
					atk_number = None
					if reaction.emoji == "1⃣":
						print(1)
						selection = [1, 2, 3]
						while True and not tokens:
							try:
								atk_number = random.choices(selection)
								print(atk_number)
							except IndexError:
								await msg.clear_reactions()
								await msg.delete()
								return await channel.send(
									embed = r.emb_resp("", "```No Attack with more than 0 Ap left!```", "info"))
							if attacks[atk_number[0]]["AP"] > 0:
								break
							
							selection.pop(atk_number[0] - 1)
						
						coll.update_one({"id": author.id}, {
							"$set": {
								"usable attacks.0": new_atk
							}, "$inc": {
								"tokens": -1 if tokens else 0
							}
						})
						if not tokens:
							coll.update_one({"id": author.id}, {"$inc": {f"usable attacks.{atk_number[0]}.AP": -5}})
							info_msg = f"5 Ap deducted from \"{attacks[atk_number[0]]['title']}\"!"
						else:
							info_msg = "One token deducted!"
						
						embed.description = f"```python\nOk, \"{embed.fields[0].value.strip('```')}\" successfully " \
											f"overwritten!\n{info_msg}``` "
					
					elif reaction.emoji == "2⃣":
						print(2)
						selection = [0, 2, 3]
						while True and not tokens:
							try:
								atk_number = random.choices(selection)
								print(atk_number)
							except IndexError:
								await msg.clear_reactions()
								await msg.delete()
								return await channel.send(
									embed = r.emb_resp("", "```No Attack with more than 0 Ap left!```", "error"))
							
							if attacks[atk_number[0]]["AP"] > 0:
								break
							if atk_number[0] == 0:
								selection.pop(0)
							else:
								selection.pop(atk_number[0] - 1)
						
						coll.update_one({"id": author.id}, {
							"$set": {
								"usable attacks.1": new_atk
							}, "$inc": {
								"tokens": -1 if tokens else 0
							}
						})
						if not tokens:
							coll.update_one({"id": author.id}, {"$inc": {f"usable attacks.{atk_number[0]}.AP": -5}})
							info_msg = f"5 Ap deducted from \"{attacks[atk_number[0]]['title']}\"!"
						else:
							info_msg = "One token deducted!"
						
						embed.description = f"```python\nOk, \"{embed.fields[1].value.strip('```')}\" successfully " \
											f"overwritten!\n{info_msg}``` "
					
					elif reaction.emoji == "3⃣":
						print(3)
						selection = [0, 1, 3]
						while True and not tokens:
							try:
								atk_number = random.choices(selection)
								print(atk_number)
							except IndexError:
								await msg.clear_reactions()
								await msg.delete()
								return await channel.send(
									embed = r.emb_resp("", "```No Attack with more than 0 Ap left!```", "error"))
							
							if attacks[atk_number[0]]["AP"] > 0:
								break
							
							if atk_number[0] == 3:
								selection.pop(-1)
							else:
								selection.pop(atk_number[0])
						
						coll.update_one({"id": author.id}, {
							"$set": {
								"usable attacks.2": new_atk
							}, "$inc": {
								"tokens": -1 if tokens else 0
							}
						})
						if not tokens:
							coll.update_one({"id": author.id}, {"$inc": {f"usable attacks.{atk_number[0]}.AP": -5}})
							info_msg = f"5 Ap deducted from \"{attacks[atk_number[0]]['title']}\"!"
						else:
							info_msg = "One token deducted!"
						
						embed.description = f"```python\nOk, \"{embed.fields[2].value.strip('```')}\" successfully " \
											f"overwritten!\n{info_msg}``` "
					
					elif reaction.emoji == "4⃣":
						print(4)
						selection = [0, 1, 2]
						while True and not tokens:
							try:
								atk_number = random.choices(selection)
								print(atk_number)
							except IndexError:
								await msg.clear_reactions()
								await msg.delete()
								return await channel.send(
									embed = r.emb_resp("", "```No Attack with more than 0 Ap left!```", "error"))
							
							if attacks[atk_number[0]]["AP"] > 0:
								break
							selection.pop(atk_number[0])
						
						coll.update_one({"id": author.id}, {
							"$set": {
								"usable attacks.3": new_atk
							}, "$inc": {
								"tokens": -1 if tokens else 0
							}
						})
						if not tokens:
							coll.update_one({"id": author.id}, {"$inc": {f"usable attacks.{atk_number[0]}.AP": -5}})
							info_msg = f"5 Ap deducted from \"{attacks[atk_number[0]]['title']}\"!"
						else:
							info_msg = "One token deducted!"
						
						embed.description = f"```python\nOk, \"{embed.fields[3].value.strip('```')}\" successfully " \
											f"overwritten!\n{info_msg}``` "
				
				except TimeoutError:
					if not tokens:
						
						selection = [0, 1, 2, 3]
						while True:
							try:
								atk_number = random.choices(selection)
								print(atk_number)
							except IndexError:
								await msg.clear_reactions()
								await msg.delete()
								return await channel.send(
									embed = r.emb_resp("", "```No Attack with more than 0 Ap left!```", "error"))
							
							if attacks[atk_number[0]]["AP"] > 0:
								break
							selection.pop(atk_number[0])
						
						coll.update_one({"id": author.id}, {"$inc": {f"usable attacks.{atk_number[0]}.AP": -5}})
						
						embed.description = f"```You took too long to decide!\nNo attack overwritten! (Costs will be deducted " \
											f"anyway)\n\n5 Ap deducted from {attacks[atk_number[0]]['title']}!``` "
					else:
						coll.update_one({"id": author.id}, {"$inc": {"tokens": -1}})
						embed.description = "f```You took too long to decide!\nOne token will be deducted anyway!```"

				embed.clear_fields()
				await msg.edit(embed = embed)
				return await msg.clear_reactions()
			
			else:
				return await channel.send(
					"```\"Oak's words echoed...\n There's a time and place for everything, but not now.\"```")
		else:
			return await channel.send(embed = r.emb_resp("Error!", "No attack name given!", "info"))
	
	elif args[0] == "learned":
		sort_by = ""
		inverse = False
		res = re.search("--sort (-)?(dmg|ap|rate|title)", stuff.message.content)
		if res:
			sort_by = res.group(2)
			inverse = res.group(1)
		try:
			args = [int(args[1]), int(args[2])]
		except IndexError:
			args = []
		except ValueError:
			args = []
		
		embed = await p.get_attacks(author, sort_by, inverse, *args)
		return await channel.send(embed = embed)
	
	elif args[0] == "registered":
		users = coll.find()
		users = users.sort("name", 1)
		# if len(args) > 1:
		# 	place = "from all servers"
		# else:
		place = "in " + guild.name
		text = f"__**Registered users {place}:**__\n"
		for user in users:
			if place != "from all servers":
				member = guild.get_member(user["id"])
				if member and not member.bot:
					_new = (lambda old, new: old if old == new else f"{new} (Registered with: {old})")(user["name"], member.name)
					text += f"- {discord.utils.escape_markdown(_new)}\n"
			else:
				member = client.get_user(user["id"])
				if member and not member.bot:
					_new = (lambda old, new: old if old == new else f"{new} (Registered with: {old})")(user["name"], member.name)
					text += f"- {discord.utils.escape_markdown(_new)}\n"
				
		return await channel.send(text)
	
	elif args[0] == "gamble":
		gambler = coll.find_one(
			{
				"id": author.id,
			}
		)
		# if not gambler or (gambler.get("status") and gambler["status"]["value"] != "alive"):
		# 	return "Nope", "Only possible when you are alive", "error"
		msg = await channel.send(embed = r.emb_resp(
			"Info", "Ready to pay 10% of your max HP to give this a try?", "std_info"
		))
		await msg.add_reaction("✅")
		await msg.add_reaction("❌")
		
		def check(check_reaction: discord.Reaction, check_user):
			c_1 = check_reaction.message == msg
			c_2 = check_user == author
			return c_1 and c_2
		
		while True:
			try:
				reaction, user = await client.wait_for("reaction_add", check = check, timeout = 60.0)
			except TimeoutError:
				await msg.edit(embed = r.emb_resp("Cancelled", "You took too loong to decide", "info"))
				return
			
			if reaction.emoji == "✅":
				gamble = True
				break
			elif reaction.emoji == "❌":
				gamble = False
				break
				
		if not gamble:
			await msg.clear_reactions()
			await msg.edit(embed = r.emb_resp("Ok", "No gamble", ""))
			return
		
		embed = r.emb_resp("Info", "Choose a symbol", "info")
		values = "```python\n'triangle': 1 token\n'square': 2 tokens\n'diamond': 3 tokens\n'circle': 4 tokens```"
		embed.add_field(name = "Rewards", value = values)
		await msg.edit(embed = embed)
		await msg.clear_reactions()
		symbols = {
			"triangle": "🔺",  # 1 token
			"square": "🟧",  # 2 token
			"diamond": "🔷",  # 3 token
			"circle": "🟢"  # 4 token
		}
		await msg.add_reaction(symbols["triangle"])
		await msg.add_reaction(symbols["square"])
		await msg.add_reaction(symbols["diamond"])
		await msg.add_reaction(symbols["circle"])
		
		status_change_dic = {
			"$set": {
				"state": {
					"$switch": {
						"branches": [
							{
								"case": {
									"$lte": ["$hp", 0]
								},
								"then": {
									"value": "dead",
									"in": None,
									"since": datetime.today().strftime("%Y/%m/%d")
								}
							}
						],
						"default": {
							"value": "alive",
							"in": None,
							"since": datetime.today().strftime("%Y/%m/%d")
						}
					}
				}
			}
		}

		while True:
			try:
				reaction, user = await client.wait_for("reaction_add", check = check, timeout = 60.0)
			except TimeoutError:
				coll.update_one(
					{
						"id": author.id
					}, [
						{
							"$set": {
								"hp": {
									"$add": ["$hp", -gambler["max hp"] * 0.05]
								}
							}
						},
						status_change_dic
					]
				)
				await msg.clear_reactions()
				return "Timed out", "Took 5% max-HP anyway", "info"
			
			if reaction.emoji == symbols["triangle"]:
				amount = 1
				selected = symbols["triangle"]
				break
			elif reaction.emoji == symbols["square"]:
				amount = 2
				selected = symbols["square"]
				break
			elif reaction.emoji == symbols["diamond"]:
				amount = 3
				selected = symbols["diamond"]
				break
			elif reaction.emoji == symbols["circle"]:
				amount = 4
				selected = symbols["circle"]
				break
		
		await msg.clear_reactions()
		text = f"You selected {selected}" \
			f" and will receive {amount} token{'s' if amount > 1 else ''} if you choose the right option"
		embed = r.emb_resp("Info", text, "info")
		await msg.edit(embed = embed)
		
		options = [*[symbols["triangle"]]*4, *[symbols["square"]]*3, *[symbols["diamond"]]*2, symbols["circle"]]
		random.shuffle(options)
		choices = {str(val[0]) + "\ufe0f\u20e3": val[1] for val in zip(range(10), options)}
		# await channel.send(choices)
		[await msg.add_reaction(key) for key in choices.keys()]

		while True:
			try:
				reaction, user = await client.wait_for("reaction_add", check = check, timeout = 60.0)
			except TimeoutError:
				await msg. clear_reactions()
				coll.update_one(
					{
						"id": author.id
					}, [
						{
							"$set": {
								"hp": {
									"$add": ["$hp", -gambler["max hp"] * 0.1]
								}
							}
						},
						status_change_dic
					]
				)
				return "Timed out!", "10% of your max HP will be deducted anyway!", "info"
			selection = choices.get(reaction.emoji)
			if selection == selected:
				won = True
				embed = r.emb_resp("Congrats!", f"You won {amount} token{'s' if amount > 1 else ''}", "success")
				await msg.edit(embed = embed)
				break
			elif selection:
				won = False
				embed = r.emb_resp("Nope", f"That was {selection}", "info")
				await msg.edit(embed = embed)
				break
		await msg.clear_reactions()
		
		if won:
			update = [
				{
					"$set": {
						"tokens": {
							"$add": ["$tokens", amount]
						} if gambler.get("tokens") else amount,
						"hp": {
							"$add": ["$hp", -gambler["max hp"] * 0.1]
						}
					},
				},
				status_change_dic
			]
		else:
			update = [
				{
					"$set": {
						"hp": {
							"$add": ["$hp", -gambler["max hp"] * 0.1]
						}
					}
				},
				status_change_dic
			]
			
		coll.update_one(
			{
				"id": author.id
			}, update
		)
		return
	
	now = datetime.now().strftime("%Y/%m/%d - %H:%M:%S")
	
	if "genesung" not in "".join(args).lower() and len(args) < 2:
		return await channel.send(embed = r.emb_resp("*Question*", "Who are you trying to attack?", "info"))
	
	try:
		target_id = next(filter(lambda x: re.search(r"([0-9])+", x), args))
		target = client.get_user(int(re.search(r"([0-9])+", target_id).group()))
		print(target_id, target)
		args.remove(target_id)
	except Exception as e:
		print(e)
		target = author
	if not hasattr(target, "id"):
		return "Error", "Target couldn't be found!", "error"
	atk = args.pop(0).title()
	# atk_list: str = db.db.Lists.find_one({"name": "strings"})["attacken"]
	# print(atk_list)
	# pos = u.dmp.match_bitap(atk_list, atk, 0)
	# print(pos)
	# if pos != -1:
	# 	atk = atk_list[pos:((lambda x: x if x != -1 else None)(atk_list.find(" ", pos)))]
	# 	print(atk)
	# print(target.permissions_in(channel))
	if db.db.get_collection(name = "Attacken").find_one({"title": atk}):
		
		topic = client.user.name + " Battleground"
		if channel.topic != topic:
			all_text_channels = [
				(ch, ch.topic) for ch in channel.guild.channels if ch.type == discord.enums.ChannelType.text
			]
			valid_channels = list(filter(lambda x: x[1] == topic, all_text_channels))
			if not valid_channels:
				desc = f"Only allowed in channels with the topic: `{topic}`, do you want to create one now?"
				embed = r.emb_resp("Wrong channel!", desc, "error")
				msg = await channel.send(embed = embed)
				reaction = await u.reaction(msg, author, client)
				if reaction == "✅":
					try:
						new_ch = await msg.guild.create_text_channel(name = "New_Channel",
																	 category = msg.channel.category,
																	 topic = f"{client.user.name} Battleground",
																	 reason = "Created for pokémon battles between members")
						await msg.edit(content = f"{author.mention} go to {new_ch.mention}", embed = None)
					except discord.Forbidden:
						txt = "I don't have the necessary permissions to create the channel" \
							  "you have to do it manually!"
						await msg.edit(embed = r.emb_resp("Error", txt, "error"))
				elif reaction == "❌":
					embed.title = "OK"
					embed.description = "Channel not created!"
					await msg.edit(embed = embed)
				await msg.clear_reactions()
			else:
				goto = valid_channels[0][0]
				await channel.send(f"{author.mention} go to {goto.mention}!")
			return
		attacker = coll.find_one(
			{
				"id": author.id,
				"learned attacks.title": atk
			},
			projection = {
				"_id": False
			}
		)
		_nr = None
		if atk == "Genesung":
			delta = (datetime.now() - datetime.strptime(attacker["last attack"],
														"%Y/%m/%d - %H:%M:%S")).total_seconds()
			if delta <= 300.0:
				return await channel.send(embed = r.emb_resp("Nope", f"```Your last attack has to be at least 5 "
					f"minutes ago before you can attack again!\nNext attack possible in: "
					f"{int((300 - delta) / 60)} minutes and {int((300 - delta) % 60)} seconds```", "info"))

			curr_hp = attacker["hp"]
			max_hp = attacker["max hp"]
			if curr_hp == max_hp:
				return "", "As much as I would like to see you struggle after wasting AP, " \
					"I choose to be nice and stop you now.", "error"
			nr = 0
			for attack in attacker["usable attacks"]:
				_nr = nr
				nr += 1
				if attack["title"] == atk and attack["AP"] > 0:
					break
			else:
				return await channel.send(embed = r.emb_resp("", "```Not available!```", "info"))
			hp = curr_hp + max_hp * 0.5
			if hp > max_hp:
				hp = max_hp
		
		elif author and (author.id == target.id):
			return await channel.send(embed = r.emb_resp("", "```I don't think that's a good idea...```", "info"))
		
		else:
			if attacker:

				if attacker.get("state") and attacker["state"]["value"] == "passive":
					embed = r.emb_resp("Nope", "You are invincible for a price", "error")
					return await channel.send(embed = embed)
				elif attacker.get("state") and attacker["state"]["value"] == "muted":
					embed = r.emb_resp("Nope", "You are too aggressive, cool down a bit", "info")
					return await channel.send(embed = embed)
				hit = [1, 0]
				hits = [2, 3, 4, 5]
				modifier = attacker["init"]
				a1 = modifier / a
				b1 = modifier / b
				c1 = modifier / c
				d1 = modifier / d
				chance = [1 / 3 + d1, 1 / 3 + c1, 1 / 6 + b1, 1 / 6 + a1]
				_hits = random.choices(hits, chance).pop()
				delta = (datetime.now() - datetime.strptime(attacker["last attack"],
															"%Y/%m/%d - %H:%M:%S")).total_seconds()
				if delta > 300.0:
					
					attacks = attacker["usable attacks"]
					nr = 0
					# print(attacker)
					
					for attack in attacks:
						# print(attack, atk)
						_nr = nr
						nr += 1
						
						if attack["title"] == atk and attack["AP"] > 0:
							trgt = coll.find_one({"id": target.id, "hp": {"$gt": 0}})
							if trgt:
								# print(trgt["name"])
								diff = abs(attacker["init"] - trgt["init"])
								norm = math.ceil(diff / 100) * 100
								print(diff)
								rate = attack["rate"]
								if trgt.get("state") and trgt["state"]["value"] == "passive":
									embed = r.emb_resp("Nope", "This player is currently invincible", "error")
									return await channel.send(embed = embed)
								
								elif rate == 100 or random.choices(
									hit,
									[diff / norm * rate / 100, 1 - diff / norm * rate / 100]
								).pop():
									
									if attack["dmg"] >= 20:
										crit, dmg, result = await p.update(trgt, (attack, attacker))
										
										coll.update_one(
											{
												"id": attacker["id"]
											},
											{
												"$set": {
													"last attack": now,
													"aggro": 0
												},
												"$inc": {
													f"usable attacks.{_nr}.AP": -1
												}
											}
										)
										
										if result == "dead":
											await p.mute(target, channel, f"Defeated by {author.name}", "day")
										crit_info = "\nIt was a critical hit!" if crit else ""
										return await channel.send(f"{author.mention} used {attack['title']} against "
											f"{target.mention} and did {dmg} damage!\n{target.name} is {result}\n" + crit_info)
									
									else:
										crit, dmg, result = await p.update(trgt, attack, _hits)
										
										coll.update_one(
											{
												"id": attacker["id"]
											},
											{
												"$set": {
													"last attack": now,
													"aggro": 0
												},
												"$inc": {
													f"usable attacks.{_nr}.AP": -1
												}
											}
										)
										
										if result == "dead":
											await p.mute(target, channel, f"Defeated by {author.name}", "day")
										crit_info = "\nIt was a critical hit!" if crit else ""
										return await channel.send(f"{author.mention} used {attack['title']} against "
											f"{target.mention} and did {dmg} damage! (Hits: {_hits})\n{target.name} is {result}!" + crit_info)
								
								else:
									coll.update_one(
										{
											"id": attacker["id"]
										},
										{
											"$set": {
												"last attack": now,
												"aggro": 0
											},
											"$inc": {
												f"usable attacks.{_nr}.AP": -1
											}
										}
									)
									return await channel.send(embed = r.emb_resp("", "```Attack missed!```", "info"))
							
							else:
								return await channel.send(embed = r.emb_resp("INFO",
									"```Your target is either already dead or not registered yet!```", "std_info"))
						
						elif attack["title"] == atk and attack["AP"] <= 0:
							return await channel.send(embed = r.emb_resp("Not enough AP left!", "", "error"))
					
					return await channel.send(embed = r.emb_resp("Error!", "```You can't use that attack!```", "error"))
				
				else:
					msg = await channel.send(embed = r.emb_resp("Nope", f"```Your last attack has to be at least 5 "
																		f"minutes ago before you can attack "
																		f"again!\nNext attack possible in: "
																		f"{int((300 - delta) / 60)} minutes and "
																		f"{int((300 - delta) % 60)} seconds```", "error"))
					if attacker.get("aggro") and attacker["aggro"] == 2:
						coll.update_one({"id": author.id}, {"$set": {"aggro": 0}})
						await p.mute(author, channel, "Too aggressive", "temp")
					else:
						coll.update_one({"id": author.id}, {"$inc": {"aggro": 1}})
						await msg.edit(content = "Aggro increased by 1")
					return
			
			else:
				return await channel.send(embed = r.emb_resp("Info", "```You didn't learn that attack yet!```", "error"))
	
		coll.update_one(
			{
				"id": attacker["id"]
			},
			{
				"$set": {
					"last attack": now,
					"hp": hp
				},
				"$inc": {
					f"usable attacks.{_nr}.AP": -1
				}
			}
		)
		return await channel.send(f"{author.mention} used {attack['title']} and restored {hp - curr_hp} HP!")
	
	else:
		await channel.send(embed = r.emb_resp("", f"```The attack \"{atk}\" doesn't exist here!```", "error"))


async def queue(stuff: u.Data):
	channel = stuff.channel
	client = stuff.client
	author = stuff.author
	guild = channel.guild
	coll = db.db.get_collection("User_Songs")
	
	if not guild:
		await channel.send("Nope")
		return
	v_c = guild.voice_client
	
	if not v_c:
		await channel.send(embed = r.emb_resp("Info", "Not connected to voice channel!", "error"))
		return
	
	if not channel.guild:
		return "Hm.......", "Why do you think that might work?", "error_2"
	res = Player.get_queue_info(channel.guild.id)
	if not res:
		return "Empty", "No queue was found", ""
	all_items, loop_value = res
	if not all_items:
		return "Empty", "Queue is empty", ""
	
	def transform(data: dict):
		return f"{data['title']} ({data['duration']})"
	
	current = all_items[0]
	current = transform(current)
	items = all_items[1:]
	durations = map(lambda x: x["duration"].seconds, items)
	queue_duration = timedelta(seconds = sum(durations))
	# print(queue_duration)
	count = 0
	
	def get_short():
		return items[count * 10:(count + 1) * 10]
	
	async def embed(lis):
		try:
			# print(lis)
			text = list(map(lambda x: f"\n[{x[0] + count * 10}] " + transform(x[1]), enumerate(lis, start = 1)))
			q = f"{discord.utils.escape_markdown(''.join(text))}"
			# print(text)
			emb = r.emb_resp(f"Page {count + 1}/{math.ceil(len(items)/10) or 1}", "", "std_info")
			emb.set_author(name = f"Size: {len(items)}")
			emb.add_field(name = "Currently Playing", value = f"[0] {current}", inline = False)
			emb.add_field(name = f"Queue ({queue_duration})", value = q, inline = False) if q else None
			emb.set_footer(text = f"🔁 Loop: {loop_value}")
		except Exception as embed_error:
			print(embed_error.__traceback__.tb_lineno)
			return r.emb_resp2(str(embed_error))
		return emb
	
	try:
		msg = await channel.send(embed = await embed(get_short()))
	except Exception as e:
		print(e, type(e))
		return "Error", "Can't fit the queue in one message, something went wrong", "error_2"
	
	if len(items) > 10:
		await msg.add_reaction("⬅️")
		await msg.add_reaction("➡️")
	
	await msg.add_reaction("➕")
	
	def check1(check_reaction, check_user):
		return check_user == author and check_reaction.message.id == msg.id
	
	def check2(check_message: discord.Message):
		c_1 = check_message.author == author
		c_2 = check_message.channel == channel
		c_3 = not check_message.content.startswith(prefix)
		return c_1 and c_2 and c_3
	
	while True:
		response = None
		toggle = False
		try:
			reaction, user = await client.wait_for("reaction_add", check = check1, timeout = 60.0)
		except TimeoutError:
			break
		
		if reaction.emoji == "➡️":
			toggle = True
			count += 1
			if count >= (math.ceil(len(items) / 10)):
				count = 0
		
		elif reaction.emoji == "⬅️":
			toggle = True
			count -= 1
			if count < 0:
				count = math.ceil(len(items) / 10) - 1
		
		elif reaction.emoji == "➕":
			toggle = False
			info_msg = await channel.send("Type the name of the playlist you wish to add the queue to!")
			try:
				playlist_name_msg: discord.Message = await client.wait_for("message", check = check2, timeout = 60.0)
				playlist_name = playlist_name_msg.content
				await playlist_name_msg.delete(delay = 20.0)
				await info_msg.delete(delay = 20.0)
			except TimeoutError:
				await info_msg.edit(content = "Timed out!")
				await info_msg.delete(delay = 20.0)
				continue
			response = f"Added the current queue to the playlist `{playlist_name}`!"
			coll.find_one_and_update(
				{
					"user": user.id,
					"name": playlist_name
				}, {
					"$addToSet": {
						"songs": {
							"$each": [(item["link"], item["title"]) for item in all_items]
						}
					}
				}, upsert = True
			)
		if response:
			await channel.send(embed = r.emb_resp("Info", response, "success"), delete_after = 10.0)
			continue
		if toggle:
			await msg.edit(embed = await embed(get_short()))
	await msg.clear_reactions()


async def remove_from_my_playlist(stuff: u.Data):
	coll = db.db.get_collection("User_Songs")
	channel = stuff.channel
	guild = channel.guild
	playlists = stuff.message.content.split(",")
	author = stuff.author
	found_one = False
	
	if not guild:
		return "Nope", "Not possible here", "error"
	v_c = guild.voice_client
	if not v_c:
		return "Try again when i am actually playing music", "", "error"
	
	current = Player.get_current(guild.id)
	if not current:
		return "Weird", "There should be music but isn't", "error_2"
	
	for playlist in playlists:
		if playlist:
			found_one = True
			coll.find_one_and_update(
				{
					"user": author.id,
					"name": playlist.strip() if playlist else "default"
				}, {
					"$pull": {
						"songs": [current["link"], current["title"]]
					}
				}, upsert = True
			)
			desc = f"`{current['title']}` from `{playlist}`!"
			await channel.send(embed = r.emb_resp("Removed", desc, "ok"))
		if not found_one:
			return "Error", "No playlist given!", "error"


async def resume(stuff: u.Data):
	channel = stuff.channel
	guild = channel.guild
	nick = guild.me.nick
	if not guild:
		await channel.send("***HMMMMM?***")
		return
	v_c = guild.voice_client
	if not v_c:
		await channel.send(":x: Nothing there to resume!")
		return
	if stuff.message.author not in v_c.channel.members:
		await channel.send("*Nope!*")
		return
	if v_c.is_paused():
		await stuff.message.delete(delay = 10)
		v_c.resume()
		await guild.me.edit(nick = nick.replace("[PAUSED] ", ""))


async def schedule(stuff: u.Data):
	channel = stuff.channel
	args = stuff.args
	if not args:
		return "", "No link given!", "error"
	try:
		async with aiohttp.request("GET", args[0]) as resp:
			text = html2text.html2text(await resp.text())
	except Exception as e:
		print(e)
		return "Error!", "There seems to be some problem with the given Link", "error"
	try:
		text = re.search(r"\n.*\*.*Day.*\n(.*\|.*\n)*", text).group()
	except AttributeError:
		return "Error", "```No schedule for Master+1 quests provided in this link!```", "error"
	# print(text)
	titles = text.split("\n", 2)[1].replace("*", "").split("|")
	# print(titles)
	rows = text.split("\n")[3:-1]
	data = []
	for row in rows:
		data.append(row.split("|"))
	# print(data)
	
	print(format_pretty_table(data, titles))
	msg = await channel.send(
		embed = r.emb_resp("Master + 1", f'```python\n{format_pretty_table(data, titles)}```', "success"))
	try:
		await msg.pin()
	except discord.Forbidden:
		pass
	except discord.HTTPException:
		return "Can't pin the schedule", "There are already 50 pinned messages!", "error"


async def showcode(stuff: u.Data):
	channel = stuff.channel
	args = stuff.args

	if args:
		try:
			code = inspect.getsource(globals()[args[0]])
		except KeyError:
			# print(e, type(e))
			found, cmd, status = await db.get_cmd(1, args[0], u.dmp)
			if found:
				code = inspect.getsource(globals()[cmd])
			else:
				return "", "Command not found!", "error"
		code = re.sub(r"`", "\`", code)
		lines = code.splitlines(keepends = True)
		codelines = ""
		line_nr = 0
		spaces = len(str(len(lines))) - 1
		print(spaces)

		for line in lines:
			if len(codelines) < 1500:
				codelines += f"{line_nr}.{' ' * (spaces - len(str(line_nr)) + 1)}|{line}"
			else:
				await channel.send(f"```python\n{codelines}```")
				codelines = f"{line_nr}.{' ' * (spaces - len(str(line_nr)) + 1)}|{line}"
			line_nr += 1
		else:
			await channel.send(f"```python\n{codelines}```")
	else:
		return "Error", "No argument provided!", "error"
	

async def showplaylist(stuff: u.Data):
	print(stuff.message.content)
	client = stuff.client
	channel = stuff.channel
	author = stuff.author
	args = stuff.args
	coll = db.db.get_collection("User_Songs")
	playlists = list(coll.find({"user": author.id}))
	# print(playlists)
	
	if not args:
		playlist_type = "all"
		title = f"Showing all playlists ({len(playlists)} in total)"
	else:
		playlist_type = " ".join(args)
		title = f"Showing playlist `{playlist_type}`"
		
	color = "info"

	def clean(data: list):
		return "- " + "\n- ".join(data) if data else "Empty"

	if not playlists:
		return "Error", "You don't have any playlist registered yet!", "error"

	if playlist_type == "all":
		playlist_data = list(map(lambda x: f"{x['name']} ({len(x['songs'])} songs)", playlists))
	else:
		playlist_data = coll.find_one({"user": author.id, "name": playlist_type})
		if playlist_data:
			playlist_data = list(map(lambda x: x[1], playlist_data["songs"]))
		else:
			return "Error", f"Playlist {playlist_type} couldn't be found!", "error"
	
	if len(playlist_data) < 10:
		return title, clean(playlist_data), color
	
	page = 0
	
	def edit_embed(emb):
		emb.description = clean(playlist_data[page*10:(page+1)*10])
		emb.set_author(name = f"{page + 1}/{math.ceil(len(playlist_data) / 10) or 1}")
		return emb

	embed = r.emb_resp(title, "", color)
	msg = await channel.send(embed = edit_embed(embed))
	
	await msg.add_reaction("⬅️")
	await msg.add_reaction("➡️")
	
	def check(check_reaction, check_user):
		return check_user == author and check_reaction.message.id == msg.id
	
	while True:
		try:
			reaction, user = await client.wait_for("reaction_add", check = check, timeout = 60.0)
		except TimeoutError:
			break
		
		if reaction.emoji == "➡️":
			page += 1
			if page >= (math.ceil(len(playlist_data) / 10)):
				page = 0
		
		elif reaction.emoji == "⬅️":
			page -= 1
			if page < 0:
				page = math.ceil(len(playlist_data) / 10) - 1
				
		await msg.edit(embed = edit_embed(embed))
	await msg.clear_reactions()
	

async def shuffle(stuff: u.Data):
	channel = stuff.channel
	author = stuff.author
	guild = channel.guild
	options = ["answer", "nick"]
	new_name = "".join((lambda x: random.sample(x, len(x)))(author.nick or author.display_name))
	actions = {
		"answer": lambda: channel.send("*I'm a bot, I can't dance, you know?*"),
		"nick": lambda: author.edit(nick = new_name, reason = "Tried to shuffle without music")
	}
	
	if not author.voice:
		await channel.send("*Nope*")
		return
	if not guild:
		await channel.send("***Wrong channel!***")
		return
	v_c = guild.voice_client
	
	if not v_c:
		res = actions[random.choice(options)]
		try:
			await res()
		except discord.Forbidden:
			pass
		return
	
	Player.shuffle(guild.id)
	await channel.send("🔀 Queue shuffled!")


async def skip(stuff: u.Data):
	author = stuff.author
	channel = stuff.channel
	args = stuff.args 
	msg = stuff.message
	
	if not author.voice:
		return "Error", "What are you doing there?", "error"
	
	if not channel.guild:
		return "Error", "Not possible here!", "error"
	else:
		try:
			if len(args):
				value = re.match("\d+", "".join(args))
				if value:
					value = int(value.group())
				else:
					value = " ".join(args)
				res = Player.skip(channel.guild.id, value)
				if not res:
					await channel.send(embed = r.emb_resp("Can't skip!", "no music player found!", "error_2"))
				else:
					await channel.send(embed = r.emb_resp(*res), delete_after = 60.0)
			else:
				channel.guild.voice_client.stop()
		except Exception as e:
			await channel.send(embed = r.emb_resp("Error", str(e)+f"\n{e.__traceback__.tb_lineno}", "error"))
	await msg.delete(delay = 60.0)


async def unmove(stuff: u.Data):
	author = stuff.author
	channel = stuff.channel
	coll = db.db.get_collection("data")
	try:
		if coll.find_one({"id": author.id, "re-move": True}):
			coll.update_one({"id": author.id}, {"$set": {"re-move": False}})
			text = "You won't be automatically \"re-moved\" to the channel from where you have been moved out anymore!"
		else:
			coll.update_one({"id": author.id}, {"$set": {"re-move": True}}, upsert = True)
			text = "You will be automatically \"re-moved\" to the channel from where you have been moved out!"
			
		await channel.send(embed = r.emb_resp("Update successful!", text, "success"))
	except Exception as e:
		return "Error", str(e)+f"\n{e.__traceback__.tb_lineno}", "error"
