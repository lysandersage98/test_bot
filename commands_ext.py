import aiohttp
import asyncio
import bs4
import contextlib
import commands
import discord
import datetime
import googleapiclient.discovery
import importlib
import io
import json
from ldap3 import Server, Connection, ALL
import math
import numpy as np
import os
import pickle
import pprint
import pydoc
import random
import re
import rsa
import subprocess
import sys
import threading
import tkinter as tk
import youtube_dl as yt

from PIL import Image, ImagePalette, ImageFont, ImageDraw
from humanfriendly.tables import format_pretty_table, format_robust_table
from humanfriendly.terminal import ansi_strip
from music import Player, Downloader, MusicManager
from scipy import stats, special
import bot_events as evt
import db_handler as db
import responders as r
import utilities as u

restart = (lambda x: stop(x))


async def bot_sleep(stuff: u.Data):
	dur = stuff.args[0] if stuff.args else 30
	os.system(f"psshutdown.exe -d -t {dur}")
	await stuff.channel.send(f"logging off in {dur}s!")


async def deletemessage(stuff: u.Data):
	channel = stuff.channel
	args = stuff.args
	client = stuff.client
	
	if args:
		try:
			user = client.get_user(int(args[0]))
			dm_c = user.dm_channel
			if not dm_c:
				dm_c = user.create_dm()
			msg = await dm_c.fetch_message(int(args[1]))
			await msg.delete()
		except Exception as e:
			print(e)
			channel = client.get_channel(int(args[0]))
			print(dir(channel))
			# msg = await channel.fetch_msg(int(args[1]))
			# await msg.delete()
	else:
		return "Error", "No arguments!", "error"


async def get_emoji_str(stuff: u.Data):
	channel = stuff.channel
	args = stuff.args
	
	await channel.send(embed = r.emb_resp("", f"```{' '.join(args)}```", "success"))


async def geterror(stuff: u.Data):
	channel = stuff.channel
	args = stuff.args
	print(args)
	
	coll = db.db.get_collection("Errors")
	error = coll.find_one({"hash": int(args[0])})
	if error:
		traceback = ""
		for el in error["traceback"]:
			traceback += "\n".join(el) + "\n"
		embed = r.emb_resp(f"Error Log ({str(error['error'])})", f'```{traceback}```', "success")
		embed.set_author(name = error["author"][0] + ": " + str(error["author"][1]))
		embed.set_footer(text = f"{error['command']} {' '.join(list(map(str,error['args'])))}")
		await channel.send(embed = embed)
	else:
		return "Error", "```Nothing found!`` ", "error"


async def getdirectmessage(stuff: u.Data):
	await stuff.message.delete(delay = 20)
	channel = stuff.channel
	client = stuff.client
	args = stuff.args
	dm_c = await client.get_user(int(args[0])).create_dm()
	dm = await dm_c.history().flatten()
	print(len(dm))
	try:
		dm = dm[:int(args[1])]
	except IndexError:
		await channel.send(embed = r.emb_resp("Error", "Amount missing!", "error"), delete_after = 20.0)
		return
	# await evt.fill_queue(("MSG", list(reversed([((msg.author.name, msg.created_at.strftime("%D %T")), msg.content) for msg in dm]))))
	print("DONE")


async def log(stuff: u.Data):
	client = stuff.client
	args = stuff.args
	channel = stuff.channel
	
	coll = db.db.get_collection(name = "data")
	try:
		target = client.get_user(int(args[0]))
	except IndexError:
		return
	try:
		if args[1] == "stop":
			
			coll.update_one({"name": target.name, "id": target.id}, {"$set": {"log": False}})
			return await channel.send(
				embed = r.emb_resp("Info", f"{target.name}'s DMs won't be logged anymore!", "success"))
		
		elif args[1] == "continue":
			
			coll.update_one({"name": target.name, "id": target.id}, {"$set": {"log": True}})
			return await channel.send(
				embed = r.emb_resp("Info", f"{target.name}'s DMs will be logged again!", "success"))
	
	except Exception as e:
		print(e)
		try:
			coll.insert({"name": target.name, "id": target.id, "log": True, "messages": []})
			return await channel.send(
				embed = r.emb_resp("Info", f"{target.name}'s DMs will now be logged!", "success"))
		
		except db.pymongo.errors.DuplicateKeyError:
			return await channel.send(
				embed = r.emb_resp("Error", "Target already exists! Did you forget \"stop\" or \"continue\"?", "error"))


async def move(stuff: u.Data):
	author = stuff.author
	channel = stuff.channel
	client = stuff.client
	args = stuff.args
	
	if args:
		print(args)
		targets = {ch.name: ch for ch in channel.guild.channels if ch.type.value == 2}
		# print(targets)
		if len(args) == 1:
			
			try:
				target = client.get_channel(int(args[0]))
			except ValueError:
				try:
					target = targets[args[0]]
				except KeyError:
					await channel.send(embed = r.emb_resp("Warning!", "```No valid voice channel given!```",
																color = "error"))
					return
			# print(target)
			here = author.voice.channel
			
			for member in here.members:
				await member.edit(voice_channel = target)
		
		elif len(args) == 2:
			
			try:
				here = client.get_channel(int(args[0]))
			except ValueError:
				here = targets[args[0]]
			
			try:
				target = client.get_channel(int(args[1]))
			except ValueError:
				
				target = targets[args[1]]
			x = True
			
			for member in here.members:
				x = await member.edit(voice_channel = target)
				print(x)
			
			if x:
				return "Warning", "```Wrong channel order?```", "error"
		
		else:
			return "Warning!", "```Invalid amount of arguments!```", "error"
	
	else:
		return "Warning!", "```No arguments given!```", "error"


async def reload(stuff: u.Data):
	channel = stuff.channel
	args = stuff.args
	
	await db.get_json()
	title = "Error"
	arg = ""
	if args:
		print(args)
		try:
			for arg in args:
				print("reloading", arg)
				importlib.reload(importlib.import_module(arg))
			title = ""
			text = f":white_check_mark: Module(s) ```{', '.join(args)}``` reloaded"
		except Exception as e:
			print(e)
			text = f"Module ```{arg}``` doesn't exist!"
	
	else:
		print("no args")
		with open("module-list.json", 'r') as file:
			title = ""
			text = ":white_check_mark: All modules reloaded"
			modules = json.load(file)
			for module in modules:
				if module not in ("startup.py", "music.py"):
					try:
						print("reloading ", module)
						importlib.reload(
							importlib.import_module(module.split(".")[0]))
					except Exception as e:
						print(e)
	if channel:
		await channel.send(embed = r.emb_resp(title, text, "success"))


async def removecommand(stuff: u.Data):
	channel = stuff.channel
	args = stuff.args
	
	if args:
		title = ""
		text = await db.del_cmd(args[0])
		return await channel.send(embed = r.emb_resp(title, text, "success"))
	return await channel.send(embed = r.emb_resp("Warning!", "No arguments given!", "error"))


async def removerole(stuff: u.Data):
	channel = stuff.channel
	client = stuff.client
	args = stuff.args
	message = stuff.message
	
	async def get_object(obj_1, obj_2, search):
		# print(obj_1, type(obj_2), search)
		ret_obj = None
		search_obj = {o.name.lower(): o for o in getattr(obj_2, search + "s")}
		if obj_1.lower() in search_obj:
			ret_obj = search_obj[obj_1.lower()]
			return ret_obj
		else:
			try:
				ret_obj = getattr(obj_2, "get_" + search)(int(obj_1))
				if ret_obj:
					return ret_obj
				else:
					return "Error", f"```No {search} found!```", "error"
			except ValueError:
				await channel.send(embed = r.emb_resp("Error", f"```Invalid {search} given!```", "error"))
		return ret_obj
	
	if "guild" in args:
		desired_guild = args[args.index("guild") + 1]
		guild = await get_object(desired_guild, client, "guild")
		
		if not guild:
			return
	else:
		guild = channel.guild
		'''await channel.send(embed = r.emb_resp2("Invalid usage!"))
		return'''
	
	if "role" in args:
		remove_role = message.role_mentions[0] if message.role_mentions else None
		if not remove_role:
			desired_role = args[args.index("role") + 1]
			remove_role = await get_object(desired_role, guild, "role")
		
		if not remove_role:
			return
	else:
		return "Error", "Invalid usage!", "error"
	
	if "member" in args:
		target = message.mentions[0] if message.mentions else None
		if not target:
			desired_target = args[args.index("member") + 1]
			target = await get_object(desired_target, guild, "member")
		
		if not target:
			return
	else:
		target = guild.members
		
	res = r.emb_resp("RESULT", "", "info")
	desc = f"```Removed role '{remove_role.name}' from``````"
	cnt = 0
	
	txt = "Editing members!"
	msg = await channel.send(txt)
	
	if isinstance(target, list):
		target_count = len(target)
		for member in target:
			# print(member, end = "----")
			await msg.edit(content = f"{txt}\n{cnt/target_count*100}%")
			try:
				roles = member.roles
				try:
					roles.remove(remove_role)
					desc += f"- {member.name}\n"
					cnt += 1
				except ValueError:
					pass
				await member.edit(roles = roles)
			except discord.Forbidden:
				resp = f"```python\nNo permission to take the role \"{remove_role}\" from \"{member.name}\"!```"
				await channel.send(embed = r.emb_resp("Error", resp, "error"))
			except discord.HTTPException:
				pass
		# print("")
	else:
		try:
			roles = target.roles
			try:
				roles.remove(remove_role)
				desc += target.name
			except ValueError:
				await channel.send(embed = r.emb_resp("Error", "```The given user doesn't have this role!```", "error"))
			await target.edit(roles = roles)
		except discord.Forbidden:
			resp = f"```python\nNo permission to take the role \"{remove_role}\" from \"{target.name}\"!```"
			await channel.send(embed = r.emb_resp("Error", resp, "error"), delete_after = 60.0)
		except discord.HTTPException:
			pass
		
	desc += f"``````in {guild.name}!```"
	res.description = desc
	
	try:
		await msg.edit(content = None, embed = res)
	except Exception as e:
		print(e)
		res.description = f"Modified {cnt} members in {guild.name}!" if cnt else str(e)
		await msg.edit(content = None, embed = res)


async def send(stuff: u.Data):
	client = stuff.client
	channel = stuff.channel
	args = stuff.args
	
	try:
		msg = await client.get_channel(int(args[0])).send(" ".join(args[1:]))
	except Exception as e:
		print(e)
		try:
			msg = await client.get_user(int(args[0])).dm_channel.send(" ".join(args[1:]))
		except Exception as e:
			print(e)
			dm_c = await client.get_user(int(args[0])).create_dm()
			msg = await dm_c.send(" ".join(args[1:]))
	try:
		await channel.send(embed = r.emb_resp("Info", f"```{str(msg)}```", "info"))
		return 0
	except Exception as e:
		print(e)
		return msg


async def setrole(stuff: u.Data):
	channel = stuff.channel
	client = stuff.client
	args = stuff.args
	message = stuff.message
	
	async def get_object(obj_1, obj_2, search):
		# print(obj_1, type(obj_2), search)
		ret_obj = None
		search_obj = {o.name.lower(): o for o in getattr(obj_2, search + "s")}
		if obj_1.lower() in search_obj:
			ret_obj = search_obj[obj_1.lower()]
			return ret_obj
		else:
			try:
				ret_obj = getattr(obj_2, "get_" + search)(int(obj_1))
				if ret_obj:
					return ret_obj
				else:
					await channel.send(embed = r.emb_resp("Error", f"```No {search} found!```", "error"))
					return ret_obj
			except ValueError:
				await channel.send(embed = r.emb_resp("Error", f"```Invalid {search} given!```", "error"))
		return ret_obj
	
	if "guild" in args:
		desired_guild = args[args.index("guild") + 1]
		guild = await get_object(desired_guild, client, "guild")
		
		if not guild:
			# value = [(guild.name, guild.id) for guild in client.guilds]
			return  # await msg.edit(embed = msg.embeds[0].add_field(name = "Available Guilds", value = value))
	else:
		guild = channel.guild
		'''await channel.send(embed = r.emb_resp2("Invalid usage!"))
		return'''
	
	if "role" in args:
		assign_role = message.role_mentions[0] if message.role_mentions else None
		if not assign_role:
			desired_role = args[args.index("role") + 1]
			assign_role = await get_object(desired_role, guild, "role")
		
		if not assign_role:
			# value = [(role.name, role.id) for role in guild.roles]
			return  # await msg.edit(embed = msg.embeds[0].add_field(name = "Available Roles", value = value))
	else:
		return "Error", "Invalid usage!", "error"
	
	if "member" in args:
		target = message.mentions[0] if message.mentions else None
		if not target:
			desired_target = args[args.index("member") + 1]
			target = await get_object(desired_target, guild, "member")
		
		if not target:
			# value = [(member.name, member.id) for member in guild.members]
			return  # await msg.edit(embed = msg.embeds[0].add_field(name = "Available Members", value = value))
	else:
		target = guild.members
	
	res = r.emb_resp("RESULT", "", "info")
	desc = f"```Added role '{assign_role.name}' to``````"
	cnt = 0
	
	txt = "Editing members!"
	msg = await channel.send(txt)
	
	if isinstance(target, list):
		all = len(target)
		for member in target:
			# print(member, end = "----")
			await msg.edit(content = f"{txt}\n{cnt / all*100}%")
			try:
				cnt += 1
				await member.edit(roles = [*member.roles, assign_role])
				desc += f"- {member.name}\n"
			except discord.Forbidden:
				resp = f"```python\nNo permission to give \"{member.name}\" the role \"{assign_role}\"!```"
				await channel.send(embed = r.emb_resp("Error", resp, "error"), delete_after = 60.0)
			except discord.HTTPException:
				pass
		# print("")
	else:
		try:
			await target.edit(roles = [*target.roles, assign_role])
			desc += target.name
		except discord.Forbidden:
			resp = f"```python\nNo permission to give \"{target.name}\" the role \"{assign_role}\"!```"
			await channel.send(embed = r.emb_resp("Error", resp, "error"))
		except discord.HTTPException:
			pass
		
	desc += f"``````in {guild.name}!```"
	res.description = desc

	try:
		await msg.edit(content = None, embed = res)
	except Exception as e:
		print(e)
		res.description = f"Modified {cnt} members in {guild.name}!" if cnt else str(e)
		await msg.edit(content = None, embed = res)


async def stop(stuff: u.Data):
	client = stuff.client
	# channel = stuff.channel
	# title = ""
	# text = ":wave: Logging out!"
	# await channel.send(embed = r.emb_resp(title, text, "info"))
	try:
		# evt.fill_queue("STOP")
		await MusicManager.stop(full = True)
		# print("all music stopped")
		await client.change_presence(status = discord.Status.invisible)
	except Exception as e:
		print(e)
	finally:
		await client.logout()


async def evaluate(stuff: u.Data):
	author = stuff.author
	channel = stuff.channel
	client = stuff.client
	message = stuff.message
	args = stuff.args
	# --------------------------------------#
	# print(author, client, message)  #
	
	# --------------------------------------#
	# number = "82682" while(1): print(number) text = str(urllib.request.urlopen(
	# f"http://www.pythonchallenge.com/pc/def/linkedlist.php?nothing={number}").read(1000)) if "next nothing" in text:
	# print(re.search(r"next nothing.*\d+",text).group()) txt = re.search(r"next nothing.*\d+", text).group() number =
	# re.search(r"\d+",txt).group() else: break
	
	# async with aiohttp.request("GET", "https://api-defrag-ap.wrightflyer.net/webview/announcement-detail?id=415701
	# &phone_type=2&lang=en") as resp: text= html2text.html2text(await resp.text()) print(text)
	
	# text = re.search(r"\n.*\*.*Day.*\n(.*\|.*\n)*",text).group()
	# print(text)
	# await channel.send(embed = await r.emb_resp("Master + 1", f'{text.replace("*","")[:-1]}', "success"))
	# \n[test](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)'
	
	def python(py_args):
		py_args = (re.search(r"\(.*\)", py_args).group()[1:-1])
		return py_args
	
	args = " ".join(args)
	# print(type(args), args)
	
	if "help" in args:
		try:
			x = python(args)
			y = pydoc.getdoc(eval(x))
			return await channel.send(embed = r.emb_resp(f"Help for {x}", f"```Python\n{y}```", "success"))
		except Exception as e:
			return await channel.send(embed = r.emb_resp("Failure", f"```{str(e)}```", "error"))
	
	try:
		x = await eval(args)
	
	except Exception as e:
		print(e)
		
		try:
			y = eval(args)
			
			if asyncio.iscoroutine(y):
				return await channel.send(embed = r.emb_resp("Failure", f"```{str(y)}```", "error"))
			else:
				return await channel.send(embed = r.emb_resp("Success", f"```{str(y)}```", "success"))
		
		except Exception as e:
			print(e)
			return await channel.send(embed = r.emb_resp("Failure", f"```{str(e)}```", "error"))
	
	# if asyncio.iscoroutine(x):
	# await channel.send(embed = await r.emb_resp("Failure", f"```{str(x)}```", "error"))
	# else:
	await channel.send(embed = r.emb_resp("Success", f"```{str(x)}```", "success"))


async def new_picture(stuff: u.Data):
	channel = stuff.channel
	client = stuff.client
	author = stuff.author
	args = stuff.args
	start1 = "https://www.youtube.com/watch?v="
	start2 = "https://youtu.be/"
	timestamp_pattern = re.compile("((\?)|(&))t=(\d*)")
	ytdl = yt.YoutubeDL({
		"quiet": True,
		"format": "bestvideo/best",
		"youtube_include_dash_manifest": False
	})
	
	last = False
	if not args:
		return "Error", "No link provided", "error"
	
	link = re.sub("[<>]", "", args[0])
	# print(link)
	if not link.startswith(start1) and not link.startswith(start2):
		return "Error", f"Not a valid input link! Has to start with '{start1}' or '{start2}", "error"
	timestamp = timestamp_pattern.search(link)
	# print(timestamp)
	
	if timestamp:
		timestamp = timestamp.group(4)
		link = link.strip(timestamp)
		# print(timestamp)
		# timestamp = re.search("\d+", timestamp).group()
		timestamp = "0" + str(datetime.timedelta(seconds = float(timestamp)))
	
	elif len(args) >= 2:
		
		if re.match("^\d\d:\d\d:\d\d(.\d+)?$", args[1]):
			timestamp = args[1]
		
		elif args[1] == "last":
			last = True
		
		else:
			print("\nInvalid timestamp format! Has to be of 'HH:MM:SS' with optional '.ms' of any length or 'last'.\n")
	
	else:
		def check(message: discord.Message):
			c_1 = message.author == author
			c_2 = message.channel == channel
			return c_1 and c_2
		
		await channel.send("Input the timestamp for the image you want to have!\nFormat is HH:MM:SS[.ms] or 'last'")
		try:
			msg = await client.wait_for("message", check = check, timeout = 60.0)
			res = msg.content
		except TimeoutError:
			return "Timed out!", "", "info"
		
		if res == "last":
			last = True
		
		else:
			
			if re.match("^\d\d:\d\d:\d\d(.\d+)?$", res):
				timestamp = res
			
			else:
				return "Error", "Invalid timestamp format! Has to be of 'HH:MM:SS' with optional '.ms' of any length.", "error"
	
	# print(link)
	link = timestamp_pattern.sub("", link)
	now = datetime.datetime.now().strftime("%Y-%m-%d %H_%M_%S")
	# print(link)
	
	video = ytdl.extract_info(link, download = False)
	# print(video)
	stream = video.get("url")
	print(stream)
	base_path = "other stuff/bot_profile_pictures"
	_id = video.get("id")
	if _id not in os.listdir(base_path):
		os.mkdir(f'{base_path}/{_id}')
	if last:
		timestamp = video.get("duration")
		timestamp = "0" + str(datetime.timedelta(seconds = float(timestamp)))
	
	hours, minutes, seconds = re.sub("\..*", "", timestamp).split(":")
	time = datetime.timedelta(hours = int(hours), minutes = int(minutes), seconds = int(seconds)).seconds
	name = f"{base_path}/{_id}/{_id}__{now}.png"
	error = subprocess.run(
		["ffmpeg.exe", "-hide_banner", "-ss", timestamp, "-i", stream, "-vframes", "1", name],
		capture_output = True)  # "-loglevel", "level+debug",

	if not error.returncode:
		open(f"{base_path}/{_id}/timestamp.txt", "w").write(str(time))
		embed = r.emb_resp("Info", "File saved successfully!\nSet as profile picture?", "success")
	else:
		return "Hmm", error, "error"
	
	file = discord.File(name)
	
	try:
		msg = await channel.send(embed = embed, file = file)
		resp = await u.reaction(msg, author, client)
		if resp == "❌":
			os.rmdir(f"{base_path}/{_id}")
			return "OK", "Not set!", "info"
	except Exception as e:
		print(e)
	
	await channel.send("Trying to set as profile picture....")
	upload_file = open(name, "rb").read()
	try:
		await client.user.edit(avatar = upload_file)
	except Exception as e:
		return "Error", f"Editing profile picture failed!\n{e}", "error"
	
	bot_info = u.data
	bot_info["profile"] = f"{start2}{_id}?t={time}"
	json.dump(bot_info, open("other stuff/info.json", "w"), indent = 4)
	stuff.args = ["utilities", "commands"]
	await reload(stuff)


async def test(stuff: u.Data):
	args = stuff.args
	client = stuff.client
	message = stuff.message
	# channel: discord.TextChannel = await message.guild.create_text_channel("xxxx", overwrites = {
	# 	message.guild.default_role: discord.PermissionOverwrite(send_messages = False)},
	# 	category = message.channel.category,
	# 	position = message.channel.position - 1)
	
	embed = r.emb_resp("Music-menu", "Now playing: ", "")
	embed.colour = 0x666666
	
	base = ["⬅", "➡", "⚙"]
	settings = ["⏯", "⏹", "⏮", "⏭", "🆕", "🔀", "*⃣", "🔃", "🔄", "↔"]
	numbers = ["0️⃣", "1️⃣", "2️⃣", "3️⃣", "4️⃣", "5️⃣", "6️⃣", "7️⃣", "8️⃣", "9️⃣"]

	toggle = 0
	data = {
		"type": "gui"
	}
	
	# msg = await channel.send(embed = embed)
	# [await msg.add_reaction(x) for x in settings]
	# settings_msg = await channel.send("Toggle menu")
	# [await settings_msg.add_reaction(x) for x in base]
	
	# def check(check_reaction, check_user):
	# 	emoji = str(check_reaction.emoji)
	# 	return check_user == stuff.author and (emoji in base or emoji in settings or emoji in numbers)
	# vc = await stuff.author.voice.channel.connect()
	reaction_cache = []
	ffmpeg_options = {
		"before_options": " -reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5",
		'options': "-vn"  # -loglevel level+trace"
	}
	ytdl = yt.YoutubeDL({
		"quiet": True,
		# "verbose": True,
		"simulate": True,
		"format": "bestaudio/best",
		"ignoreerrors": True,
		"youtube_include_dash_manifest": False
	})
	# if "list" in args[0]:
	# 	async with aiohttp.request("GET", args[0]) as resp:
	# 		try:
	# 			html = await resp.text(encoding = "utf-8")
	# 			bs = bs4.BeautifulSoup(html, "html.parser")
	# 			data = bs.find("body")
	# 			data = next(filter(lambda x: "responseContext" in x.text, data.find_all("script")))
	# 			data = data.getText().split("= ", 1)[1].strip(";")
	# 			data_json = json.loads(data)
	# 			data_json_orig = data_json.copy()
	# 			# json.dump(data_json, open("data.json", "w"), indent = 4)
	# 			keys = list(data_json.keys())
	#
	# 			def find_sub_dict(obj, key):
	# 				if isinstance(obj, dict) and key in obj:
	# 					return obj
	# 				elif type(obj) in (list, set):
	# 					for o in obj:
	# 						result = find_sub_dict(o, key)
	# 						if result:
	# 							yield next(result)
	# 						else:
	# 							return None
	# 					else:
	# 						return None
	# 				elif not (isinstance(obj, dict) or isinstance(obj, list)):
	# 					return None
	# 				else:
	# 					for value in obj.values():
	# 						result = find_sub_dict(value, key)
	# 						if result:
	# 							yield next(result)
	# 			# for key in keys:
	# 			# 	try:
	# 			# 		print(keys)
	# 			# 		data_json = data_json[key]
	# 			# 		if key != "videoId":
	# 			# 			keys.extend(list(data_json.keys()))
	# 			# 		else:
	# 			# 			val = key
	# 			# 			break
	# 			# 	except KeyError:
	# 			# 		print(key, "KEYERROR")
	# 			# 		data_json = data_json_orig[key]
	# 			# else:
	# 			# 	return
	# 			print(find_sub_dict(data_json, "videoId"))
	# 			# json.dump(find_sub_dict(data_json, "videoId"), open("data2.json", "w"), indent = 4)
	#
	# 			return
	# 		except Exception as e:
	# 			print(type(e), e, e.__traceback__.tb_lineno)
	# 		finally:
	# 			resp.close()
	# data = ytdl.extract_info("https://www.youtube.com/watch?v=-Q--ZqWOZrw", download = False)
	data = ytdl.extract_info(args[0], download = False)
	pprint.pp(data)
	return
	json.dump(data, open("data.json", "w"))
	filename = data['url']
	print(filename)
	# print(filename)
	source = discord.FFmpegPCMAudio(source = filename, **ffmpeg_options)

	def task(error = None):
		print(error)
		client.loop.create_task(vc.disconnect())
	
	vc.play(source, after = task)

	# try:
	# 	while True:
	
	# 		reaction, user = await client.wait_for("reaction_add", check = check)

	# 		if reaction.message.id == msg.id:
	# 			print("reaction = ", reaction)
	# 			if reaction not in reaction_cache:
	# 				reaction_cache.append(reaction)
	# 			users = await reaction.users().flatten()
	# 			for user in users:
	# 				if user != client.user:
	# 					await reaction.remove(user)
	
	# 		elif reaction.message.id == settings_msg.id:
	# 			if str(reaction.emoji) == "⬅":
	# 				print("left")
	# 			elif str(reaction.emoji) == "➡":
	# 				print("right")
	
	# 			elif str(reaction.emoji) == "⚙":
	# 				toggle ^= 1
	# 				await msg.clear_reactions()
	# 				if toggle:
	# 					[await msg.add_reaction(x) for x in numbers]
	# 				else:
	# 					[await msg.add_reaction(x) for x in settings]
	
	# 				for reaction in reaction_cache:
	# 					if str(reaction.emoji) == "⏯":
	# 						if vc.is_paused():
	# 							vc.resume()
	# 						else:
	# 							vc.pause()
	# 					print("cached reaction: ", reaction)
	# 				reaction_cache.clear()

	# finally:
	# 	await channel.delete(reason = "Out of use")
	

async def evalcode(stuff: u.Data):
	client = stuff.client
	channel = stuff.channel
	content = stuff.message.content.split("```")[1].strip("```")
	if content.startswith("python"):
		content = content.split("python", 1)[1]
	# content = content.replace(r"\n", "\n")
	content = content.replace(r"\t", "    ")
	print(content)
	x = io.StringIO()
	try:
		with contextlib.redirect_stdout(x):
			exec(content, globals(), locals())
		if x.getvalue():
			mapping = enumerate(x.getvalue().split("\n"))
			mapping = list(mapping)[:-1]
			mult = len(str(len(mapping))) + 1
			# print(mul)
			res = "\n".join(map(lambda y: f"{y[0]}.{' ' * (mult - len(str(y[0])))}|{y[1]}", mapping))
			# print(res)
			
			await channel.send(f"```python\n{re.sub(r'```', '', res)}```")
	except Exception as e:
		return "Error", str(e), "error"
	
	# add = asyncio.create_task(client.wait_for("reaction_add", check = check), name = "add")
	# remove = asyncio.create_task(client.wait_for("reaction_remove", check = check), name = "remove")
	# done, pending = await asyncio.wait([add, remove], return_when = asyncio.FIRST_COMPLETED)


async def toggle(stuff: u.Data):
	# client = stuff.client
	# channel = stuff.channel
	for arg in stuff.args:
		found, cmd, status = await db.get_cmd(1, arg, u.dmp)
		try:
			if found:
				db.db.get_collection("Commands").update_one({"name": cmd}, {"$set": {"enabled": not status}})
				await db.get_json()
				return "Info", f"{cmd} successfully {'enabled' if not status else 'disabled'}", "success"
			else:
				return "Error", f"{cmd} not found!", "error"
		except Exception as e:
			print(e)


async def test2(stuff: u.Data):
	pass