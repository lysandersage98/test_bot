@echo off
pip install -r requirements.txt
:loop
py startup.py
if %errorlevel%==0 (
echo res: %errorlevel%, Stopping bot
goto end
) else (
echo res: %errorlevel%, Restarting bot
goto loop
)
:end