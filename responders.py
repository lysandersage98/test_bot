import datetime
import discord

colors = {
    "": 0,
    "ok": 0x00FF00,
    "std_info": 0xF0F8FF,
    "info": 0x000000,
    "error": 0xFF0000,
    "error_2": 0xCD00CD,
    "success": 0xFFFF00
}


def emb_resp(title: str = "", desc: str = "", color: str = "", url: str = "") -> discord.Embed:
    """Any basic embed object"""
    color = colors[color]
    return discord.Embed(title=title, description=desc, url=url, color=color, timestamp = datetime.datetime.utcnow())


def emb_resp2(msg: str) -> discord.Embed:
    """Generic error response"""
    title = "❌ Error! Something went wrong!"
    description = f"Details: {msg}"
    color = colors["error_2"]
    return discord.Embed(title=title, description=description, color=color)
